<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
 
    Route::group(['middleware' => 'install'], function() {
    Route::get('/', ['as' => 'main', 'uses' => 'MainController@redirect']);
    Route::get('locale/{locale}', ['as' => 'change_locale', 'uses' => 'MainController@changeLocale']);

    // Login
    Route::get('login', ['as' => 'get_login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'post_login', 'uses' => 'Auth\LoginController@login']);

    // Forgot Password
    Route::get('password/email', ['as' => 'get_email', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'post_email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);

    // Reset Password

        Route::post('password/reset', ['as' => 'post_reset', 'uses' => 'Auth\ResetPasswordController@reset']);

    // Add to Queue
    Route::get('queue', ['as' => 'add_to_queue', 'uses' => 'AddToNewQueueController@index']);
    Route::post('queue', ['as' => 'post_add_to_queue', 'uses' => 'AddToNewQueueController@postDept']);
   /* Route::post('queue/dept/{department}/{elem}', ['as' => 'post__add_to_queue1', 'uses' => 'AddToQueueController@postDept1']);

*/

        Route::post('appointments', 'AppointmentsController@index', ['except' => ['show']]);


        Route::get('updatedisplay/{id}', 'UpdateDisplayController@index');
    // Display
        Route::get('display', ['as' => 'display', 'uses' => 'DisplayController@index']);
        
        Route::get('display/{id}', ['as' => 'multidisplay', 'uses' => 'DisplayController@index1']);

      
        Route::resource('specializations', 'SpecializationController', ['except' => ['show']]);
        
        




    // Authenticated
      Route::group(['middleware' => 'auth:users'], function() {
        // Logout
        Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

        // Dashboard
        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        Route::post('dashboard/settings', ['as' => 'dashboard_store', 'uses' => 'DashboardController@store']);

        // Calls
        Route::get('calls', ['as' => 'calls', 'uses' => 'CallController@index']);
        Route::post('calls', ['as' => 'post_call', 'uses' => 'CallController@newCall']);
        Route::post('calls/recall', ['as' => 'post_recall', 'uses' => 'CallController@recall']);

        Route::post('calls/status_update', ['as' => 'status_update', 'uses' => 'CallController@status_update']);

        
        Route::post('calls/dept/{department}', ['as' => 'post_dept', 'uses' => 'CallController@postDept']);
         Route::post('calls/dept/{department}/{elem}', ['as' => 'post_dept1', 'uses' => 'CallController@postDept1']);


        Route::post('calls/pack/{package}', ['as' => 'post_pack', 'uses' => 'CallController@postPack']);
        Route::get('calls/getDeparts','CallController@getDeparts');




        Route::resource('locations', 'LocationController', ['except' => ['show']]);

        Route::resource('departmentGrps', 'DepartmentGroupController', ['except' => ['show']]);

        Route::resource('doctors','DoctorController', ['except' => ['show']]);

        // Department
        Route::resource('departments', 'DepartmentController', ['except' => ['show']]);


        Route::resource('packages', 'PackageController', ['except' => ['show']]);

        Route::resource('help', 'HelpController', ['except' => ['show']]);
      //  Route::resource('calls/getDeparts}','CallController@getDeparts');

         Route::get('missed','HelpController@missed');


        Route::get('help/help','HelpController@help');

        Route::get('myform1/ajax/{id}/{tno}',array('as'=>'myform.ajax','uses'=>'HelpController@myformAjax1'));


 
        Route::resource('service', 'ServiceController', ['except' => ['show']]);

        

        // Counter
            Route::resource('counters', 'CounterController', ['except' => ['show']]);
          
            Route::resource('billingcounters', 'BillingCounterController', ['except' => ['show']]);

        //Reports
            Route::group(['prefix' => 'reports', 'as' => 'reports::'], function() {
            // User Report
            Route::get('user', ['as' => 'user', 'uses' => 'UserReportController@index']);
            Route::get('user/{user}/{date}', ['as' => 'user_show', 'uses' => 'UserReportController@show']);

            // Queue list
            Route::get('queuelist/{date}', ['as' => 'queue_list', 'uses' => 'QueueListReportController@index']);
            
            Route::get('queuelist1/{date}', ['as' => 'queue_list1', 'uses' => 'QueueListReportController@show']);
            
           

            // Monthly Report
            Route::get('monthly', ['as' => 'monthly', 'uses' => 'MonthlyReportController@index']);
            Route::get('monthly/{department}/{sdate}/{edate}', ['as' => 'monthly_show', 'uses' => 'MonthlyReportController@show']);

            // Statistical Report
            Route::get('statistical', ['as' => 'statistical', 'uses' => 'StatisticalReportController@index']);
            Route::get('statistical/{date}/{user}/{department}/{counter}', ['as' => 'statistical_show', 'uses' => 'StatisticalReportController@show']);

            // Missed
            Route::get('missed-overtime', ['as' => 'missed', 'uses' => 'MissedOvertimeReportController@index']);
            Route::get('missed-overtime/{date}/{user}/{counter}/{type}', ['as' => 'missed_show', 'uses' => 'MissedOvertimeReportController@show']);


        });

      Route::post('missed-overtime/reissue', ['as' => 'post_reissue', 'uses' => 'MissedOvertimeReportController@reissue']);


/*
            Route::get('token_report', ['as' => 'missed', 'uses' => 'TokenReportController@index']);
            Route::get('token_report/{date}/{user}/{counter}/{type}', ['as' => 'token_report', 'uses' => 'TokenReportController@show']);

*/



        Route::get('token_report', ['as' => 'token_report', 'uses' => 'TokenReportController@index']);
      /*  Route::post('token_report', ['as' => 'post_call', 'uses' => 'TokenReportController@newCall']);
        Route::post('token_report/recall', ['as' => 'post_recall', 'uses' => 'TokenReportController@recall']);
        Route::post('token_report/dept/{department}', ['as' => 'post_dept', 'uses' => 'TokenReportController@postDept']);
        Route::post('token_report/pack/{package}', ['as' => 'post_pack', 'uses' => 'TokenReportController@postPack']);
        Route::get('token_report/getDeparts','TokenReportController@getDeparts');
*/


       Route::post('token_report/reissue', ['as' => 'post_reissue', 'uses' => 'TokenReportController@reissue']);






           
        // Users
        Route::get('users/{user}/password', ['as' => 'get_user_password', 'uses' => 'UserController@getPassword']);
        Route::post('users/{user}/password', ['as' => 'post_user_password', 'uses' => 'UserController@postPassword']);
        Route::resource('users', 'UserController', ['except' => ['show', 'edit', 'update']]);

        // Settings
        Route::get('settings', ['as' => 'settings', 'uses' => 'SettingsController@index']);
        Route::post('settings', ['as' => 'post_settings', 'uses' => 'SettingsController@update']);
        Route::post('settings/company', ['as' => 'post_company', 'uses' => 'SettingsController@companyUpdate']);
        Route::post('settings/overmissed', ['as' => 'post_over_missed', 'uses' => 'SettingsController@overmissedUpdate']);
        Route::post('settings/locale', ['as' => 'post_locale', 'uses' => 'SettingsController@localeUpdate']);


        Route::get('time', ['as' => 'time', 'uses' => 'TimeController@index']);



    });
});


/*
Route::get('add_to_newqueue', ['as' => 'add_to_newqueue', 'uses' => 'AddToNewQueueController@index']);
Route::get('queue', ['as' => 'add_to_queue', 'uses' => 'AddToNewQueueController@index']);




Route::get('queue', ['as' => 'post_add_to_queue', 'uses' => 'AddToNewQueueController@postDept']);

*/
//post_add_to_queue
        // Users
      
        Route::resource('locationusers', 'LocationUserController');
        Route::get('locationusers/{id}', 'UserReportController@show');
        Route::get('locationusers/{user}/edit', ['as' => 'get_user_edit', 'uses' => 'UserController@edit']);




//schedulers
Route::get('time', ['as' => 'time', 'uses' => 'TimeController@index']);
Route::get('getrecords', ['as' => 'time', 'uses' => 'GetRecordController@index']);
Route::get('scheduleQueue', ['as' => 'time', 'uses' => 'ScheduleQueueController@index']);
Route::get('secondQueue', ['as' => 'time', 'uses' => 'SecondQueueController@index']);
Route::get('thirdQueue', ['as' => 'time', 'uses' => 'ThirdQueueController@index']);
Route::get('scheduleQueues', ['as' => 'time', 'uses' => 'ScheduleQueuesController@index']);
Route::get('checklist/{mob}', ['as' => 'time', 'uses' => 'CheckListController@index']);
Route::resource('mychecklist', 'CheckListController@checkList', ['except' => ['show']]);
Route::get('consultation', ['as' => 'consultation', 'uses' => 'ConsultationController@index']);
Route::get('nextdayconsultation', ['as' => 'consultation', 'uses' => 'NextDayConsultationController@index']);
Route::resource('testtimer', 'TestTimeCallController', ['except' => ['show']]);
Route::get('pptimer', ['as' => 'pptimer', 'uses' => 'PPTimeCallController@index']);
Route::get('nonpptimer', ['as' => 'nonpptimer', 'uses' => 'NonPPTimeCallController@index']);

//schedulers





//PPTimeCallController.php
//Route::get('nextdayconsultation', ['as' => 'consultation', 'uses' => 'NextDayConsultationController@index']);


Route::get('queue_reports', ['as' => 'qmsr', 'uses' => 'QueueReportsController@index']);
Route::get('queue_reports/generate', ['as' => 'generate', 'uses' => 'QueueReportsController@store']);
Route::get('queue_reports/email', ['as' => 'queueemail', 'uses' => 'QueueReportsController@email']);


Route::resource('doctorroom', 'DoctorRoomController', ['except' => ['show']]);
Route::get('doctorroom/{id}', 'DoctorRoomController@show');

//schedulers
Route::resource('timer', 'TimeCallController', ['except' => ['show']]);
//schedulers


Route::group(['middleware' => ['web']], function () {
    // your routes here
});
use App\Models\Locations;
use Illuminate\Support\Facades\Input;

Route::get('select-ajax',['uses'=>'UserController@getDepartments']);
Route::get('select-ajax1',['uses'=>'DepartmentController@getCounters']);

    
Route::get('/getmsg','AjaxController@index');

Route::resource('billcancellation','BillcanCellationController', ['except' => ['show']]);
Route::resource('hcconsultation','HcConsultationController', ['except' => ['show']]);
Route::resource('updatecounter','UpdateCounterController', ['except' => ['show']]);

Route::resource('employee_bulk_upload', 'EmployeeUploadsController',['except' => ['show']]);