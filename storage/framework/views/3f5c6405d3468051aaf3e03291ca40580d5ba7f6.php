<?php $__env->startSection('title', trans('messages.mainapp.menu.call')); ?>

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css')); ?>" type="text/css" rel="stylesheet" media="screen,projection">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem"><?php echo e(trans('messages.mainapp.menu.call')); ?></h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="<?php echo e(route('dashboard')); ?>"><?php echo e(trans('messages.mainapp.menu.dashboard')); ?></a></li>
                        <li class="active"><?php echo e(trans('messages.mainapp.menu.call')); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

 <?php echo e($user->location_id); ?>



    <div class="container">
        <div class="row">
            <!-- Total Queues Start -->
            <div class="col s12 m12">
                <div class="card">
                    <div class="card-content" style="font-size:14px">
                        <span class="card-title" style="line-height:0;font-size:22px"><?php echo e(trans('messages.call.todays_queue')); ?></span>
                        <!-- <div id="result"><h4>Updated Successfully</h4></div> -->
                        <div class="divider" style="margin:10px 0 10px 0"></div>
                        <table id="call-table" class="display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(trans('Services User')); ?></th>
                                    <th><?php echo e(trans('messages.call.number')); ?></th>
                                    <th><?php echo e(trans('messages.call.called')); ?></th>
                                    <th><?php echo e(trans('Department')); ?></th>
                                    <th><?php echo e(trans('Name')); ?></th>
                                    <th><?php echo e(trans('messages.mainapp.menu.counter')); ?></th>
                                    <th><?php echo e(trans('messages.call.recall')); ?></th>
                                      <th><?php echo e(trans('status')); ?></th>
                                </tr>
                            </thead>
                        </table>
                      
                        <form id="new_call" action="<?php echo e(route('post_call')); ?>" method="post">
                            <?php echo e(csrf_field()); ?>

                            <?php if(!$user->is_admin): ?>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <label for="user"><?php echo e(trans('messages.call.user')); ?></label>
                                        <input id="user" type="hidden" name="user" value="<?php echo e($user->id); ?>" data-error=".user">
                                        <input type="hidden" data-error=".user" value="<?php echo e($user->name); ?>" readonly>
                                        <div class="user">
                                            <?php if($errors->has('user')): ?><div class="error"><?php echo e($errors->first('user')); ?></div><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <label for="user" class="active"><?php echo e(trans('messages.call.user')); ?></label>
                                        <select id="user" class="browser-default" name="user" data-error=".user">
                                            <option value=""><?php echo e(trans('messages.select')); ?> <?php echo e(trans('messages.call.user')); ?></option>
                                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cuser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($cuser->id); ?>"<?php echo $cuser->id==old('user')?' selected':''; ?>><?php echo e($cuser->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <div class="user">
                                            <?php if($errors->has('user')): ?><div class="error"><?php echo e($errors->first('user')); ?></div><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                           
                               
<!-- 
                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="department" class="active"><?php echo e(trans('Token')); ?></label>
                                    <select id="department" class="browser-default" name="department" data-error=".department">
                                       
                                               <?php if(count($queuess) > 0): ?>  
                                                <option value="<?php echo e($queuess[0]['department_id']); ?>" selected><?php echo e($queuess[0]['department_id']); ?></option>
                                           
                                            <?php endif; ?>
                               
                                    </select>
                                    <div class="department">
                                        <?php if($errors->has('department')): ?><div class="error"><?php echo e($errors->first('department')); ?></div><?php endif; ?>
                                    </div>
                                </div>
                            </div>


                         
 -->








                          


                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="counter" class="active"><?php echo e(trans('messages.mainapp.menu.counter')); ?></label>
                                    <select id="counter" class="browser-default" name="counter" data-error=".counter">
                                       
                                        <?php $__currentLoopData = $counters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $counter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(session()->has('counter') && ($counter->id==session()->get('counter'))): ?>
                                                <option value="<?php echo e($counter->id); ?>" selected><?php echo e($counter->name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($counter->id); ?>"><?php echo e($counter->name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <div class="counter">
                                        <?php if($errors->has('counter')): ?><div class="error"><?php echo e($errors->first('counter')); ?></div><?php endif; ?>
                                    </div>
                                </div>
                            </div>


                             <div class="row">
                                <div class="input-field col s12">

                                    <label for="counter" class="active"><?php echo e(trans('messages.mainapp.menu.counter')); ?></label>
                                   
                                    <div id= 'demo'> 
                                      
                                         

                            
                                    </div>
                                    <div id= 'demo1'> 
                                      
                                         

                            
                                    </div>
                                </div></div>



                                

                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn waves-effect waves-light right" type="submit">
                                        <?php echo e(trans('messages.call.call_next')); ?><i class="mdi-navigation-arrow-forward right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Total Queues End -->
            <!--  Hold/Missed Start -->
         <div class="col s12 m12">
                <div class="card">
                    <div class="card-content" style="font-size:14px">
                        <span class="card-title" style="line-height:0;font-size:22px">Missed/Hold</span>
                        <div class="divider" style="margin:10px 0 10px 0"></div>
                        <table id="call-table1" class="display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(trans('Services User')); ?></th>
                                    <th><?php echo e(trans('messages.call.number')); ?></th>
                                    <th><?php echo e(trans('messages.call.called')); ?></th>
                                    <th><?php echo e(trans('Department')); ?></th>
                                    <th><?php echo e(trans('Name')); ?></th>
                                    <th><?php echo e(trans('messages.mainapp.menu.counter')); ?></th>
                                    <th><?php echo e(trans('messages.call.recall')); ?></th>
                                    <th><?php echo e(trans('Status')); ?></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
             <!--  Hold/Missed End -->
        </div>
    </div>

<div id="modal1" class="modal">
  <div class="modal-content">
    <div class="modal-header">
        <h5>Status Update</h5>
    </div>
    <div class="modal-body">
        <div id="modal_content"></div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('print'); ?>
    <?php if(session()->has('department_name')): ?>
        <style>#printarea{display:none;text-align:center}@media  print{#loader-wrapper,header,#main,footer,#toast-container{display:none}#printarea{display:block;}}@page{margin:0}</style>
        <div id="printarea" style="line-height:1.25">
            
            <span style="font-size:27px; font-weight: bold"><?php echo e($company_name); ?></span><br>

            <span style="font-size:25px"><?php echo e(session()->get('department_name')); ?></span><br>
            <span style="font-size:20px">Your Token Number</span><br>
            <span><h3 style="font-size:70px;font-weight:bold;margin:0;line-height:1.5"><?php echo e(session()->get('number')); ?></h3></span>
            <span style="font-size:20px">Please wait for your turn</span><br>
            <span style="font-size:20px">Total customer(s) waiting: <?php echo e(session()->get('total')-1); ?></span><br>
            <span style="float:left"><?php echo e(\Carbon\Carbon::now()->format('d-m-Y')); ?></span><span style="float:right"><?php echo e(\Carbon\Carbon::now()->format('h:i:s A')); ?></span>
        </div>
        <script>
            window.onload = function(){window.print();}
        </script>
    <?php endif; ?>






                   




<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js')); ?>"></script>
<script>
function getHtml() {
  return '<div class="col s12">' +
  '<form method="post">'+
  '<input type="hidden" id="callid" name="callid">' +
  '<div class="stus" id="status">' +
  '<input type="radio" id="1" name="status" value="1">' +
  '<label for="1" style="margin-right:15px; padding-left: 25px;">Finished</label>'+
  '<input type="radio" id="2" name="status" value="2">' +
  '<label for="2" style="margin-right:15px; padding-left: 25px;">Partial Complete</label>' +
  '<input type="radio" id="3" name="status" value="3">' +
  '<label for="3" style="margin-right:15px; padding-left: 25px;">Stand By</label>' +
  '<input type="radio" id="4" name="status" value="4">' +
  '<label for="4" style="margin-right:15px; padding-left: 25px;">Reschedule</label>' +
  '<input type="radio" id="5" name="status" value="5">' +
  '<label for="5" style="margin-right:15px; padding-left: 25px;">No Show</label>' +
  '</div>' +
   '<div class="row">' +
   '<label for="message">Message</label>'+
    '<textarea  name="message" class="form-control" id="message" rows="10" cols="50">'+
    '</textarea>'+
    '</div>'+
    '<div class="row" style="margin-bottom:0px">' +
    '<input type="button" name="submit" value="Submit" onclick="statusupdate()"" class="btn btn-primary">'+
    '</div>'+
    '</form>'+
    '</div>';
};
</script>
<script>
        $("#new_call").validate({
/*            rules: {
                user: {
                    required: true,
                    digits: true
                },
                department: {
                    required: true,
                    digits: true
                },
                counter: {
                    required: true,
                    digits: true
                },
            },*/
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
        function call_dept(value) {
            $('body').removeClass('loaded');
            var myForm1 = '<form id="hidfrm1" action="<?php echo e(url('calls/dept')); ?>/'+value+'" method="post"><?php echo e(csrf_field()); ?></form>';
            $('body').append(myForm1);
            myForm1 = $('#hidfrm1');
            myForm1.submit();
        }


         function call_pack(value) {
            $('body').removeClass('loaded');
            var myForm2 = '<form id="hidfrm2" action="<?php echo e(url('calls/pack')); ?>/'+value+'" method="post"><?php echo e(csrf_field()); ?></form>';
            $('body').append(myForm2);
            myForm1 = $('#hidfrm2');
            myForm1.submit();
        }





       function recall(call_id) {          
       
            $('body').removeClass('loaded');
            var data = 'call_id='+call_id+'&_token=<?php echo e(csrf_token()); ?>';



            $.ajax({
                type:"POST",
                url:"<?php echo e(route('post_recall')); ?>",
                data:data,
                cache:false,

                success: function(response) {
                 
                    location.reload();
                }
            });
        }
     $('.modal-trigger').leanModal();

       function status(call_id) {
        var htmlFromServer = getHtml();
        
        $('#modal_content').html(htmlFromServer);
        $('#modal1').openModal();
        $("#callid").val(call_id);
/*         var mes=document.getElementById("message").value;
         var stus=document.getElementById("status").value;
        var data = 'call_id='+call_id+'&mes='+mes+'&_token=<?php echo e(csrf_token()); ?>';
         console.log(data);
        $.ajax({
                type:"POST",
                url:"<?php echo e(route('status_update')); ?>",
                data:data,
                cache:false,

                success: function(response) {
                 
                    location.reload();
                }
            });*/
        }
        function statusupdate()
        {
        var mes=document.getElementById("message").value;
        var stus=document.getElementById("status").value;
        var callid=document.getElementById("callid").value;

        if (document.getElementById('1').checked) {
          radiostatus = document.getElementById('1').value;
        } else  if (document.getElementById('2').checked) {
          radiostatus = document.getElementById('2').value;
        } else  if (document.getElementById('3').checked) {
          radiostatus = document.getElementById('3').value;
        } else  if (document.getElementById('4').checked) {
          radiostatus = document.getElementById('4').value;
        } else  if (document.getElementById('5').checked) {
          radiostatus = document.getElementById('5').value;
        }
        var statusst = radiostatus;
         var data = 'callid='+callid+'&mes='+mes+'&stus='+statusst+'&_token=<?php echo e(csrf_token()); ?>';
            alert(radiostatus);

            $.ajax({
                type:"POST",
                url:"<?php echo e(route('status_update')); ?>",
                data:data,
                cache:false,

              success: function(response) {
                 
                    location.reload();
                }
            });
        }

        $(function() {

            var calltable = $('#call-table').dataTable({
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_",
                    "sSearch": "Search"
                },
                "columnDefs": [{
                    "targets": [ -1 ],
                    "searchable": false,
                    "orderable": false
                }],
                "ajax": "<?php echo e(url('help')); ?>",
  

                "columns": [
                    { "data": "id" },
                    { "data": "service_user" },
                    { "data": "number" },
                    { "data": "called" },
                    { "data": "counter1" },
                    {  "data" : "name"},
                    { "data": "counter" },
                    { "data": "recall" },
             /*       {
                    "data": null,
                    "className": "center",
                    "defaultContent": '    <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>'
                   }*/
                    { "data": "hold" }
                ]
            });

            setInterval(function(){
                calltable.api().ajax.reload(null,false);
            }, 3000);

        });









//calls/getDeparts


    </script>
    

    <script>
          $(function() {

            var calltable1 = $('#call-table1').dataTable({
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_",
                    "sSearch": "Search"
                },
                "columnDefs": [{
                    "targets": [ -1 ],
                    "searchable": false,
                    "orderable": false
                }],
                "ajax": "<?php echo e(url('missed')); ?>",

                //   console.log(data);
                "columns": [
                    { "data": "id" },
                    { "data": "service_user" },
                    { "data": "number" },
                    { "data": "called" },
                    { "data": "counter1" },
                    {  "data" : "name"},
                    { "data": "counter" },
                    { "data": "recall" },
                     { "data": "status" }
                ]
            });


            setInterval(function(){
                calltable1.api().ajax.reload(null,false);
            }, 3000);
        });      
    </script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
   /* (function() {
        $("#basin_id").change(function() {
            $.getJSON("calls/getDeparts", function(data) {
                var $stations = $("#station_id");
                     $.each(data, function(index, value) {
                    $stations.append('<option value="' + index +'">' + value + '</option>');
                });
            });
        });
    });*/


 


   /* (function () {

        setInterval(function () {


            axios.get('calls/getDeparts',)
                .then(function(response){
                        document.querySelector('#partial')
                                .innerHtml(response.data);
                }); // do nothing for error - leaving old content.
            }); 
        }, 5000); // milliseconds
*/


function doRefresh(){
   


$.get("<?php echo e(url('calls/getDeparts')); ?>", 
               
                function(data) {

                    //Total Queue   '+data.length;
                    var model = $('#model');
                    model.empty();

                 if(data.length != 0){   
                

                document.getElementById("demo").innerHTML = '<input type="hidden" name="department" value ="'+data[0]["department_id"] +'" > ';
                
                document.getElementById("demo1").innerHTML = '<input type="text" name="qid" value ="'+data[0]["id"] +'" > <BR> Total Queue   '+data.length+'<BR> Proceed to Call';   
                    }else{

                      

                document.getElementById("demo").innerHTML = '<input type="hidden" name="department" value ="null" > <BR> No Queues';
                 
                    }


                });
                
                }



   $(function() {
       setInterval(doRefresh, 3000);
    });








</script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>