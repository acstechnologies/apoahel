<?php $__env->startSection('title', trans('messages.issue').' '.trans('messages.display.token')); ?>

<?php $__env->startSection('css'); ?>
   

    
    
<style>

* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 20%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media  screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 1px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

  <div class=" s12 m6 6 " >
                                
       <?php echo $url_code = \Request::segment(2);                           
             echo $url_counter = \Request::segment(3);
            ?>     



                            
<?php if(Session::has('messages')): ?>
<p class="alert alert-success"><?php echo e(session('messages')); ?></p>
<?php endif; ?>
</div>

<div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m4" style="padding-top:10px;padding-bottom:10px">
                
                
                    <div class="row">
                        <div class="input-field col s12">
                            <label style="font-size:120%; color:black;"> <?php echo e(trans('Please Enter Registered UHID')); ?></label>
                            <br>
                            
                           
                        </div>
                    </div>
                   
               
            </div>
        </div>
    </div>
<div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3" style="padding-top:10px;padding-bottom:10px" >
                
                
                    <div class="row">
                        <div class="input-field col s12">
                           
                           
                           
                        <input  style="font-size:200%; color:black;" type ='text' name='uhid' id="uhid" placeholder="<?php echo e(trans(' ')); ?> " autofocus  required>
                             
                        
                       
                      
                           
                        </div>
                    </div>
                   
               
            </div>
        </div>
    </div>





<div class="container">
<div class="row">
            <div class="col s12 m6 offset-m5" style="padding-top:10px;padding-bottom:10px" >
                
                
                    <div class="row">
                        <div class="input-field col s12">
                            <label style="font-size:150%; color:black;"> <?php echo e(trans('Please Select Option')); ?></label>
                            
                        </div>
                    </div>
                   
               
            </div>
        </div>
    </div>
<br>
<br>
<br>
<br>

<div class="container">
<div class="row">
<div class="col s12 offset-m1" >
    

    
<div class="col s2">
           
<div class="card hoverable" onclick="call_depts3()">
 <div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id03').style.display='block'">
     <div class="center-align"><i class="fas fa-user-md fa-3x"></i></div>
 </div>
<div style="padding:20px;" class="card-action light-blue darken-4">
<div class="center-align">
<a  style="text-transform:none;color:#fff">Consultation </a>
</div>
    </div></div>
                 
</div>
    
<div class=" col s2">
                   
<div class="card hoverable" onclick="call_depts4()">
<div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id04').style.display='block'">
    <div class="center-align"><i class="fas fa-diagnoses fa-3x"></i></div>
</div>
<div  style="padding:20px;" class="card-action light-blue darken-4">
<div class="center-align">
<a style="text-transform:none;color:#fff">Health Checks </a>
</div>
</div>
</div>  
</div>


<div class=" col s2">
                   
<div class="card hoverable" onclick="helpdesk()">
<div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('idhelp').style.display='block'">
    <div class="center-align"><i class="fas fa-diagnoses fa-3x"></i></div>
</div>
<div  style="padding:20px;" class="card-action light-blue darken-4">
<div class="center-align">
<a style="text-transform:none;color:#fff">Help Desk </a>
</div>
</div>
</div>  
</div>
    



</div>
</div>
</div>


































<?php $__env->stopSection(); ?>







<?php $__env->startSection('script'); ?>

 

    <script type="text/javascript">
        $(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
        $(window).resize(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
     

     function call_depts1(){
         
         
        var elem = document.getElementById('number').value;
        var counter = document.getElementById('counter').value;
        var qtype = 1;
        var lid = document.getElementById('location_id').value;
         
        $('body').removeClass('loaded');
        var myForm1 ='<form id="hidfrm1" action="<?php echo e(url('queue/dept')); ?>/'+counter+'/'+elem+'/'+qtype+'/'+lid+'" method="post"><?php echo e(csrf_field()); ?></form>';
        $('body').append(myForm1);
        myForm1 = $('#hidfrm1');
        myForm1.submit();
       }


     function call_depts2(){
        
        var elem = document.getElementById('number').value;
          var dep_id = document.getElementById('department').value;
        var counter = document.getElementById('counter').value;
        var qtype = 2;
        var lid = document.getElementById('location_id').value;
         
        $('body').removeClass('loaded');
        var myForm2 = '<form id="hidfrm2" action="<?php echo e(url('queue/dept')); ?>/'+counter+'/'+elem+'/'+qtype+'/'+lid+'" method="post"><?php echo e(csrf_field()); ?></form>';
        $('body').append(myForm2);
        myForm2 = $('#hidfrm2');
        myForm2.submit();
       }

        function call_depts3(){
            
           
          
        var elem = document.getElementById('uhid').value;
           
          var id =1;
        $('body').removeClass('loaded');
        var myForm3 ='<form id="hidfrm3" action="<?php echo e(route('post_add_to_queue')); ?>" method="post"><?php echo e(csrf_field()); ?><input type="hidden" name="uhid" value="'+elem+'"><input type="hidden" name="hid" value="'+id+'"></form>';
        $('body').append(myForm3);
        myForm3 = $('#hidfrm3');
        myForm3.submit();
      
        
        /*
            var elem = document.getElementById('uhid').value;
            $('body').removeClass('loaded');
            var myForm2 = '<form id="hidfrm3" action="<?php echo e(route('post_add_to_queue')); ?>" method="post"><?php echo e(csrf_field()); ?><input type="hidden" name="uhid" value="'+elem+'"></form>';
            $('body').append(myForm3);
            myForm3 = $('#hidfrm3');
            myForm3.submit();
        
        
        */
        
       
        
        }

        function call_depts4(){
            
          
          
        var elem = document.getElementById('uhid').value;
          var id =2;   
          
         
        $('body').removeClass('loaded');
        var myForm4 = '<form id="hidfrm4" action="<?php echo e(route('post_add_to_queue')); ?>" method="post"><?php echo e(csrf_field()); ?><input type="hidden" name="uhid" value="'+elem+'"><input type="hidden" name="hid" value="'+id+'"></form>';
        $('body').append(myForm4);
        myForm4 = $('#hidfrm4');
        myForm4.submit();
       }

        function helpdesk(){
                                
        var elem = document.getElementById('uhid').value;
          var id =6;   
          
         
        $('body').removeClass('loaded');
        var myForm6 = '<form id="hidfrm6" action="<?php echo e(route('post_add_to_queue')); ?>" method="post"><?php echo e(csrf_field()); ?><input type="hidden" name="uhid" value="'+elem+'"><input type="hidden" name="hid" value="'+id+'"></form>';
        $('body').append(myForm6);
        myForm6 = $('#hidfrm6');
        myForm6.submit();
       }

        function call_depts5(){
           
        var elem = document.getElementById('number').value;
             var dep_id = document.getElementById('department').value;
        var counter = document.getElementById('counter').value;
        var qtype = 5;
        var lid = document.getElementById('location_id').value;
             
        $('body').removeClass('loaded');
        var myForm5 = '<form id="hidfrm5" action="<?php echo e(url('queue/dept')); ?>/'+counter+'/'+elem+'/'+qtype+'/'+lid+'" method="post"><?php echo e(csrf_field()); ?></form>';
        $('body').append(myForm5);
        myForm5 = $('#hidfrm5');
        myForm5.submit();
       }



  $(document).ready(function(){
          $(".alert").delay(5000).slideUp(5000)
    });





    </script>

<?php $__env->stopSection(); ?>



        
       
<?php echo $__env->make('layouts.mainappqueue_new', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>