
@extends('layouts.mainappqueue')

@section('title', trans('messages.display.display'))

@section('content')
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.css')}}"/>  

   <div class="container"> 
    <div id="callarea" class="row" style="line-height:1.23;">
      
<?php
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 4;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
            @foreach ($data as $data)
                 <div class="col-sm-3 py-2<?php echo $bootstrapColWidth; ?>" style="margin-bottom: 25px;">
                <div class="card card-body h-100 center-align" style="min-height: 25; max-height: 25;  width: 20rem; ">

                    <span id="num1" style="font-size:55px; font-weight:bold;line-height:1.00">{{ $data['number'] }}</span><br>
                    <small id="cou1" style="font-size:20px;color: #ff6600">{{ $data['counter'] }}</small>
                
                     </div>  
                </div>
                <?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
        ?>
            @endforeach     
    </div>
   </div>

    <div class="row" style="margin-bottom:0;font-size:{{ $settings->size }}px;color:{{ $settings->color }}">
        <marquee>{{ $settings->notification }}</marquee>
    </div>
@endsection

@section('script')
    
    <script type="text/javascript" src="{{ asset('assets/js/voice.min.js') }}"></script>
    <script>
        $(function() {
            $('#main').css({'min-height': $(window).height()-114+'px'});
        });
        $(window).resize(function() {
            $('#main').css({'min-height': $(window).height()-114+'px'});
        });

        (function($){
            $.extend({
                playSound: function(){
                  return $("<embed src='"+arguments[0]+".mp3' hidden='true' autostart='true' loop='false' class='playSound'>" + "<audio autoplay='autoplay' style='display:none;' controls='controls'><source src='"+arguments[0]+".mp3' /><source src='"+arguments[0]+".ogg' /></audio>").appendTo('body');
                }
            });
        })(jQuery);

        function checkcall() {
            $.ajax({
                type: "GET",
                url: "{{ url('assets/files/display') }}",
                cache: false,
                success: function(response) {
                    s = JSON.parse(response);
                    if (curr!=s[0].call_id) {
                        $("#callarea").fadeOut(function(){
                            $('#num0').html(s[0].number);
                            $("#cou0").html(s[0].counter);
                            $('#num1').html(s[1].number);
                            $("#cou1").html(s[1].counter);
                            $('#num2').html(s[2].number);
                            $("#cou2").html(s[2].counter);
                            $('#num3').html(s[3].number);
                            $("#cou3").html(s[3].counter);
                        });
                        $("#callarea").fadeIn();
                        if (curr!=0) {
                            var bleep = new Audio();
                            bleep.src = '{{ url('assets/sound/sound1.mp3') }}';
                            bleep.play();

                            window.setTimeout(function() {
                                msg1 = '{!! trans('messages.display.token') !!} '+s[0].call_number+' {!! trans('messages.display.please') !!} {!! trans('messages.display.proceed_to') !!} '+s[0].counter;
                                responsiveVoice.speak(msg1, "{{ $settings->language->display }}", {rate: 0.85});
                            }, 800);
                        }
                        curr = s[0].call_id;
                    }
                }
            });
        }

        window.setInterval(function() {
            checkcall();
        }, 3000);

        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "{{ url('assets/files/display') }}",
                cache: false,
                success: function(response) {
                    s = JSON.parse(response);
                    curr = s[0].call_id;
                }
            });
            checkcall();
        });
    </script>
@endsection
