<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <title>@yield('title')</title>
        <link rel="icon" href="{{ asset('assets/favicon.ico') }}">
        <link href="{{ asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        @yield('css')
        <link href="{{ asset('assets/css/style.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    </head>

    <body>
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>

        <header id="header" class="page-topbar">
            <div class="navbar-fixed">
                    <nav class="navbar-color">
                    <div class="nav-wrapper">
                        <ul class="left">
                            <!-- <li><h1 class="logo-wrapper"><a href="{{ route('dashboard') }}" class="brand-logo darken-1"><img src="{{ asset('assets/images/jltoken-logo.png') }}" alt="materialize logo"></a><span class="logo-text">ACS Queue</span></h1></li> -->
                            <li><h1 class="logo-wrapper"><a href="{{ route('dashboard') }}" class="brand-logo darken-1"><img src="{{ asset('assets/images/Apollo-Clinics-Logo.jpg') }}" alt="materialize logo"></a><span class="logo-text">ACS Queue</span></h1></li>
                        </ul>
                                               
                       
                        <ul class="right hide-on-med-and-down">
                            <span class="truncate" style="margin-right:20px;font-size:19px">{{ $company_name }}</span>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

        <div id="main">
            <div class="wrapper">
                

                <section id="content">
                    @yield('content')
                </section>
            </div>
        </div>

        <footer class="page-footer">
            <div class="footer-copyright">
                <div class="container">
                    <span>Powered by <a class="grey-text text-lighten-3" href="http://acstechnologies.co.in/" target="_blank">ACS Technologies Ltd.</a> All rights reserved.</span>
                    <span class="right"> <span class="grey-text text-lighten-3">Version</span> 2.1.1</span>
                </div>
            </div>
        </footer>
        @yield('print')
        <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery-1.11.2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins.min.js') }}"></script>
        @yield('script')
        <script>
            $(function() {
                $('#main').css({'min-height': $(window).height()-134+'px'});
                $(window).resize(function() {
                    $('#main').css({'min-height': $(window).height()-134+'px'});
                });
                $('#translation-dropdown').perfectScrollbar();
                $('select').each(function() {
                    if(!$(this).parent().hasClass('picker__header')) {
                        $(this).select2();
                    }
                });
            });
            $(".frmsubmit").on("click",function(e) {
                var message = 'Are you sure you want to delete?';
                if(e.currentTarget.attributes.message!=undefined) message = e.currentTarget.attributes.message.value;
                var cnfrm = true;
                if(e.currentTarget.attributes.message!=undefined && e.currentTarget.attributes.message.value=='false') cnfrm = false;
                if (cnfrm) {
                    if(confirm(message)) {
                        e.preventDefault();
                        var myForm = '<form id="hidfrm" action="'+e.currentTarget.attributes.href.value+'" method="post">{{ csrf_field() }}<input type="hidden" name="_method" value="'+e.currentTarget.attributes.method.value+'"></form>';
                        $('body').append(myForm);
                        myForm = $('#hidfrm');
                        myForm.submit();
                    }
                } else {
                    e.preventDefault();
                    var myForm = '<form id="hidfrm" action="'+e.currentTarget.attributes.href.value+'" method="post">{{ csrf_field() }}<input type="hidden" name="_method" value="'+e.currentTarget.attributes.method.value+'"></form>';
                    $('body').append(myForm);
                    myForm = $('#hidfrm');
                    myForm.submit();
                }
                return false;
            });
            @if(!Request::is('calls*'))
                $(document).ajaxStart(function() {
                    $("body").removeClass('loaded');
                    Pace.restart();
                });
                $(document).ajaxStop(function() {
                    $("body").addClass('loaded');
                });
            @endif
        </script>
        @include('common.messages')
    </body>
</html>
