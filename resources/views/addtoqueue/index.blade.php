@extends('layouts.mainappqueue')

@section('title', trans('messages.issue').' '.trans('messages.display.token'))

@section('css')
    <style>
        .btn-queue{padding:25px;font-size:47px;line-height:36px;height:auto;margin:10px;letter-spacing:0;text-transform:none}
        .modal{max-height: 35%; max-width: 30%; }
        input[type=text]{font-size: 2.5em;}
        .btn-space {
    margin-right: 10px;
}
    </style>
    

   <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 

 
@endsection

@section('content')





    <div class="row">
    <div class="col s12">
    <div class="card" style="background:#f9f9f9;box-shadow:none">
    <span class="card-title" style="line-height:0;font-size:22px">{{ trans('Click  Services to Issue Token') }}</span>
    <div class="divider" style="margin:10px 0 10px 0"></div>
        
    <button type="button" class="btn btn-large btn-queue waves-effect waves-light" value="{{$departments[6]['id']}}"  onclick='f1(this)' data-toggle="modal" data-target="#myModal">
       CONSULTATION
    </button>
        
    </div>
    </div>
    </div>




    <div class="row">
    <div class="col s12">
    <div class="card" style="background:#f9f9f9;box-shadow:none">
    <span class="card-title" style="line-height:0;font-size:22px">{{ trans('Click one Enrolment to Issue Token') }}</span>
    <div class="divider" style="margin:10px 0 10px 0"></div>
        
    <button type="button" class="btn btn-large btn-queue waves-effect waves-light" value="{{$departments[0]['id']}}" onclick='f2(this)' data-toggle="modal" data-target="#myModal1">
      Enrolment
    </button>
         
    </div>
    </div>
    </div>










    <!-- The Modal -->
    <div class="modal"  id="myModal">
    
    <!-- Modal Header -->
    
    <div class="modal-header" >
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <span class="card-title" style="">{{ trans('Please Enter Mobile Number') }}</span>
    </div>
        
      <!-- Modal body -->
      
     <div style="margin-top:20px;" class="modal-body">
     <div style="" class="row">
     <div class="col-lg-12">
     <div class="form-group">
     <input type="text" class="form-control" name='number' id="number" placeholder="Enter Mobile Number">
     </div>
     <input id="department" type="text" name="department" placeholder="{{ old('name') }}" value="" data-error=".name">
     <div class="form-group">
     <button type='submit' name="submit" class="btn btn-primary  btn-space pull-right" onclick="call_depts()" >
       Submit
      </button>
     <button type='submit' name="submit" class="btn btn-primary btn-space pull-right" data-dismiss="modal">
      Cancel
      </button>
    </div>
    </div>
    </div>
    </div>
    </div>




    <div class="modal"  id="myModal1">
    
    <!-- Modal Header -->
    
    <div class="modal-header" >
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <span class="card-title" style="">{{ trans('Please Enter Mobile Number') }}</span>
    </div>
        
      <!-- Modal body -->
      
     <div style="margin-top:20px;" class="modal-body">
     <div style="" class="row">
     <div class="col-lg-12">
     <div class="form-group">
     <input type="text" class="form-control" name='number' id="number1" placeholder="Mobile Number">
     </div>
     <input id="department1" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
     <div class="form-group">
     <button type='submit' name="submit" class="btn btn-primary  btn-space pull-right" onclick="call_depts1()" >
       Submit
      </button>
     <button type='submit' name="submit" class="btn btn-primary btn-space pull-right" data-dismiss="modal">
      Cancel
      </button>
    </div>
    </div>
    </div>
    </div>
    </div>

  
  


@endsection






@section('print')
    @if(session()->has('department_name'))
        <style>#printarea{display:none;text-align:center}@media print{#loader-wrapper,header,#main,footer,#toast-container{display:none}#printarea{display:block;}}@page{margin:0}</style>
        <div id="printarea" style="line-height:1.25">
            <span style="font-size:27px; font-weight: bold">{{ $settings->name }}</span><br>
            <span style="font-size:25px">{{ session()->get('department_name') }}</span><br>
            <span style="font-size:20px">Your Token Number</span><br>
            <span><h3 style="font-size:70px;font-weight:bold;margin:0;line-height:1.5">{{ session()->get('number') }}</h3></span>
            <span style="font-size:20px">Please wait for your turn</span><br>
            <span style="font-size:20px">Total customer(s) waiting: {{ session()->get('total')-1 }}</span><br>
            <span style="float:left">{{ \Carbon\Carbon::now()->format('d-m-Y') }}</span><span style="float:right">{{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
        </div>
        <script>
            window.onload = function(){window.print();}
        </script>
    @endif
@endsection

@section('script')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
        $(window).resize(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
        function queue_dept(value) {
            $('body').removeClass('loaded');
            var myForm2 = '<form id="hidfrm2" action="{{ route('post_add_to_queue') }}" method="post">{{ csrf_field() }}<input type="hidden" name="department" value="'+value+'"></form>';
            $('body').append(myForm2);
            myForm2 = $('#hidfrm2');
            myForm2.submit();
        }
    

        function call_dept(value) {

            var elem = document.getElementById('number').value;
           
            $('body').removeClass('loaded');
            var myForm1 = '<form id="hidfrm1" action="{{ url('calls/dept') }}/'+value+'/'+elem+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm1);
            myForm1 = $('#hidfrm1');
            myForm1.submit();
        }


         function call_pack(value) {

           
           $('body').removeClass('loaded');
            var myForm2 = '<form id="hidfrm2" action="{{ url('calls/pack') }}/'+value+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm2);
            myForm1 = $('#hidfrm2');
            myForm1.submit();
        }



$(function() {
    $('#favoritesModal').on("show.bs.modal", function (e) {
         $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
         $("#fav-title").html($(e.relatedTarget).data('title'));
    });
});
function f1(objButton){  
 
    var dep_id = objButton.value;

document.getElementById("department").value = dep_id;




}

function f2(objButton){  

  
    var dep_id = objButton.value;

document.getElementById("department").value = dep_id;




}



    function call_depts(){

            var elem = document.getElementById('number').value;
            var dep_id = document.getElementById('department').value;


            
            $('body').removeClass('loaded');
            var myForm1 = '<form id="hidfrm1" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm1);
            myForm1 = $('#hidfrm1');
           myForm1.submit();
  
}


    function call_depts1(){

            var elem = document.getElementById('number1').value;
            var dep_id = document.getElementById('department1').value;


            
            $('body').removeClass('loaded');
            var myForm1 = '<form id="hidfrm1" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm1);
            myForm1 = $('#hidfrm1');
           myForm1.submit();
  
}







    </script>

@endsection
