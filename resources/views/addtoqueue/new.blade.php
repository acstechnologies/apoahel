@extends('layouts.mainappqueue_new')

@section('title', trans('messages.issue').' '.trans('messages.display.token'))

@section('css')
    <style>
        /* .btn-queue{padding:25px;font-size:47px;line-height:36px;height:auto;margin:10px;letter-spacing:0;text-transform:none}
        .modal{max-height: 35%; max-width: 30%; }
        input[type=text]{font-size: 2.5em;}
        .btn-space {
    margin-right: 10px; */
    
}
.card{border-radius:10px;}a:hover { cursor: pointer; }
    </style>
    
<style>
.twPc-div {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #e1e8ed;
    border-radius: 6px;
    height: 200px;
    max-width: 340px; // orginal twitter width: 290px;
}
.subscribe-bg {
    background-image: url("http://13.235.26.152/apollo_qms/assets/img/600x200.jpg");
    background-position: 0 50%;
    background-size: 100% auto;
    border-bottom: 1px solid #e1e8ed;
    border-radius: 4px 4px 0 0;
    /* height: 95px; */
    width: 100%;
}
.subscribe-block {
    display: block !important;
}
.modal-title {
    color: white;
}
.inline-label{
    position: relative;
}
.inline-label label {
  position: absolute;
  top: 0;
  font-size: 80%;
  color: #788991;
  font-weight: bold;
  padding: 6px 12px;
}

.inline-label .form-control{
  padding-top: 24px;
  height: auto;
}

.inline-label .form-control:focus{
    box-shadow: none;
}

.inline-label .form-control:focus + label{
    color: #29abe9;
}
.twPc-button {
    margin: -35px -10px 0;
    text-align: right;
    width: 100%;
}
.twPc-avatarLink {
    background-color: #fff;
    border-radius: 6px;
    display: inline-block !important;
    float: left;
    margin: -30px 5px 0 8px;
    max-width: 100%;
    padding: 1px;
    vertical-align: bottom;
}
.twPc-avatarImg {
    border: 2px solid #fff;
    border-radius: 7px;
    box-sizing: border-box;
    color: #fff;
    height: 72px;
    width: 72px;
}
.twPc-divUser {
    margin: 5px 0 0;
}
.twPc-divName {
    font-size: 18px;
    font-weight: 700;
    line-height: 21px;
}
.twPc-divName a {
    color: inherit !important;
}
.twPc-divStats {
    margin-left: 11px;
    padding: 10px 0;
}
.twPc-Arrange {
    box-sizing: border-box;
    display: table;
    margin: 0;
    min-width: 100%;
    padding: 0;
    table-layout: auto;
}
ul.twPc-Arrange {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
.twPc-ArrangeSizeFit {
    display: table-cell;
    padding: 0;
    vertical-align: top;
}
.twPc-ArrangeSizeFit a:hover {
    text-decoration: none;
}
.twPc-StatValue {
    display: block;
    font-size: 18px;
    font-weight: 500;
    transition: color 0.15s ease-in-out 0s;
}
.twPc-StatLabel {
    color: #8899a6;
    font-size: 10px;
    letter-spacing: 0.02em;
    overflow: hidden;
    text-transform: uppercase;
    transition: color 0.15s ease-in-out 0s;
}
.modal{
    /* background-color: rgba(76, 175, 80, 0.9) ; */
    padding: 0;
    max-height: 100%;
    width: auto;
    margin: auto;
    overflow-y: auto;
    border-radius: 2px;
    opacity:0.9;
    /* will-change: top,opacity; */
}
.card{
    width : 180px;
    height : 130px;
}

.centered{
    position: absolute;
    top: 40%;
    left: 50%;

}
.bottomLeft{
    position:absolute;
    top:40%;
    left:35%;
}
.bottomRight{
    position:absolute;
    top:40%;
    left:65%;
}
.topLeft{
    position: absolute;
    top: 15%;
    left: 50%;

}
.topRight{
    position: absolute;
    top: 65%;
    left: 50%;

}
.page-footer{
    margin-top:none;
}
.center-align{
    word-wrap: break-word;
    
}
.keyboard{
    position: relative;
    top: -40px;
    right: -530px;
    cursor: pointer;
    opacity: 0.3;
}
table {
    display: table;
    border-collapse: separate;
    border-spacing: 2px;
    border-color: grey;
}
.main{
    display:flex;
}
.part{
    flex:50%;
    padding: 10px;
}
[type=checkbox]+label, [type=radio]:checked+label, [type=radio]:not(:checked)+label {
    padding-left: 20px;}
.easy-numpad-number-container > table a{margin:1px;}

.fclose{float:right;color:#fff;cursor:pointer;font-size:18px;line-height:1;}
</style>
   <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
   <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
   
 
@endsection

@section('content')
  <div class=" s12 m6 6 " >
                                
                                    
                                
                               
                            
@if(Session::has('messages'))
<p class="alert alert-success">{{ session('messages') }}</p>
@endif
</div>

{{ Session::get('location_id')}}



<div id="card-stats">
<div class="topLeft ">
<div class="card hoverable">
<div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id01').style.display='block'" >
<i class="fas fa-file-medical-alt fa-3x"></i>   
</div>
                            <div style="padding:20px;" class="card-action light-blue darken-4"  onclick="document.getElementById('id01').style.display='block'">
                                <div class="center-align">
                                    
                                    <a onclick="document.getElementById('id01').style.display='block'" style="text-transform:none;color:#fff">Investigations </a>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class=" s12 m6 l2 topRight">
                        <div class="card hoverable">
                            <div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id02').style.display='block'">
                                
                            <i class="fas fa-capsules fa-3x"></i>
                                
                            </div>
                            <div style="padding:20px;" class="card-action light-blue darken-4">
                                <div class="center-align">
                                    

                                    <a onclick="document.getElementById('id02').style.display='block'" style="text-transform:none;color:#fff">Others </a>
                                </div>
                            </div>
                        </div>
                    </div> 
<div class=" s12 m6 l2 centered">
                        <div class="card hoverable">
                            <div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id03').style.display='block'">
                                
                                <i class="fas fa-user-md fa-3x"></i>
                                
                            </div>
                            <div style="padding:20px;" class="card-action light-blue darken-4">
                                <div class="center-align">
                                    
                                    <a onclick="document.getElementById('id03').style.display='block'" style="text-transform:none;color:#fff">Consultation </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" s12 m6 l2 bottomLeft">
                        <div class="card hoverable">
                            <div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id04').style.display='block'">
                                
                            <i class="fas fa-diagnoses fa-3x"></i>
                                
                            </div>
                            <div  style="padding:20px;" class="card-action light-blue darken-4">
                                <div class="center-align">
                                   
                                    
                                    <a onclick="document.getElementById('id04').style.display='block'" style="text-transform:none;color:#fff">Health Checks </a>

                               
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class=" s12 m6 l2 bottomRight">
                        <div class="card hoverable">
                            <div class="card-content light-blue darken-2 white-text" onclick="document.getElementById('id05').style.display='block'">
                                
                            <i class="fas fa-diagnoses fa-3x"></i>
                                
                            </div>
                            <div style="padding:20px;" class="card-action light-blue darken-4">
                                <div class="center-align">
                                   
                                    <a onclick="document.getElementById('id05').style.display='block'" style="text-transform:none;color:#fff">Corporate checks </a>
                                </div>
                            </div>
                        </div>
                    </div>    
                             


<!-- New Modal -->
<div class="modal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <form method="post" action="update.php" class="ng-pristine ng-valid">
    <div class="modal-content">
      <div class="modal-header subscribe-bg subscribe-block">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Subscribe our Newsletter

</h4>
      </div>
      <div class="modal-body">        
          <fieldset class="form-group inline-label">
            <input type="text" name="name" class="form-control" id="formGroupExampleInput" placeholder="John Doe">
            <label for="formGroupExampleInput">Your name</label>
          </fieldset>
          <fieldset class="form-group inline-label">
            <input type="text" name="email" class="form-control" id="formGroupExampleInput2" placeholder="mail@gmail.com">
            <label for="formGroupExampleInput2">Your email</label>
          </fieldset>
          
          <fieldset class="form-group inline-label">
            Yes: <input type="radio" name="human" value="yes"> No: <input type="radio" name="human" value="no" checked="checked">
            <label for="formGroupExampleInput2">Are you human?:</label>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="addSubscriber" class="btn btn-primary">Subscribe</button>
      </div>
      </form>
    </div>
  </div>
</div>












<div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

        <div class="modal-header subscribe-bg subscribe-block">
       
        <i onclick="document.getElementById('id01').style.display='none'" aria-hidden="true"  class="fclose fa  fa-times"></i>
        <h4 class="modal-title" id="myModalLabel">Mobile Number</h4>
      </div>
      <div class="modal-body ">        
          <fieldset class="form-group inline-label">
            <input type="text" name="number"  class="easy-numpad-output"  class="form-control easy-put" id="number1" placeholder="+91 XXXXX XXXXX" autofocus>
            <input id="department1" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">

            
            <label for="formGroupExampleInput">Mobile Number</label>
            <input id="location_id" type="hidden" name="location_id" placeholder="{{ old('name') }}" value=" {{ Session::get('location_id') }}" data-error=".name">

          </fieldset>
<fieldset>
<div class="text-center">
<input type="submit" class="btn btn-primary " onclick="call_depts1()" value="Submit" name=""></div>
</fieldset>
    <fieldset>
  
</fieldset> 
      </div>
    </div>
  </div>
</div>

<div id="id02" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
<div class="modal-header subscribe-bg subscribe-block">
       
        <i onclick="document.getElementById('id02').style.display='none'" aria-hidden="true"  class="fclose fa fa-times"></i>
        <h4 class="modal-title" id="myModalLabel">Mobile Number</h4>
      </div>
      <div class="modal-body ">        
          <fieldset class="form-group inline-label">
           <input type="text" name="number"  class="easy-numpad-output"  class="form-control easy-put" id="number2" placeholder="+91 XXXXX XXXXX">
            <input id="department2" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
            
          
            <label for="formGroupExampleInput">Mobile Number</label>
            <input id="location_id" type="hidden" name="location_id" placeholder="{{ old('name') }}" value=" {{ Session::get('location_id') }}" data-error=".name">

          </fieldset>
<fieldset>
<div class="text-center">
<input type="submit" class="btn btn-primary " onclick="call_depts2()" value="Submit" name=""></div>
</fieldset>
    <fieldset>

</fieldset> 
      </div>
    </div>
  </div>
</div>

<div id="id03" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
<div class="modal-header subscribe-bg subscribe-block">
      
        <i onclick="document.getElementById('id03').style.display='none'" aria-hidden="true"  class="fclose fa fa-times"></i>
        <h4 class="modal-title" id="myModalLabel">Mobile Number</h4>
      </div>
      <div class="modal-body ">        
          <fieldset class="form-group inline-label">
            
            <input type="text" name="number"  class="easy-numpad-output"  class="form-control easy-put" id="number3" placeholder="+91 XXXXX XXXXX">
            <input id="department3" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
            
          
            <label for="formGroupExampleInput">Mobile Number</label>
            <input id="location_id" type="hidden" name="location_id" placeholder="{{ old('name') }}" value=" {{ Session::get('location_id') }}" data-error=".name">

          </fieldset>
<fieldset>
<div class="text-center">
<input type="submit" class="btn btn-primary " onclick="call_depts3()" value="Submit" name=""></div>
</fieldset>
    <fieldset>
 
</fieldset> 
      </div>
    </div>
  </div>
</div>

<div id="id04" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
<div class="modal-header subscribe-bg subscribe-block">
     
        <i onclick="document.getElementById('id04').style.display='none'" aria-hidden="true"  class="fclose fa fa-times"></i>
        <h4 class="modal-title" id="myModalLabel">Mobile Number</h4>
      </div>
      <div class="modal-body ">        
          <fieldset class="form-group inline-label">
            
            <input type="text" name="number"  class="easy-numpad-output"  class="form-control easy-put" id="number4" placeholder="+91 XXXXX XXXXX">
            <input id="department4" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
            
            <input id="location_id" type="hidden" name="location_id" placeholder="{{ old('name') }}" value=" {{ Session::get('location_id') }}" data-error=".name">


          
            <label for="formGroupExampleInput">Mobile Number</label>
          </fieldset>
<fieldset>
<div class="text-center">
<input type="submit" class="btn btn-primary " onclick="call_depts4()" value="Submit" name=""></div>
</fieldset>
    <fieldset>
  
</fieldset> 
      </div>
    </div>
  </div>
</div>

<div id="id05" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
<div class="modal-header subscribe-bg subscribe-block">
       
        <i onclick="document.getElementById('id05').style.display='none'" aria-hidden="true"  class="fclose fa fa-times"></i>
        <h4 class="modal-title" id="myModalLabel">Mobile Number</h4>
      </div>
      <div class="modal-body ">        
          <fieldset class="form-group inline-label">
           <input type="text" name="number"  class="easy-numpad-output"  class="form-control easy-put" id="number5" placeholder="+91 XXXXX XXXXX">
            <input id="department5" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
            
            <label for="formGroupExampleInput">Mobile Number</label>
            <input id="location_id" type="hidden" name="location_id" placeholder="{{ old('name') }}" value=" {{ Session::get('location_id') }}" data-error=".name">

          </fieldset>
<fieldset>
<div class="text-center">
<input type="submit" class="btn btn-primary " onclick="call_depts5()" value="Submit" name=""></div>
</fieldset>
    <fieldset>
  

</fieldset> 
      </div>
    </div>
  </div>
</div>




    <!-- The Modal -->
    <div class="modal"  id="myModal">
    
    <!-- Modal Header -->
    
    <div class="modal-header" >
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <span class="card-title" style="">{{ trans('Please Enter UH ID') }}</span>
    </div>
        
      <!-- Modal body -->
      
     <div style="margin-top:20px;" class="modal-body">
     <div style="" class="row">
     <div class="col-lg-12">
     <div class="form-group">
     <input type="text" class="form-control" name='number' id="number" placeholder="Enter UH ID">
     </div>
     <input id="department" type="hidden" name="department" placeholder="{{ old('name') }}" value="" data-error=".name">
     <div class="form-group">
     <button type='submit' name="submit" class="btn btn-primary  btn-space pull-right" onclick="call_depts()" >
       Submit
      </button>
     <button type='submit' name="submit" class="btn btn-primary btn-space pull-right" data-dismiss="modal">
      Cancel
      </button>
    </div>
    </div>
    </div>
    </div>
    </div>




    <div class="modal"  id="myModal1">
    
    <!-- Modal Header -->
    
    <div class="modal-header" >
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <span class="card-title" style="">{{ trans('Please Enter Mobile Number') }}</span>
    </div>
        
      <!-- Modal body -->
      
     <div style="margin-top:20px;" class="modal-body">
     <div style="" class="row">
     <div class="col-lg-12">
     <div class="form-group">
     <input type="text" class="form-control" name='number' id="number1" placeholder="Mobile Number">
     </div>
     <input id="department1" type="hidden" name="department" placeholder="{{ old('name') }}" value="{{$departments[0]['id']}}" data-error=".name">
     <div class="form-group">
     <button type='submit' name="submit" class="btn btn-primary  btn-space pull-right" onclick="call_depts1()" >
       Submit
      </button>
     <button type='submit' name="submit" class="btn btn-primary btn-space pull-right" data-dismiss="modal">
      Cancel
      </button>
    </div>
    </div>
    </div>
    </div>
    </div>

  

@endsection







@section('script')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
        $(window).resize(function() {
            $('#main').css({'min-height': $(window).height()-134+'px'});
        });
     

     function call_depts1(){
        var elem = document.getElementById('number1').value;
        var dep_id = document.getElementById('department1').value;
        var qtype = 1;
        var lid = document.getElementById('location_id').value;
        $('body').removeClass('loaded');
        var myForm1 ='<form id="hidfrm1" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'/'+qtype+'/'+lid+'" method="post">{{ csrf_field() }}</form>';
        $('body').append(myForm1);
        myForm1 = $('#hidfrm1');
        myForm1.submit();
       }


     function call_depts2(){
        var elem = document.getElementById('number2').value;
        var dep_id = document.getElementById('department2').value;
        var qtype = 2;
        var lid = document.getElementById('location_id').value;
        $('body').removeClass('loaded');
        var myForm2 = '<form id="hidfrm2" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'/'+qtype+'/'+lid+'" method="post">{{ csrf_field() }}</form>';
        $('body').append(myForm2);
        myForm2 = $('#hidfrm2');
        myForm2.submit();
       }

        function call_depts3(){
        var elem = document.getElementById('number3').value;
        var dep_id = document.getElementById('department3').value;
        var qtype = 3;


       
        var lid = document.getElementById('location_id').value;
        $('body').removeClass('loaded');
        var myForm3 ='<form id="hidfrm3" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'/'+qtype+'/'+lid+'" method="post">{{ csrf_field() }}</form>';
        $('body').append(myForm3);
        myForm3 = $('#hidfrm3');
        myForm3.submit();
       }

        function call_depts4(){
        var elem = document.getElementById('number4').value;
        var dep_id = document.getElementById('department4').value;
        var qtype = 4;

        var lid = document.getElementById('location_id').value;
        $('body').removeClass('loaded');
        var myForm4 = '<form id="hidfrm4" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'/'+qtype+'/'+lid+'" method="post">{{ csrf_field() }}</form>';
        $('body').append(myForm4);
        myForm4 = $('#hidfrm4');
        myForm4.submit();
       }

        function call_depts5(){
        var elem = document.getElementById('number5').value;
        var dep_id = document.getElementById('department5').value;
        var qtype = 5;
        var lid = document.getElementById('location_id').value;
        $('body').removeClass('loaded');
        var myForm5 = '<form id="hidfrm5" action="{{ url('queue/dept') }}/'+dep_id+'/'+elem+'/'+qtype+'/'+lid+'" method="post">{{ csrf_field() }}</form>';
        $('body').append(myForm5);
        myForm5 = $('#hidfrm5');
        myForm5.submit();
       }



  $(document).ready(function(){
          $(".alert").delay(5000).slideUp(5000)
    });





    </script>

@endsection



        
       