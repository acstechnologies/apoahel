@extends('layouts.app')

@section('title', trans('messages.add').' '.trans('messages.mainapp.menu.doctor'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.add') }} {{ trans('messages.mainapp.menu.doctor') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('doctors.index') }}">{{ trans('messages.mainapp.menu.doctor') }}</a></li>
                        <li class="active">{{ trans('messages.add') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('doctors.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="add" action="{{ route('doctors.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input id="name" type="text" name="name" placeholder="{{ trans('messages.mainapp.menu.doctor') }} {{ trans('messages.name') }}" value="{{ old('name') }}" data-error=".name">
                            <div class="name">
                                @if($errors->has('name'))<div class="error">{{ $errors->first('name') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="doctor_id">{{ trans('Doctor_id') }}</label>
                            <input id="doctor_id" type="text" name="doctor_id" placeholder="{{ trans('doctor_id') }}"  data-error=".doctor_id">
                            <div class="doctor_id">
                                @if($errors->has('doctor_id'))<div class="error">{{ $errors->first('doctor_id') }}</div>@endif
                            </div>
                        </div>
                    </div>
                      <div class="row">
            <div class="input-field col s12">
                <label for="specialization" class="active">{{ trans('Specialization') }}</label>
                    <select id="specialization_id" class="browser-default" name="specialization_id" data-error=".specialization_id">
                      <option value="">Select Specialization</option>
                      @foreach($specializations as $specialization )
                      <option value="{{ $specialization->id}}">{{ $specialization->name}} </option>
                      @endforeach
                        
                    </select>
              <div class="specialization">
                    @if($errors->has('specialization'))<div class="error">{{ $errors->first('specialization') }}</div>@endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <label for="hospital_id" class="active">{{ trans('Hospital') }}</label>
                    <select id="hospital_id" class="browser-default" name="hospital_id" data-error=".hospital_id">
                      <option value="">Select Hospital</option>
                      @foreach($hospitals as $hospital )
                      <option value="{{ $hospital->id}}">{{ $hospital->name}} </option>
                      @endforeach 
                    </select>
              <div class="hospital">
                    @if($errors->has('hospital'))<div class="error">{{ $errors->first('hospital') }}</div>@endif
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.save') }}<i class="mdi-content-save left"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#add").validate({
            rules: {
                name: {
                    required: true
                },
                doctor_id: {
                    required: true,
                    digits: true
                },
                specialization_id:{
                    required: true
                   
                },

                hospital_id:{
                    required: true
                    
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    </script>
@endsection
