@extends('layouts.app')

@section('title', trans('messages.mainapp.menu.call'))

@section('css')
    <link href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.mainapp.menu.call') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li class="active">{{ trans('messages.mainapp.menu.call') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

 {{ $user->location_id }}


    <div class="container">
        <div class="row">
            <!-- Total Queues Start -->
            <div class="col s12 m12">
                <div class="card">
                    <div class="card-content" style="font-size:14px">
                        <span class="card-title" style="line-height:0;font-size:22px">{{ trans('messages.call.todays_queue') }}</span>
                        <div class="divider" style="margin:10px 0 10px 0"></div>
                        <table id="call-table" class="display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('Services User') }}</th>
                                    <th>{{ trans('messages.call.number') }}</th>
                                    <th>{{ trans('messages.call.called') }}</th>
                                    <th>{{ trans('Department') }}</th>
                                    <th>{{ trans('Name') }}</th>
                                    <th>{{ trans('messages.mainapp.menu.counter') }}</th>
                                    <th>{{ trans('messages.call.recall') }}</th>
                                </tr>
                            </thead>
                        </table>
                        <form id="new_call" action="{{ route('post_call') }}" method="post">
                            {{ csrf_field() }}
                            
                             <div class="row">
                                <div class="input-field col s12">

                                    <label for="counter" class="active">{{ trans('messages.mainapp.menu.counter') }}</label>
                                   
                                    <div id= 'demo'>                             
                                    </div>
                                    <div id= 'demo1'>                          
                                    </div>
                                </div></div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn waves-effect waves-light right" type="submit">
                                        {{ trans('messages.call.call_next') }}<i class="mdi-navigation-arrow-forward right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Total Queues End -->
            <!--  Hold/Missed Start -->
<!--          <div class="col s12 m12">
                <div class="card">
                    <div class="card-content" style="font-size:14px">
                        <span class="card-title" style="line-height:0;font-size:22px">Missed/Hold</span>
                        <div class="divider" style="margin:10px 0 10px 0"></div>
                        <table id="call-table1" class="display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('Services User') }}</th>
                                    <th>{{ trans('messages.call.number') }}</th>
                                    <th>{{ trans('messages.call.called') }}</th>
                                    <th>{{ trans('Department') }}</th>
                                    <th>{{ trans('Name') }}</th>
                                    <th>{{ trans('messages.mainapp.menu.counter') }}</th>
                                    <th>{{ trans('messages.call.recall') }}</th>
                                </tr>
                            </thead>
                        </table>
                        <form id="new_call" action="{{ route('post_call') }}" method="post">
                            {{ csrf_field() }}
                            
                             <div class="row">
                                <div class="input-field col s12">

                                    <label for="counter" class="active">{{ trans('messages.mainapp.menu.counter') }}</label>
                                   
                                    <div id= 'demo'>                             
                                    </div>
                                    <div id= 'demo1'>                          
                                    </div>
                                </div></div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn waves-effect waves-light right" type="submit">
                                        {{ trans('messages.call.call_next') }}<i class="mdi-navigation-arrow-forward right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> -->
             <!--  Hold/Missed End -->
        </div>
    </div>
@endsection

@section('print')
    @if(session()->has('department_name'))
        <style>#printarea{display:none;text-align:center}@media print{#loader-wrapper,header,#main,footer,#toast-container{display:none}#printarea{display:block;}}@page{margin:0}</style>
        <div id="printarea" style="line-height:1.25">
            
            <span style="font-size:27px; font-weight: bold">{{ $company_name }}</span><br>

            <span style="font-size:25px">{{ session()->get('department_name') }}</span><br>
            <span style="font-size:20px">Your Token Number</span><br>
            <span><h3 style="font-size:70px;font-weight:bold;margin:0;line-height:1.5">{{ session()->get('number') }}</h3></span>
            <span style="font-size:20px">Please wait for your turn</span><br>
            <span style="font-size:20px">Total customer(s) waiting: {{ session()->get('total')-1 }}</span><br>
            <span style="float:left">{{ \Carbon\Carbon::now()->format('d-m-Y') }}</span><span style="float:right">{{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
        </div>
        <script>
            window.onload = function(){window.print();}
        </script>
    @endif






                   




@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script>
        $("#new_call").validate({
/*            rules: {
                user: {
                    required: true,
                    digits: true
                },
                department: {
                    required: true,
                    digits: true
                },
                counter: {
                    required: true,
                    digits: true
                },
            },*/
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
        function call_dept(value) {
            $('body').removeClass('loaded');
            var myForm1 = '<form id="hidfrm1" action="{{ url('calls/dept') }}/'+value+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm1);
            myForm1 = $('#hidfrm1');
            myForm1.submit();
        }


         function call_pack(value) {
            $('body').removeClass('loaded');
            var myForm2 = '<form id="hidfrm2" action="{{ url('calls/pack') }}/'+value+'" method="post">{{ csrf_field() }}</form>';
            $('body').append(myForm2);
            myForm1 = $('#hidfrm2');
            myForm1.submit();
        }





       function recall(call_id) {


        
            $('body').removeClass('loaded');
            var data = 'call_id='+call_id+'&_token={{ csrf_token() }}';


            $.ajax({
                type:"POST",
                url:"{{ route('post_recall') }}",
                data:data,
                cache:false,

                success: function(response) {
                 
                    location.reload();
                }
            });
        }



        $(function() {

            var calltable = $('#call-table').dataTable({
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_",
                    "sSearch": "Search"
                },
                "columnDefs": [{
                    "targets": [ -1 ],
                    "searchable": false,
                    "orderable": false
                }],
                "ajax": "{{ url('help') }}",


                "columns": [
                    { "data": "id" },
                    { "data": "service_user" },
                    { "data": "number" },
                    { "data": "called" },
                    { "data": "counter1" },
                    {  "data" : "name"},
                    { "data": "counter" },
                    { "data": "recall" }
                ]
            });


  /*          setInterval(function(){
                calltable.api().ajax.reload(null,false);
            }, 3000);*/
        });









//calls/getDeparts


    </script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
   /* (function() {
        $("#basin_id").change(function() {
            $.getJSON("calls/getDeparts", function(data) {
                var $stations = $("#station_id");
                     $.each(data, function(index, value) {
                    $stations.append('<option value="' + index +'">' + value + '</option>');
                });
            });
        });
    });*/


 


   /* (function () {

        setInterval(function () {


            axios.get('calls/getDeparts',)
                .then(function(response){
                        document.querySelector('#partial')
                                .innerHtml(response.data);
                }); // do nothing for error - leaving old content.
            }); 
        }, 5000); // milliseconds
*/


function doRefresh(){
   


$.get("{{ url('calls/getDeparts')}}", 
               
                function(data) {

                    //Total Queue   '+data.length;
                    var model = $('#model');
                    model.empty();

                 if(data.length != 0){   
                

                document.getElementById("demo").innerHTML = '<input type="hidden" name="department" value ="'+data[0]["department_id"] +'" > ';
                
                document.getElementById("demo1").innerHTML = '<input type="text" name="qid" value ="'+data[0]["id"] +'" > <BR> Total Queue   '+data.length+'<BR> Proceed to Call';   
                    }else{

                      

                document.getElementById("demo").innerHTML = '<input type="hidden" name="department" value ="null" > <BR> No Queues';
                 
                    }


                });
                
                }



   $(function() {
       setInterval(doRefresh, 3000);
    });








</script>



@endsection
