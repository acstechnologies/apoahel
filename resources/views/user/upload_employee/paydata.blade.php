@extends('layouts.default')

    @section('content')
       

    <div class="row">
            <div class="col-md-4">
                <div class="box-info full">


            <div class="table-responsive">
                        <table class="table table-hover table-striped table-ajax-load" id="clock-table" data-source="">
                            <thead>
                                <tr>
                                    <th>{!! trans('Pay Data Upload') !!}</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th></th>
                            <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>


                    <div style="padding:10px;" id="clock-button">


                             </div>

                    <div class="clear"></div>
                    <br />
                    @if(Entrust::can('upload_attendance'))
                        <div style="padding:10px;">
                        {!! Form::open(['files' => 'true','route' => 'salary.paydataUpload','role' => 'form', 'class'=>'form-inline upload-leaves-form','id' => 'upload-leaves-form', 'data-submit' => 'noAjax']) !!}
                          <div class="form-group">
                            <label class="sr-only" for="file">{!! trans('messages.upload_file') !!}</label>
                            <input type="file" name="file" id="file" class="btn btn-info" title="{!! trans('messages.select').' '.trans('messages.file') !!}">
                          </div>
                          {!! Form::submit(trans('messages.upload'),['class' => 'btn btn-primary']) !!}
                          <div class="help-block"><strong>{!! trans('messages.note') !!}</strong> {!! trans('messages.only_csv_file_allowed') !!} <br /><a href="#" data-toggle="modal" data-target="#myModal"></a></div>
                          </div>
                        {!! Form::close() !!}
                    @endif
                </div>
            </div>


    </div></div></div>


                    
            
        

       

        

        


        



        
                
            

    @stop
