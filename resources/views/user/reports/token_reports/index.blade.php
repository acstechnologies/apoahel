@extends('layouts.app')

@section('title', trans('messages.mainapp.menu.call'))

@section('css')
    <link href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
   <style type="text/css">
   td.details-control {
    background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
}
   </style>
@endsection

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.mainapp.menu.call') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li class="active">{{ trans('messages.mainapp.menu.call') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card-panel">
                    <div class="row">
                        <div class="col s9">
                            <span style="line-height:0;font-size:22px;font-weight:300">{{ trans('messages.report') }}</span>
                        </div>
                        <div class="col s3">
                            
                        </div>
                    </div>
                    <div class="divider" style="margin:15px 0 10px 0"></div>
                    <table id="example" class="display" cellspacing="0">
                   
    <thead>
        <tr>
            <th></th>
            <th>Servieses</th>
            <th>{{ trans('messages.call.number') }}</th>
            <th>{{ trans('messages.call.called') }}</th>
           
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
           
        </tr>
    </tfoot>
</table>
                </div>
            </div>
        </div>
    </div>









@endsection

@section('print')
    @if(session()->has('department_name'))
        <style>#printarea{display:none;text-align:center}@media print{#loader-wrapper,header,#main,footer,#toast-container{display:none}#printarea{display:block;}}@page{margin:0}</style>
        <div id="printarea" style="line-height:1.25">
            
            <span style="font-size:27px; font-weight: bold">{{ $company_name }}</span><br>

            <span style="font-size:25px">{{ session()->get('department_name') }}</span><br>
            <span style="font-size:20px">Your Token Number</span><br>
            <span><h3 style="font-size:70px;font-weight:bold;margin:0;line-height:1.5">{{ session()->get('number') }}</h3></span>
            <span style="font-size:20px">Please wait for your turn</span><br>
            <span style="font-size:20px">Total customer(s) waiting: {{ session()->get('total')-1 }}</span><br>
            <span style="float:left">{{ \Carbon\Carbon::now()->format('d-m-Y') }}</span><span style="float:right">{{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
        </div>
        <script>
            window.onload = function(){window.print();}
        </script>
    @endif






                   




@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>

    var a;
    $.get("{{ url('calls/getDeparts')}}",function(data) 
    {
        var model = $('#model');
        model.empty();
        console.log(data);
       
       document.getElementById("demo").innerHTML = '<input type="text" name="department" value ="'+data[0]["department_id"] +'" > <BR> proceed';
    });



    function format ( d ) {

       
        var id =0;
         var id = d.department_id;
         var tno =d.numbers;
 

var myData ='NUll';

var myData = getSomething();


function getSomething(){
    
  
             $.ajax({
                    url: 'myform1/ajax/'+id+'/'+tno,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        
                       //alert(data[0]['name']);
                       result  = data;
                      
                      // return data;
                       //  alert(result);  
                    }

              });
   return  result ;
    
}

/* $.get('myform1/ajax/'+id+'/'+tno, function(data, status){
            alert("Data: " + data);
        });
*/





//var i =5;
    var text = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
text +='<tr><td> Servieses </td><td> Number </td><td> Called </td><td> counter </td><td> Re Issue </td></tr>';

for (var i = 0; i < myData.length; i++) {


    
    
    text +='<tr><td> '+ myData[i]['department']+'</td> <td>'+ myData[i]['number'] +'</td> <td>'+ myData[i]['called']+'</td> <td>'+ myData[i]['counter1'] +'</td> <td>'+ myData[i]['recall'] +'</td></tr>';
}
text +='</table>';


myData =null;
return text;


  // }




  //for (var i=0; i <= myData.length; i++ )




//{


       /* return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+myData[0]['number']+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+myData[0]['number']+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';

*/
//}




/*
                        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.department+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.department+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';*/







    // `d` is the original data object for the row
  
}

$(document).ready(function() {
    var table = $('#example').DataTable({
       'ajax': "{{ url('help/help')}}",
        'columns': [
            {
                'className':      'details-control',
                'orderable':      false,
                'data':           null,
                'defaultContent': ''
            },
            { 'data': 'id' },
            { 'data': 'department' },
            { 'data': 'number' },
           
        ],
        'order': [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function(){

      
       

        var tr = $(this).closest('tr');
        var row = table.row( tr );

     
        if(row.child.isShown()){
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
            row.empty();
        }
    });

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
       
        // Enumerate all rows
        table.rows().every(function(){
            // If row has details collapsed
            if(!this.child.isShown()){
                // Open this row
                this.child(format(this.data())).show();
                $(this.node()).addClass('shown');
            }
        });
    });

    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Enumerate all rows
        table.rows().every(function(){
            // If row has details expanded
            if(this.child.isShown()){
                // Collapse row details
                this.child.hide();
                $(this.node()).removeClass('shown');
            }
        });
    });
});

   
   

  </script>



@endsection

<script>






function fnFormatDetails ( oTable, nTr, dMon )
{
    var aData = oTable.fnGetData( nTr );
    var dts=getData( dMon );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'; 
    sOut+=dts;
    sOut += '</table>';
    return sOut;
   
}

function getData(dMon)
{  
    var result;
    $.ajax({
        url: 'invoice/getSubInvoiceDetails',
        data: {'month' : dMon},        
        method: 'POST', // or GET        
        async: false,   
        success: function(data) {            
            var ParsedObject = JSON.parse(data);            
            for (var i = 0; i < ParsedObject.length; i++) {               
                result += '<tr><td> '+ParsedObject[i]['Vendor'] +'</td><td>$'+ ParsedObject[i]['Total'] +'</td></tr>';
            }
        },
        error : function (xmlHttpRequest, textStatus, errorThrown) {
            alert("Error " + errorThrown);
            if(textStatus==='timeout')
                alert("request timed out");
       }
   
    });  
   /* $.getJSON('invoice/getSubInvoiceDetails/?month=' + dMon ,
        function (data) {
           //console.log(data);
           //alert(data);
           //result= JSON.parse(data);
           //alert(result);
           for (var i = 0; i < data.length; i++) {
            //console.log(stringify[i]['price']);
            result += '<tr><td> '+ data[i][6] +'</td><td>$'+ data[i][4] +'</td></tr>';
        }
        }
    );*/    
    return result;    
}

$(document).ready(function() {

    $('#dynamic-table, #dynamic-table2').dataTable( {
        "aaSorting": [[ 4, "desc" ]]
    } );

    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement( 'th' );
    var nCloneTd = document.createElement( 'td' );
    nCloneTd.innerHTML = '<img src="assets/images/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#hidden-table-info tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $(document).on('click','#hidden-table-info tbody td img',function () {
        var nTr = $(this).parents('tr')[0];       
        var id = $(this).closest('tr').attr("val");
                
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "assets/images/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "assets/images/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr,id), 'Details' );
        }
    } );
} );
</script>