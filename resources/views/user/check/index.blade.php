@extends('layouts.mainappuser')

@section('title', trans('My Check List'))

@section('css')
    <link href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <style>
     nav {
    background-color: #ffffff;
    width: 100%;
    /* height: 56px; */
    line-height: 56px;
} 
.row-centered {
    text-align:center;
}
.col-centered {
    display:inline-block;
    float:none;
    /* reset the text-align */
    /* text-align:left; */
    /* inline-block space fix */
    /* margin-right:-4px; */
    text-align: center;
    /* background-color: #ccc;
    border: 1px solid #ddd; */
}
    </style>
@endsection

@section('content')
    
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <!-- <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('Check List') }}</h5> -->
                    
                </div>
            </div>
        </div>
  




    <div class="container">
        <div class="row">
                       <div class="col s12 m9">
                <div class="card row-centered">
                    <div class="card-content" style="font-size:14px">
                    <div class="col-centered">
                    <span style="float:left;"><strong>NAME:</strong> {{  $current_position['user_name'] }}</span><br>
                    <span style="float:left;"><strong>UHID:</strong> {{  $current_position['mr_no'] }}</span><br>
                    <span style="float:left;"><strong>Token:</strong> {{ $current_position['tone_number'] }}</span>
                    
                    <div>
                    <span style="float:left;"><strong>Service At:</strong>  {{ $current_position['department'] }}</span>
                    
                    </div><div>
                    <span style="float:left;"><strong>Status:</strong> <?php echo $current_position['called'] ?></span>
                    </div>
                    <div><span><strong>
                         <?php

                         if($called == 0){

                            echo 'Please wait at';

                          ?>
                         </div> </strong></span><div>
                         <span style="float:left;"><strong> {{ $counter['name'] }} </strong> </span>

                         </div><div>
                         <span style="float:left;"><strong>Approximate Time:</strong> <?php echo $current_position['approx_time'] ?>Minutes</span>
                         </div>
                         <?php
                         } ?>

                         </div></div>
                         


                        <table class="table" id="table">
    <!-- <thead>
        <tr>
            
            <th class="text-center">Service At :  {{ $current_position['department'] }}</th>
            <th class="text-center">Token : {{ $current_position['tone_number'] }} </th>
            <th class="text-center">Status : <?php echo $current_position['called'] ?></th>
        </tr>
    </thead> -->
    <tbody>
                        
                        
                        <div class="divider" style="margin:10px 0 10px 0"></div>
                        <table id="call-table" class="display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="center">
                                    <i style="color:blue;margin-right:10px;" class="fas fa-circle">&nbsp;Waiting</i>
                    <i style="color:green;margin-right:10px;" class="fas fa-circle">&nbsp;Completed</i>
                    <i style="color:orange;margin-right:10px;" class="fas fa-circle">&nbsp;In-Queue</i>
                                    </th>
                                 
                                </tr>
                            </thead>
                        </table>
                    


    <table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Service</th>
            <!-- <th class="text-center">Service Name</th> -->
            <th class="text-center">Status</th>
        </tr>
    </thead>
    <tbody>

       
       @foreach($mychecklist as $mychecklists)
<tr >
    <td>{{$mychecklists['id']}}</td>
    <td>{{$mychecklists['department']}}</td>
   <?php // <td>{{$mychecklists['name']}}</td> ?>
    <td><?php echo $mychecklists['called'] ?></td>
   
   
</tr>
@endforeach
    </tbody>
</table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('print')
    @if(session()->has('department_name'))
        <style>#printarea{display:none;text-align:center}@media print{#loader-wrapper,header,#main,footer,#toast-container{display:none}#printarea{display:block;}}@page{margin:0}</style>
        <div id="printarea" style="line-height:1.25">
            
            <span style="font-size:27px; font-weight: bold">{{ $company_name }}</span><br>

            <span style="font-size:25px">{{ session()->get('department_name') }}</span><br>
            <span style="font-size:20px">Your Token Number</span><br>
            <span><h3 style="font-size:70px;font-weight:bold;margin:0;line-height:1.5">{{ session()->get('number') }}</h3></span>
            <span style="font-size:20px">Please wait for your turn</span><br>
            <span style="font-size:20px">Total customer(s) waiting: {{ session()->get('total')-1 }}</span><br>
            <span style="float:left">{{ \Carbon\Carbon::now()->format('d-m-Y') }}</span><span style="float:right">{{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
        </div>
        <script>
            window.onload = function(){window.print();}
        </script>
    @endif






                   




@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script>
       








//calls/getDeparts


    </script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
  



   






   setTimeout(function() {
  location.reload();
}, 30000);




</script>



@endsection
