@extends('layouts.app')

@section('title', trans('messages.edit').' '.trans('messages.mainapp.menu.department'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('messages.mainapp.menu.department') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('departments.index') }}">{{ trans('messages.mainapp.menu.department') }}</a></li>
                        <li class="active">{{ trans('messages.edit') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('departments.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="edit" action="{{ route('departments.update', ['departments' => $department->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input id="name" type="text" name="name" placeholder="{{ trans('messages.mainapp.menu.department') }} {{ trans('messages.name') }}" value="{{ $department->name }}" data-error=".name">
                            <div class="name">
                                @if($errors->has('name'))<div class="error">{{ $errors->first('name') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <label for="location" class="active">{{ trans('Location') }}</label>
                            <select id="location_id" class="browser-default" name="location_id" data-error=".location_id">
                            @foreach($locations as $location)
                                @if(session()->has('location') && ($location->id==session()->get('location')))
                                <option value="{{ $location->id }}" {{ $department->Location->id == $location->id ? 'selected="selected"' : '' }}>{{ $location->name }}</option>
                                @else
                                <option value="{{ $location->id }}" {{ $department->Location->id == $location->id ? 'selected="selected"' : '' }}>{{ $location->name }}</option>
                                @endif
                            @endforeach
                            </select>
              <div class="location">
                    @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                </div>
                        <!-- <div class="input-field col s12">
                            <label for="location">{{ trans('messages.department.location') }}</label>
                            <input id="location" type="text" name="location" placeholder="{{ trans('messages.mainapp.menu.department') }} {{ trans('messages.department.location') }}" value="{{ $department->Location->name }}" data-error=".location">
                            <div class="location">
                                @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                            </div>
                        </div> -->
                    </div>
                </div>
                     <div class="row">
                        <div class="input-field col s12">
                            <label for="counter" class="active">{{ trans('messages.mainapp.menu.counter') }}</label>
                        <select id="counter_id" class="browser-default" name="counter_id" data-error=".counter">
                        @foreach($counters as $counter)
                        <option value="{{ $counter->id }}" {{ $department->counter_id == $counter->id ? 'selected="selected"' : '' }}>{{ $counter->name }}</option>
                        @endforeach
                    </select>
              <div class="counter">
                    @if($errors->has('counter'))<div class="error">{{ $errors->first('counter') }}</div>@endif
                </div>
            </div>
                    </div> 
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="letter">{{ trans('messages.department.letter') }}</label>
                            <input id="letter" type="text" name="letter" placeholder="{{ trans('messages.department.letter') }}" value="{{ $department->letter }}" data-error=".letter">
                            <div class="letter">
                                @if($errors->has('letter'))<div class="error">{{ $errors->first('letter') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="start">{{ trans('messages.department.start') }}</label>
                            <input id="start" type="text" name="start" placeholder="{{ trans('messages.department.start') }}" value="{{ $department->start }}" data-error=".start">
                            <div class="start">
                                @if($errors->has('start'))<div class="error">{{ $errors->first('start') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#edit").validate({
            rules: {
                name: {
                    required: true
                },
                start: {
                    required: true,
                    digits: true
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax1')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#counter_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });
    </script>
@endsection
