@extends('layouts.app')

@section('title', trans('messages.edit').' '.trans('messages.mainapp.menu.doctor'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('messages.mainapp.menu.doctor') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('doctors.index') }}">{{ trans('messages.mainapp.menu.doctor') }}</a></li>
                        <li class="active">{{ trans('messages.edit') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('doctors.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="edit" action="{{ route('doctors.update', ['doctors' => $doctor->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input id="name" type="text" name="name" placeholder="{{ trans('messages.mainapp.menu.doctor') }} {{ trans('messages.name') }}" value="{{ $doctor->name }}" data-error=".name">
                            <div class="name">
                                @if($errors->has('name'))<div class="error">{{ $errors->first('name') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="service_id">{{ trans('messages.doctor.service_id') }}</label>
                            <input id="service_id" type="text" name="service_id" placeholder="{{ trans('messages.doctor.service_id') }}" value="{{ $doctor->service_id }}" data-error=".service_id">
                            <div class="service_id">
                                @if($errors->has('service_id'))<div class="error">{{ $errors->first('service_id') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <label for="location" class="active">{{ trans('Location') }}</label>
                            <select id="location_id" class="browser-default" name="location_id" data-error=".location_id">
                            @foreach($locations as $location)
                                @if(session()->has('location') && ($location->id==session()->get('location')))
                                <option value="{{ $location->id }}" {{ $doctor->Location->id == $location->id ? 'selected="selected"' : '' }}>{{ $location->name }}</option>
                                @else
                                <option value="{{ $location->id }}" {{ $doctor->Location->id == $location->id ? 'selected="selected"' : '' }}>{{ $location->name }}</option>
                                @endif
                            @endforeach
                            </select>
              <div class="location">
                    @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                </div>
                    </div>
                </div>
                <div class="row">
                       <div class="input-field col s12">
                        <label for="group_id" class="active">{{ trans('Department Group') }}</label>
                            <select id="group_id" class="browser-default" name="group_id" data-error=".group_id" >
                            @foreach($departmentGrps as $departmentGrp)
                                @if(session()->has('departmentGrp') && ($departmentGrp->id==session()->get('departmentGrp')))
                                <option value="{{ $departmentGrp->id }}" {{ $doctor->group_id == $departmentGrp->id ? 'selected="selected"' : '' }}>{{ $departmentGrp->name }}</option>
                                @else
                                <option value="{{ $departmentGrp->id }}" {{ $doctor->group_id == $departmentGrp->id ? 'selected="selected"' : '' }}>{{ $departmentGrp->name }}</option>
                                @endif
                            @endforeach
                            </select>
              <div class="location">
                    @if($errors->has('departmentGrp'))<div class="error">{{ $errors->first('departmentGrp') }}</div>@endif
                </div>
            </div>
                </div>
                   
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#edit").validate({
            rules: {
                name: {
                    required: true
                },
                start: {
                    required: true,
                    digits: true
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax1')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#counter_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });
    </script>
@endsection
