@extends('layouts.app')

@section('title', trans('Bill Cancellation'))

@section('css')
    <link href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('Bill Cancellation') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li class="active">{{ trans('messages.mainapp.menu.doctor') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card-panel">
                    <a class="btn-floating waves-effect waves-light tooltipped" href="{{ route('doctors.create') }}" data-position="top" data-tooltip="{{ trans('messages.add') }} {{ trans('messages.mainapp.menu.doctor') }}"><i class="mdi-content-add left"></i></a>
                    <div class="divider" style="margin:15px 0 10px 0"></div>
                    <table id="doctor-table" class="display" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:40px">#</th>
                                <th>{{ trans('Bill No') }}</th>
                                <th>{{ trans('Patient Name') }}</th>
                                <th>{{ trans('MR No') }}</th>
                                <th>{{ trans('Phone Number') }}</th>
                                <th style="width:63px">{{ trans('messages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bills as $bill)
 

                                    <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $bill->bill_no }}</td>
                                    <td>{{ $bill->patient_name }}</td>
                                    <td>{{ $bill->mr_no }}</td>
                                    <td>{{ $bill->patient_phone }}</td>
                                    <td>
                                        <?php if($bill->status ==1) {?>
                                        <a class="btn-floating btn-action waves-effect waves-light orange tooltipped" href="{{ route('billcancellation.edit',['id' => $bill->bill_no]) }}" data-position="top" data-tooltip="{{ trans('Revoke') }}"><i class="mdi-navigation-check"></i></a>
                                    <?php }else{?>
                                        <a class="btn-floating btn-action waves-effect waves-light red tooltipped frmsubmit" href="{{ route('billcancellation.destroy',['id' => $bill->bill_no]) }}" data-position="top" data-tooltip="{{ trans('Cancel') }}" method="DELETE"><i class="mdi-navigation-close"></i></a>
                                   <?php  } ?>

                                    </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script>
        $(function() {
            $('#doctor-table').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_",
                    "sSearch": "Search"
                },
                "columnDefs": [{
                    "targets": [ -1 ],
                    "searchable": false,
                    "orderable": false
                }]
            });
        });
    </script>
@endsection
