@extends('layouts.app')

@section('title', trans('messages.edit').' '.trans('department_id'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('department') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('doctors.index') }}">{{ trans('department') }}</a></li>
                        <li class="active">{{ trans('messages.edit') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

<div class="container">
        <div class="row">
            <div class="col s12 m9 offset-m1" style="padding-top:10px;padding-bottom:10px">
               
                
                <h2 class="breadcrumbs-title col s6" style="margin:.82rem 0 .656rem">{{ trans('Your current Department- ') }} {{ $user_department }}</h2>
                
             
                
                
            </div></div></div>
              
                



    <div class="container">
        <div class="row">
            <div class="col s12 m9 offset-m1" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('updatecounter.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="edit" action="{{ route('updatecounter.update', ['user' => $user->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                <div class="input-field col s4">
                <label for="counter" class="active">{{ trans('Department') }}</label>
                    <select id="department_id" class="browser-default" name="department_id" data-error=".location_id">
                         <option value="">{{ trans('messages.select') }} {{ trans('Department') }}</option>
                        @foreach( $departments as  $department)
                        @if(session()->has('department') && ($user->department_id==session()->get('department')))
                        <option value="{{ $user->department_id }}" selected>{{ $user->department_id }}</option>
                        @else
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endif
                        @endforeach
                    </select>
              <div class="counter">
                    @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                </div>
            </div> 
                        
                   
                  
                   
                    
                   
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#edit").validate({
            rules: {
                name: {
                    required: true
                },
                start: {
                    required: true,
                    digits: true
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax1')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#counter_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });
    </script>
@endsection
