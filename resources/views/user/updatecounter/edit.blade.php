@extends('layouts.app')

@section('title', trans('messages.edit').' '.trans('messages.mainapp.menu.doctor'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('messages.mainapp.menu.doctor') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('doctors.index') }}">{{ trans('messages.mainapp.menu.doctor') }}</a></li>
                        <li class="active">{{ trans('messages.edit') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m9 offset-m1" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('hcconsultation.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="edit" action="{{ route('hcconsultation.update', ['consultations' => $consultations[0]['bill_no']]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                   <?php $i = 1; ?>
                   @foreach ($consultations as $consultations)
                    <div class="row">
                      

                         <div class="input-field col s3">
                            <label for="name">{{ $consultations->activity_description }}</label>
                            <?php
                           
                             $data = DB::select(DB::raw('select * from users where username ="'.$consultations->service_id.'"'));

                            
                            if( $data == NULL)
                            {
                               


                           ?>
                           <input id="name" type="text" name="names"  value="{{  $consultations->activity_description }}" readonly>
                           
                       <?php }else{ ?>

                        <input id="name" type="text" name="names"  value="{{  $data[0]->name}}" readonly>

                        
                        
                    <?php

                    

                     }


                        ?>

                        </div>



























                        <div class="input-field col s4">
                            <label for="name">{{ trans('Service') }}</label>
                            <input id="name" type="text" name="name[<?php echo $i; ?>]" placeholder="{{ trans('messages.mainapp.menu.doctor') }} {{ trans('messages.name') }}" value="{{ $consultations->service_id }}" readonly>

                        </div>






                         <div class="input-field col s4">
                <label for="counter" class="active">{{ trans('Doctor') }}</label>
                    <select id="doctor_iddoctor_id[<?php echo $i; ?>]" class="browser-default" name="doctor_id[<?php echo $i; ?>]" data-error=".location_id">
                         <option value="">{{ trans('messages.select') }} {{ trans('messages.call.user') }}</option>
                        @foreach($doctors as $doctor)
                        @if(session()->has('location') && ($location->id==session()->get('location')))
                        <option value="{{ $doctor->id }}" selected>{{ $doctor->name }}</option>
                        @else
                        <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                        @endif
                        @endforeach
                    </select>
              <div class="counter">
                    @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                </div>
            </div> 
                        
                    </div>
                     <?php $i++; ?>
                    @endforeach
                    
                   
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#edit").validate({
            rules: {
                name: {
                    required: true
                },
                start: {
                    required: true,
                    digits: true
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax1')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#counter_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });
    </script>
@endsection
