@extends('layouts.app')

@section('title', trans('messages.change').' '.trans('messages.users.password'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('messages.mainapp.menu.users') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        <li><a href="{{ route('users.index') }}">{{ trans('messages.mainapp.menu.users') }}</a></li>
                        <li class="active">{{ trans('messages.change') }} {{ trans('messages.users.password') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="container">
        <div class="row">
            <div class="col s12 m9 offset-m1" style="padding-top:10px;padding-bottom:10px">
                <a class="btn-floating waves-effect waves-light orange tooltipped right" href="{{ route('hcconsultation.index') }}" data-position="top" data-tooltip="{{ trans('messages.cancel') }}"><i class="mdi-navigation-arrow-back"></i></a>
                <form id="edit" action="{{ route('hcconsultation.update', ['consultations' => $consultations[0]['bill_no']]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                   <?php $i = 1; ?>
                   @foreach ($users as $users)
                    <div class="row">
                      

                         <div class="input-field col s3">
                            <label for="name">User Name</label>
                           
                        <input id="name" type="text" name="names"  value="{{  $user->name }}" readonly>
                           
                       

                       

                        
                        
                    
                        </div>



























                     
                    @endforeach
                    
                   
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#pass").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });



        jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#department_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });
      
jQuery(document).ready(function($){
    $('#location_id').change(function(){

      $.get("{{ url('select-ajax1')}}", 
            { option: $(this).val() },

               function(data) {
                   
                    var model = $('#counter_id');
                    model.empty();

                    $.each(data, function(index, element) {
                        model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });
                });
        });
    });

  
    </script>
@endsection
