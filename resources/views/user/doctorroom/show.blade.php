@extends('layouts.app')

@section('title', trans('messages.change').' '.trans('Room'))

@section('content')
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title col s5" style="margin:.82rem 0 .656rem">{{ trans('messages.edit') }} {{ trans('messages.mainapp.menu.users') }}</h5>
                    <ol class="breadcrumbs col s7 right-align">
                        <li><a href="{{ route('dashboard') }}">{{ trans('messages.mainapp.menu.dashboard') }}</a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>

    
     <div class="container">
        <div class="row">
            <div class="col s12 m9 offset-m1" style="padding-top:10px;padding-bottom:10px">
                
                <form id="update" action="{{ route('doctorroom.update', ['location' => $locations->id]) }}" method="post">

                    
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <?php $i = 1; ?>
                   @foreach ($users as $user)
                    <div class="row">
                      

                         <div class="input-field col s3">
                         <label for="name">User Name</label>
                         <input id="name" type="text" name="name[<?php echo $i; ?>]"  value="{{  $user->name }}" readonly>
                          
                         </div>


                         <div class="input-field col s1">
                         
                           <input id="user_ids" type="number" name="user_ids[<?php echo $i; ?>]"  value="{{  $user->id }}"  readonly>
                         </div>


                          
                           <div class="input-field col s4">
                            <label for="name">{{ trans('Department') }}</label>
                            <input id="name" type="text" name="departments[<?php echo $i; ?>]"  value="{{  $user->Counter->name }}" readonly>

                        </div>


                         <div class="input-field col s4">
                <label for="counter" class="active">{{ trans('ROOM') }}</label>
                    <select id="counters[<?php echo $i; ?>]" class="browser-default" name="counters[<?php echo $i; ?>]" data-error=".location_id">
                         <option value="">{{ trans('messages.select') }} {{ trans('Room') }}</option>
                        @foreach($counters as $counter)
                        
                        <option value="{{ $counter->id }}">{{ $counter->name }}</option>
                       
                        @endforeach
                    </select>
              <div class="counter">
                    @if($errors->has('location'))<div class="error">{{ $errors->first('location') }}</div>@endif
                </div>
            </div> 



                     </div>


 <?php $i++; ?>
                     
                    @endforeach
                    
                   
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light right" type="submit">
                                {{ trans('messages.update') }}<i class="mdi-action-swap-vert left"></i>
                            </button>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

  
    </script>
@endsection
