@extends('layouts.mainapp')


@section('menu')
    <li class="bold{!! Request::is('dashboard*') ? ' active' : '' !!}"><a href="{{ route('dashboard') }}" class="waves-effect waves-cyan truncate">
        <i class="mdi-hardware-laptop"></i> {{ trans('messages.mainapp.menu.dashboard') }}</a>
    </li>
   @if($user->role != 'R')    
    <li class="bold{!! Request::is('calls*') ? ' active' : '' !!}"><a href="{{ route('calls') }}"  target="_new" class="waves-effect waves-cyan truncate">
        <i class="mdi-communication-chat"></i> {{ trans('messages.mainapp.menu.call') }}</a>
    </li>
@endif
    @can('access', \App\Models\Department::class)
        <li class="bold{!! Request::is('departments*') ? ' active' : '' !!}"><a href="{{ route('departments.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('messages.mainapp.menu.department') }}</a>
        </li>
   


        <li class="bold{!! Request::is('locations*') ? ' active' : '' !!}"><a href="{{ route('locations.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('messages.mainapp.menu.location') }}</a>
        </li>

        <li class="bold{!! Request::is('departGrps*') ? ' active' : '' !!}"><a href="{{ route('departmentGrps.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('messages.mainapp.menu.departmentGrp') }}</a>
        </li>
        
        <li class="bold{!! Request::is('doctors*') ? ' active' : '' !!}"><a href="{{ route('doctors.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('messages.mainapp.menu.doctor') }}</a>
        </li>
 @endcan

    @if($user->role != 'R')    
    <li class="bold{!! Request::is('billcancellation*') ? ' active' : '' !!}"><a href="{{ route('billcancellation.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('Bill Cancellation') }}</a>
        </li>
         <li class="bold{!! Request::is('hcconsultation*') ? ' active' : '' !!}"><a href="{{ route('hcconsultation.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('HC Consultation') }}</a>
        </li>

        <li class="bold{!! Request::is('updatecounter*') ? ' active' : '' !!}"><a href="{{ route('updatecounter.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('Update Billing Counter') }}</a>
        </li>

          <li class="bold{!! Request::is('doctorroom*') ? ' active' : '' !!}"><a href="{{ route('doctorroom.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('Floor Mapping') }}</a>
        </li>

@endif






          @if($user->role == 'A' || $user->role == 'CM')
        <li class="bold{!! Request::is('counters*') ? ' active' : '' !!}"><a href="{{ route('counters.index') }}" class="waves-effect waves-cyan truncate">
            <i class="mdi-action-view-quilt"></i> {{ trans('messages.mainapp.menu.counter') }}</a>
        </li>
        @endif



     <li class="bold{!! Request::is('specializations*') ? ' active' : '' !!}"><a href="{{ route('specializations.index')}} "  class="waves-effect waves-cyan truncate">
            <i class="mdi-communication-business"></i> {{ trans('messages.mainapp.menu.specialization') }}</a>
        </li>



    @can('access', \App\Models\User::class)
        <li>
            <ul class="collapsible collapsible-accordion">
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan truncate{!! Request::is('reports*') ? ' active' : '' !!}"><i class="mdi-editor-insert-chart"></i> {{ trans('messages.mainapp.menu.reports.reports') }}</a>
                    <div class="collapsible-body">
                        <ul>
                            <li{!! Request::is('reports/user*') ? ' class="active"' : '' !!}><a href="{{ route('reports::user') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.user_report') }}</a></li>
                            <li{!! Request::is('reports/queuelist*') ? ' class="active"' : '' !!}><a href="{{ route('reports::queue_list', ['date' => \Carbon\Carbon::now()->format('d-m-Y')]) }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.queue_list') }}</a></li>
                            <li{!! Request::is('reports/monthly*') ? ' class="active"' : '' !!}><a href="{{ route('reports::monthly') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.monthly') }}</a></li>
                            <li{!! Request::is('reports/statistical*') ? ' class="active"' : '' !!}><a href="{{ route('reports::statistical') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.statistical') }}</a></li>
                            <li{!! Request::is('reports/missed-overtime*') ? ' class="active"' : '' !!}><a href="{{ route('reports::missed') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.missed') }} / {{ trans('messages.mainapp.menu.reports.overtime') }}</a></li>

                    <li class="bold{!! Request::is('users*') ? ' active' : '' !!}"><a href="{{ route('token_report')  }}" class="waves-effect waves-cyan truncate">
            <i class=""></i> {{ trans('Token Report') }}</a>
        </li>       
                                
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
    @endcan


@if($user->role == 'R')   

<li>
            <ul class="collapsible collapsible-accordion">
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan truncate{!! Request::is('reports*') ? ' active' : '' !!}"><i class="mdi-editor-insert-chart"></i> {{ trans('messages.mainapp.menu.reports.reports') }}</a>
                    <div class="collapsible-body">
                        <ul>
                            <li{!! Request::is('reports/user*') ? ' class="active"' : '' !!}><a href="{{ route('reports::user') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.user_report') }}</a></li>
                            <li{!! Request::is('reports/queuelist*') ? ' class="active"' : '' !!}><a href="{{ route('reports::queue_list', ['date' => \Carbon\Carbon::now()->format('d-m-Y')]) }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.queue_list') }}</a></li>
                            <li{!! Request::is('reports/monthly*') ? ' class="active"' : '' !!}><a href="{{ route('reports::monthly') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.monthly') }}</a></li>
                            <li{!! Request::is('reports/statistical*') ? ' class="active"' : '' !!}><a href="{{ route('reports::statistical') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.statistical') }}</a></li>
                            <li{!! Request::is('reports/missed-overtime*') ? ' class="active"' : '' !!}><a href="{{ route('reports::missed') }}" class="truncate"> {{ trans('messages.mainapp.menu.reports.missed') }} / {{ trans('messages.mainapp.menu.reports.overtime') }}</a></li>
    
                        </ul>
                    </div>
                </li>
            </ul>
        </li>



@endif















    @can('access', \App\Models\User::class)
        <li class="bold{!! Request::is('users*') ? ' active' : '' !!}"><a href="{{ route('users.index') }}" class="waves-effect waves-cyan truncate">
            <i class="mdi-social-group"></i> {{ trans('messages.mainapp.menu.users') }}</a>
        </li>


    <li class="bold{!! Request::is('locationusers*') ? ' active' : '' !!}"><a href="{{ route('locationusers.index') }}" class="waves-effect waves-cyan truncate">
            <i class="mdi-social-group"></i> {{ trans('Location Users') }}</a>
        </li>
    @endcan

    <li class="bold{!! Request::is('settings*') ? ' active' : '' !!}"><a href="{{ route('settings') }}" class="waves-effect waves-cyan truncate">
        <i class="mdi-action-settings"></i> {{ trans('messages.settings') }}</a>
    </li>
    <br><br>
@endsection
