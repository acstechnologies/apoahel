<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <title>@yield('title')</title>
        <link rel="icon" href="{{ asset('assets/favicon.ico') }}">
        <link href="{{ asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        @yield('css')
        <link href="{{ asset('assets/css/style.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        <style>
        a, nav ul a, span.badge.new {
    color: #01579b;
}
nav {
    background-color: #ffffff;
    width: 100%;
    height: 56px;
    line-height: 56px;
}
header .brand-logo img {
    width: 172px;
    margin-top: -10px;
}
.truncate {
    color: #01579b;
}
        </style>
    </head>

    <body>
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>

        <header id="header" class="page-topbar">
            <div class="navbar-fixed">
                    <nav class="navbar-color teal lighten-1">
                    <div class="nav-wrapper">
                        <ul class="left">
                            <!-- <li><h1 class="logo-wrapper"><a href="{{ route('dashboard') }}" class="brand-logo darken-1"><img src="{{ asset('assets/images/jltoken-logo.png') }}" alt="materialize logo"></a><span class="logo-text">ACS Queue</span></h1></li> -->
                            <li><h1 class="logo-wrapper"><a href="{{ route('dashboard') }}" class="brand-logo darken-1" style="font-size:24px; padding:10px; font-weight:400">Apollo iQMS</a><span class="logo-text">Apollo iQMS</span></h1></li>
                            
                            
                          
                            
                        </ul>
                        
                        
                        
                       
                        
                        
                        <ul class="right hide-on-med-and-down">
                            <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown" style="color:#fff !important;"><i class="mdi-editor-insert-link"></i></a></li>
                           
                        </ul>
                         <ul class="right hide-on-med-and-down">
                             
                             <a class="frmsubmit" href="{{ route('logout') }}" message="false" method="post" style="color:#fff !important;"><i class="mdi-action-exit-to-app"></i> </a>
                           
                        </ul>
                        
                        
                                 
                        
                        
                        

                        <ul id="notifications-dropdown" class="dropdown-content">
                            <li><h5>LINKS</h5></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('display') }}" target="_blank" style="font-weight:400">{{ trans('messages.mainapp.display_url') }}</a></li>
                            <li><a href="{{ route('add_to_queue') }}" target="_blank" style="font-weight:400">{{ trans('messages.mainapp.issue_url') }}</a></li>
                        </ul>

                        
                        
                       <ul class="right hide-on-med-and-down">
                           <span style="margin-right:20px;font-size:19px">{{ $loction_name }} </span> 
                        </ul>
                        
                        
                        <ul class="right hide-on-med-and-down">
                           <span style="margin-right:20px;font-size:19px">{{ $user->name }} </span> 
                        </ul>
                        
                         
                        
                    </div>
                </nav>
            </div>
        </header>

        <div id="main">
            <div class="wrapper">
                <aside id="left-sidebar-nav">
                    <ul id="slide-out" class="side-nav fixed leftside-navigation">
                        <li class="white" style="border-bottom: 3px solid #fcb70e;">
                            <div class="row" style="margin-bottom: 0px">
                                <div class="col s12" style="text-align: center">
                                    <img src="{{ asset('assets/images/Apo_Logo.jpg') }}" width="135px" style="margin-top:5px;"/>

                                    
      <!--                           <div class="col col s4 m4 l4">
                                    <img src="{{ asset('assets/images/avatar.jpg') }}" alt="" class="circle responsive-img valign profile-image">
                                </div>
                                <div class="col col s8 m8 l8">
                                    <ul id="profile-dropdown" class="dropdown-content">
                                        <li><a href="{{ route('settings') }}"><i class="mdi-action-settings"></i> {{ trans('messages.settings') }}</a></li>
                                        <li class="divider"></li>
                                        <li><a class="frmsubmit" href="{{ route('logout') }}" message="false" method="post"><i class="mdi-hardware-keyboard-tab; mdi-navigation-close"></i> {{ trans('messages.mainapp.logout') }}</a></li>
                                 
                                    
                                    
                                    </ul>
                                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn truncate" href="#" data-activates="profile-dropdown">
                                        {{ $user->name }}<i class="mdi-navigation-arrow-drop-down right"></i>
                                    </a>
                                    <p class="user-roal">{{ $user->role_text }}</p>
                                </div> -->


                                </div>
                            </div>
                        </li>
                        @yield('menu')
                    </ul>
                    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only teal lighten-1"><i class="mdi-navigation-menu"></i></a>
                </aside>

                <section id="content">
                    @yield('content')
                </section>
            </div>
        </div>

        <footer class="page-footer">
            <div class="footer-copyright">
                <div class="container">
                    <span>Powered by <a class="grey-text text-lighten-3" href="http://acstechnologies.co.in/" target="_blank">ACS Technologies Ltd.</a> All rights reserved.</span>
                    <span class="right"> <span class="grey-text text-lighten-3">Version</span> 0.1.1</span>
                </div>
            </div>
        </footer>
        @yield('print')
        <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery-1.11.2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins.min.js') }}"></script>
        @yield('script')
        <script>
            $(function() {
                $('#main').css({'min-height': $(window).height()-134+'px'});
                $(window).resize(function() {
                    $('#main').css({'min-height': $(window).height()-134+'px'});
                });
                $('#translation-dropdown').perfectScrollbar();
                $('select').each(function() {
                    if(!$(this).parent().hasClass('picker__header')) {
                        $(this).select2();
                    }
                });
            });
            $(".frmsubmit").on("click",function(e) {
                var message = 'Are you sure you want to delete?';
                if(e.currentTarget.attributes.message!=undefined) message = e.currentTarget.attributes.message.value;
                var cnfrm = true;
                if(e.currentTarget.attributes.message!=undefined && e.currentTarget.attributes.message.value=='false') cnfrm = false;
                if (cnfrm) {
                    if(confirm(message)) {
                        e.preventDefault();
                        var myForm = '<form id="hidfrm" action="'+e.currentTarget.attributes.href.value+'" method="post">{{ csrf_field() }}<input type="hidden" name="_method" value="'+e.currentTarget.attributes.method.value+'"></form>';
                        $('body').append(myForm);
                        myForm = $('#hidfrm');
                        myForm.submit();
                    }
                } else {
                    e.preventDefault();
                    var myForm = '<form id="hidfrm" action="'+e.currentTarget.attributes.href.value+'" method="post">{{ csrf_field() }}<input type="hidden" name="_method" value="'+e.currentTarget.attributes.method.value+'"></form>';
                    $('body').append(myForm);
                    myForm = $('#hidfrm');
                    myForm.submit();
                }
                return false;
            });
         /* @if(!Request::is('calls*'))
                $(document).ajaxStart(function() {
                    $("body").removeClass('loaded');
                    Pace.restart();
                });
@endif*/
                $(document).ajaxStop(function() {
                    $("body").addClass('loaded');
                });
              
        </script>
        @include('common.messages')
    </body>
</html>
