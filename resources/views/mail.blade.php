 
<!DOCTYPE html>
<html lang="en">
<head>
  
  <style>
table, td, th {
  border: 1px solid black;
}

#table1 {
  border-collapse: separate;
}

#table2 {
  border-collapse: collapse;
}
</style>
</head>


<body>


<p>Daily IQMS Report for all the AHLL Clinics for the Date {{ $today['day']}} </p>



	<div class="container">
	<div class="col s12">
	<div class="row">
   <?php
   for ($i = 0; $i <10; $i++) {
   ?> 
    
 	<table id="table2">
    
    	 
      <tr>
      	
        <th style="width: 90px;">S No</th>
        <th style="width: 90px;">Clinic</th>
        <th style="width: 90px;">Tokens</th>
        <th style="width: 90px;">Queue</th>
        <th style="width: 90px;">Served</th>
        <th style="width: 90px;">Awaiting</th>
        <th style="width: 90px;">Missed</th>
        <th style="width: 90px;" >Overtime</th>
       </tr>
     
   
    
    	@foreach($reports as $report)
      <tr>
        <td align="center">{{ $loop->iteration }} </td>
        <td align="left">{{ $report['location_name'] }}</td>
        <td align="center">{{ $report['tokens'] }}</td>
        <td align="center">{{ $report['today_queue'] }}</td>
        <td align="center">{{ $report['served'] }}</td>
        <td align="center">{{ $report['waiting'] }}</td>
        <td align="center">{{ $report['missed'] }}</td>
        <td align="center">{{ $report['overtime'] }}</td>

        
      </tr>
       @endforeach
      </tr>
    
  </table>
  <?php
  echo "<br>";
}
?>
</div></div></div></div>
<br>

<p>Regards,</p>
<p>Team IQMS.</p>
</body>
</html>