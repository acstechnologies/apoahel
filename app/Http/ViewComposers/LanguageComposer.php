<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Location;
use App\Models\Department;


class LanguageComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
          
        
        if($user = \Auth::user()) {
            
            $locations = Location::where('id','=',($user->location_id))->first();
            
             if($locations != NULL ){
                $location_name = $locations->name;
                
            }else
            {
                 $location_name ='Super Admin';
                
            }
          
            
            $department = Department::where('location_id','=',($user->location_id))->where('counters',0)->get();
            
            $view->with([
                'languages' => \App\Models\Language::get(),
                'clocale' => \App\Models\Language::where('code', \App::getLocale())->first(),
                'billingcounter' => $department,
                'loction_name' =>$location_name,
                
                
                
            ]);
        }
    }
}
