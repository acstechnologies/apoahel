<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Doctor;
use App\Models\Location;
use App\Models\Specialization;
use App\Models\Hospital;



class SpecializationController extends Controller
{
    protected $specialization;

  

    public function index()
    {

        

       
      $specializations = Specialization::all();
       
        return view('user.specializations.index',['specializations'=>$specializations]);
    }

    public function create()
    {
       
     
     

        return view('user.specializations.create');
    }

    public function store(Request $request, Specialization $specialization)
    {
    

        $this->validate($request, [
            'name' => 'required'
           
       ]);

       
        $specialization->name = $request->name;
       
        $specialization->save();



        flash()->success('Specialization created');
        return redirect()->route('specializations.index');
    }

    public function edit(Request $request, Specialization $specialization)
    {
        
       
      
        return view('user.specializations.edit', [
             'specialization' => $specialization
        ]);
    }

    public function update(Request $request, Specialization $specialization )
    {
       
   
        
      
     
        $specialization->name = $request->name;
       
        $specialization->save();


        flash()->success('Specialization updated');
        return redirect()->route('specializations.index');
    }
    public function destroy(Request $request, Specialization $specialization)
    {
        //$this->authorize('access', Department::class);

        $specialization->delete();

        flash()->success('Specialization deleted');
        return redirect()->route('specializations.index');
    }

}
