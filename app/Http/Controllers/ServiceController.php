<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\DepartmentGroups;
use App\Models\Service;



class ServiceController  extends Controller
{
    protected $departmentGrps;

    public function __construct(DepartmentGroups $departmentGrps)
    {
        $this->departmentGrps = $departmentGrps;
    }

    public function index()
    {

      //  $this->authorize('access', Location::class);

       $departmentGrps =Service::all();//  DepartmentGroups::where('id','>',0)->get();

        return view('user.service.index', [
            'departmentGrps' =>$departmentGrps,
        ]);
    }

    public function create()
    {
        //$this->authorize('access', Location::class);

        return view('user.departmentGrps.create',['departmentGrps' => DepartmentGroups::all()]);
    }

    public function store(Request $request, DepartmentGroups $departmentGrp)
    {
        //$this->authorize('access', Location::class);

        $this->validate($request, [
            'name' => 'required'
        ]);

      //  Department::create($request->all());

        $departmentGrp->name = $request->name;
        $departmentGrp->save();



        flash()->success('Department group created');
        return redirect()->route('departmentGrps.index');
    }

    public function edit(Request $request, DepartmentGroups $departmentGrp)
    {
      //$departGrps = DepartmentGroup::all();
        return view('user.departmentGrps.edit', [
            'departmentGrp' => $departmentGrp,
        ]);
    }

    public function update(Request $request, DepartmentGroups $departmentGrp)
    {
        //$this->authorize('access', Location::class);

        $this->validate($request, [
            'name' => 'required'
        ]);

        $departmentGrp->name = $request->name;
        $departmentGrp->save();

        flash()->success('Department group updated');
        return redirect()->route('departmentGrps.index');
    }

    public function destroy(Request $request, DepartmentGroups $departmentGrp)
    {
        //$this->authorize('access', Location::class);

        $departmentGrp->delete();

        flash()->success('Department group deleted');
        return redirect()->route('departmentGrps.index');
    }
}
