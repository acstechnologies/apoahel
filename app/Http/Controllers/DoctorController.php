<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Doctor;
use App\Models\Location;
use App\Models\Specialization;
use App\Models\Hospital;

use DB;

class DoctorController extends Controller
{
    protected $doctor;

    public function __construct(Location $locations)
    {
       
        $this->locations = $locations;
    }

    public function index()
    {

        //$this->authorize('access', Department::class);

       $doctors =  Doctor::all();

     /*  foreach ($doctors as $doctor) {
          
return $doctor->name;

       }/**/
    
       


        return view('user.doctors.index', [
            'doctors' => $doctors
           
           
        ]);
    }

    public function create()
    {
       
      $specializations= Specialization::all();
      $hospitals=Hospital::all();
       
    //dd($hospitals);
        return view('user.doctors.create',['specializations'=>$specializations,'hospitals'=>$hospitals]);
    }

    public function store(Request $request, Doctor $doctor)
    {
    

        $this->validate($request, [
            'name' => 'required',
             'doctor_id' =>'required',
            'specialization_id' =>'required',
             'hospital_id' => 'required'
       ]);

      $specializationname = Specialization::where('id',$request->specialization_id)->first();
        $doctor->name = $request->name;
        $doctor->doctor_id = $request->doctor_id;
        $doctor->specialization_id=$request->specialization_id;
        $doctor->specialization=$specializationname->name;
        $doctor->hospital_id = $request->hospital_id;
        $doctor->save();



        flash()->success('Doctor created');
        return redirect()->route('doctors.index');
    }

    public function edit(Request $request, Doctor $doctor)
    {
        
        $specializations = Specialization::all();
        $hospitals = Hospital::all();
        return view('user.doctors.edit', [
            'doctor' => $doctor, 'specializations' => $specializations, 'hospitals' => $hospitals,
        ]);
    }

    public function update(Request $request, Doctor $doctor )
    {
       
   
        
      
       $specializationname = Specialization::where('id',$request->specialization_id)->first();
        $doctor->name = $request->name;
        $doctor->doctor_id = $request->doctor_id;
        $doctor->specialization_id=$request->specialization_id;
        $doctor->specialization=$specializationname->name;
        $doctor->hospital_id = $request->hospital_id;
        $doctor->save();


        flash()->success('Doctor updated');
        return redirect()->route('doctors.index');
    }
    public function destroy(Request $request, Doctor $doctor)
    {
        //$this->authorize('access', Department::class);

        $doctor->delete();

        flash()->success('Doctor deleted');
        return redirect()->route('doctors.index');
    }

}
