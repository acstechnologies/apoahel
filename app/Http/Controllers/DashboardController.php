<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DashbordRepository;
use App\Models\Department;
use App\Models\User;
use App\Models\Location;


class DashboardController extends Controller
{
    protected $setting;
    public function __construct(DashbordRepository $setting)
    {
        $this->setting = $setting;
    }

    public function index()
    {
        

       /* $locations = Location::wherein('id',[3,17,8,7,9,18,21,22,23,24])->get();
       

        foreach($locations as $location ){

             $id =$location->id;
               $tokens = $this->setting->locationissedToken($id);
               $today_queue =$this->setting->locationGetTodayQueue($id);
              $served=$this->setting->loctionGetTodayServed($id);
               $waiting =$this->setting->loctionGetwaiting($id);
               $missed = $this->setting->locationGetTodayMissed($id);
               $overtime =$this->setting->locationGetTodayOverTime($id);


            $reports[] =array('location_name' => $location->name,
                              'tokens' =>  $tokens,
                              'today_queue' =>$today_queue,
                              'served' => $served,
                              'waiting' =>$waiting,
                              'missed' => $missed,
                              'overtime' =>$overtime,
                              );

                            
                            }


*/





       
        return view('user.dashboard.index', [
            'setting' => $this->setting->getSetting(),
            'today_queue' => $this->setting->getTodayQueue(),
            'missed' => $this->setting->getTodayMissed(),
            'overtime' => $this->setting->getTodayOverTime(),
            'served' => $this->setting->getTodayServed(),
            'counters' => $this->setting->getCounters(),
            'today_calls' => $this->setting->getTodayCalls(),
            'yesterday_calls' => $this->setting->getYesterdayCalls(),
            'tokens' => $this->setting->issedToken(),
            'waiting' =>$this->setting->getwaiting(),
          //  'reports' => $reports,

        ]);

        
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'notification' => 'bail|required|min:5',
            'size' => 'bail|required|numeric',
            'color' => 'required',
        ]);

        $setting = $this->setting->updateNotification($request->all());

        flash()->success('Notification updated');
        return redirect()->route('dashboard');
    }
}
