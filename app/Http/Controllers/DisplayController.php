<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DisplayRepository;
use Auth;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use App\Repositories\QueueListReportRepository;
use App\Models\Location;

class DisplayController extends Controller
{
    protected $displays;

    public function __construct(DisplayRepository $displays,QueueListReportRepository $queue,Call $calls)
    {
        $this->displays = $displays;
    }

    public function index(Request $request)
    {
/*
               $calls =     DB::select("SELECT * FROM calls WHERE id IN ( SELECT MAX(id) FROM calls GROUP BY department_id )");


 
       */
         $id = Auth::user();
 $request->session()->put('location_id',($id->location_id));
        
        
        $settings = $this->displays->getSettings();

        \App::setLocale($settings->language->code);

        event(new \App\Events\TokenCalled());

        return view('display.index', [
            'data' => $this->displays->getDisplayData(),
            'settings' => $settings,
        ]);
    }
    
    public function index1(Request $request,$id)
    {
         
        
        $location = Location::where('code',$id)->first();
       
        $settings = $this->displays->getSettings();
        \App::setLocale($settings->language->code);
        event(new \App\Events\TokenCalled());
           return view('display.index', [
            'data' => $this->displays->getDisplayDataLocation($location->id),
            'settings' => $settings,
            'location_name' => $location->name
        
        
        
        ]);
        
                
        
    }
    
    
    
    
    
    
    
    
    
    
    

}
