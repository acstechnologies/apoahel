<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Doctor;
use App\Models\Location;
use App\Models\DepartmentGroups;
use App\Models\Department;
use App\Models\TokenTemp;
use App\Models\User;
use App\Repositories\TokenTempRepository;
use DB;
use Auth;

class UpdateCounterController extends Controller
{
    protected $departments;

    public function __construct(Location $locations,Department $department)
    {
       
        $this->locations = $locations;
    }

    public function index()
    {
        $user = Auth::user();
         
        $user_deparment = Department::where('id',($user->department_id))->first();
        $deparment = Department::where('counters',0)->where('location_id',($user->location_id))->get();
        if ($user_deparment !=NULL){
         $name =$user_deparment->name;
        }else
        {

           $name ='Super Admin' ;
        }

        
        return view('user.updatecounter.index', [
          
          
           'departments' => $deparment,
            'user'          =>$user_deparment,
            'user_department' =>$name
          

        ]);
    }

    public function create()
    {
        //$this->authorize('access', Department::class);
        $locations = Location::all();
        $departmentGrps = DepartmentGroups::all();

        return view('user.doctors.create',[ 'departmentGrps' =>$departmentGrps,'locations' => $locations]);
    }

    public function store(Request $request, TokenTemp $temp)
    {
        return  $request->id;
        //$this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'service_id' =>'required',
            'location_id' =>'required|numeric',
            'group_id' => 'required|numeric'
        ]);

      //  Department::create($request->all());

        $doctor->name = $request->name;
        $doctor->service_id = $request->service_id;
        $doctor->location_id = $request->location_id;
        $doctor->group_id = $request->group_id;
        $doctor->save();



        flash()->success('Doctor created');
        return redirect()->route('doctors.index');
    }

    public function edit(Request $request,TokenTemp $temp,$id)
    {
         


         $consultations = TokenTemp::where('bill_no','=',$id)->whereIn('priority',[99,0])->get();
          $doctors     = User::where('role','D')->get();
         
        
        $location = Location::all();
        $departmentGrps = DepartmentGroups::all();
        return view('user.hcconsultation.edit',['consultations' =>$consultations,'doctors' =>$doctors]);
    }

    public function update(Request $request, Doctor $doctor,$id)
    {
        $id;            
       
       
        //$this->authorize('access', Department::class);
        $department_id = $request->department_id;
           
        $user = User::where('id',$id)->first();

        
       User::where('id','=', $id)->update(['department_id'=>$department_id]);

   


        
        
        
    

       
        flash()->success('Department updated');
        return redirect()->route('updatecounter.index');
    }
    public function destroy(Request $request, Doctor $doctor,$id)
    {
       

       TokenTemp::where('bill_no','=',$id)->update(['status'=>1]); 
  
       

        flash()->success('Bill Cancelled');
        return redirect('billcancellation');
        
    }

}
