<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use App\Repositories\CounterRepository;
use App\Models\Counter;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Location;
use App\Models\DepartmentGroups;
use App\Models\Specialization;
use DB;

class DepartmentController extends Controller
{
    protected $departments;

    public function __construct(DepartmentRepository $departments,CounterRepository $counters)
    {
        $this->departments = $departments;
        $this->counters = $counters;
    }

    public function index()
    {

        $this->authorize('access', Department::class);

        // $DepartmentGroups = DepartmentGroups::all();
       $specializations = Specialization::all();
       $departments =  Department::get();
       $counters = Counter::all();
        return view('user.departments.index', [
                     'departments' =>$departments,
                     'counters' =>$counters,
                     'specializations'  =>$specializations
        ]);
    }

    public function create()
    {
        $this->authorize('access', Department::class);

        return view('user.departments.create',[ 'counters' =>$this->counters->getAll(),'locations' => Location::all(),'specializations'  =>Specialization::all()]);
    }

    public function store(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'counter_id' =>'required',
            'start' =>'required|numeric'
        ]);

      //  Department::create($request->all());

        $department->name = $request->name;
        $department->letter = $request->letter;
        $department->start = $request->start;
        $department->counter_id = $request->counter_id;
        $department->location_id =$request->location_id;
        $department->specialization_id =$request->specialization_id;
        $department->save();



        flash()->success('Department created');
        return redirect()->route('departments.index');
    }

    public function edit(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);
        $location = Location::all();
        $counters = Counter::all();
        return view('user.departments.edit', [
            'department' => $department, 'locations' => $location, 'counters' => $counters,
        ]);
    }

    public function update(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'start' =>'required|numeric',
        ]);

        $department->name = $request->name;
        $department->location_id = $request->location_id;
        $department->counter_id = $request->counter_id;
        $department->letter = $request->letter;
        $department->start = $request->start;
        $department->save();

        flash()->success('Department updated');
        return redirect()->route('departments.index');
    }
    public function destroy(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        $department->delete();

        flash()->success('Department deleted');
        return redirect()->route('departments.index');
    }

    public function getCounters(Request $request){
    $data =$request->option;
     return $subcategories  = Counter::where('location_id','=',$data)->get();
    }
}
