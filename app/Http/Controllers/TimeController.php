<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DisplayRepository;
use Auth;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use App\Models\Online;

class TimeController extends Controller
{
  protected $calls;
  public function __construct(CallRepository $calls)
  {
    $this->calls = $calls;
  }

    
  public function index(Request $request)
  
  {
    

    
    $service_users = Online::all('user_id');

    foreach ($service_users as $service_user) {
            $service_user_id[]=$service_user->user_id;
      }

    $queues= Queue::with('package')
                ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('defaults','=',1)->where('called','=',1)->get();
 

    foreach ($queues as $queue){

       $patient_phone =$queue->moblie_number;
      $patient_phone_lists =TokenTemp::where('patient_phone',$patient_phone)->where('status',0)->distinct()->get(['mr_no','patient_phone','location_id']);

      foreach ($patient_phone_lists as $patient_phone_list) 
      {
        $mr_no = $patient_phone_list->mr_no;
        $patient_phone = $patient_phone_list->patient_phone;
        $location_id=$patient_phone_list->location_id;


        $queues= Queue::with('department')->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('mr_no','=',$mr_no)->where('moblie_number','=',$patient_phone)->where('called','=',1)->first();

    
        $queues_id = $patient_phone_list->location_id;
        $department =   Department::where('location_id','=',$queues_id)->where('counters','=',1)->first();
        $department_id =$department->id;
        $tokenTime =  TokenTemp::where('department_id',$department_id)->where('status',0)->where('patient_phone','=',$queue->moblie_number)->where('mr_no','=',$mr_no)->where('called','=',1)->distinct()->get(['bill_no','patient_phone','department_id','updated_at']);

         $tokenTime_pp =  TokenTemp::where('department_id',$department_id)->where('status',0)->where('patient_phone','=',$queue->moblie_number)->where('mr_no','=',$mr_no)->where('pp','=',1)->distinct()->get(['mr_no','patient_phone','department_id','updated_at']);
         $tokenTime_pp_count =  TokenTemp::where('department_id',$department_id)->where('status',0)->where('patient_phone','=',$queue->moblie_number)->where('mr_no','=',$mr_no)->where('pp','=',1)->distinct()->count();

    if($tokenTime_pp_count  >=  1)
    {
      foreach ($tokenTime as $token_time)
      {
        $token_update_time = $token_time->updated_at;
        $token_mrno =$token_time->bill_no;
        $token_mobile =$token_time->patient_phone;
        $ldate = date('Y-m-d H:i:s');
        $to_time = strtotime($ldate);
        $from_time = strtotime( $token_time->updated_at);
        $minites = round(abs($to_time - $from_time) / 60,2);

        if($minites  > 90)
        {
          $temp_tokens = TokenTemp::where('patient_phone','=',$token_mobile)->where('status',0)->where('mr_no','=',$mr_no)->where('pp','=',1)->get();
          $queues_number =Queue::where('department_id','=',$department_id)->where('mr_no','=',$mr_no)->where('moblie_number','=',$token_mobile)->first();

          foreach ($temp_tokens as $temp_token)
          {
            $auto_call = DB::table('pp_call')->select('*')->where('mr_no',$mr_no)->count();
            if($auto_call == 0)
            {
              $qservice_id =$temp_token->service_id;
              $service = Services::where('service_id','=',$qservice_id)->first();
              $qbill_no  = $temp_token->mr_no;
              $department_id = $temp_token->department_id;
              $location_id = $temp_token->location_id;
                $patient_name =$temp_token->patient_name;
              $counter = Counter::where('location_id','=',$location_id)->where('group_id','=',($service->counter_id))->first();
              $department = Department::where('counter_id','=',($counter->id))->first();


              
              $qmobile_number =$temp_token->patient_phone;
              $queues = Queue::where('department_id','=',$department_id)->where('mr_no','=',$mr_no)->where('moblie_number','=',$qmobile_number)->first();
              if($temp_token->priority != 0 )
              {
                $service =  Services::where('service_id','=',$qservice_id)->first();
                $sdepartment_id = $service->counter_id;



                //$department=  Department::where('id','=',$sdepartment_id)->first();
                
                $sdepartment_id = $service->counter_id;
                $users =User::where('department_id','=',($department->id))->get();


             $count_array =array();
            
            foreach ($users as $user)
            {
                if (in_array($user->id,$service_user_id))
                {
                    $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                    $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                }
            }


            $min_counter_array=min($count_array);
             $min_user_id=$min_counter_array['user_id'];




                $queues_dup = Queue::where('called','=',0)->where('moblie_number','=',$qmobile_number)->first();
                $queues_dup_count =  Queue::where('called','=',0)->where('mr_no','=',$mr_no)->where('moblie_number','=',$qmobile_number)->count();            
               
                if ($queues_dup_count >= 1)
                {
                  $queues_dup = Queue::where('called','=',0)->where('mr_no','=',$mr_no)->where('moblie_number','=',$qmobile_number)->first();
             
                  DB::table('queues')->where('called','=',0)->where('department_id','=',($queues_dup->department_id))->where('moblie_number','=',$qmobile_number)->where('mr_no','=',$mr_no)->update(['department_id' => $department_id,'user_id' =>( $user->id)]);
                  $queues_dup = Queue::where('called','=',0)->where('moblie_number','=',$qmobile_number)->where('mr_no','=',$mr_no)->first();
                    
                }
                else
                {

                 
                  $queue = $department->queues()->create([
                  'number' =>$queues_number->number,
                  'token_prefix' => $queues_number->token_prefix,
                  'called' => 0,
                  'moblie_number' =>  $token_mobile,
                  'user_id' => $min_user_id,
                  'service_id'=>$qservice_id,

                  'mr_no'   => $mr_no,
                   'location_id' =>$location_id,
                    'patient_name' =>$patient_name
                  ]);
                }

                DB::table('pp_call')->insert(['mr_no' => $mr_no]);    
                $mobile_number =  TokenTemp::where('mr_no','=',$qbill_no)->first();

                
                
                $patient_phone= $token_mobile;
                $dname =$department->name;
                $departmentname = str_replace(' ', '%20', $dname);
                $token_number = $queue->number;
                $queue_token_prefix = $queues_number->token_prefix;
                $total = $this->calls->getCustomersWaiting($department);
                $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                $patient_name=$temptoken->patient_name;
                $patientname = str_replace(' ', '%20', $patient_name);
                $patient_gender=$temptoken->patient_gender;
                    if($patient_gender == 'M')
                        {
                            $gender_prifix = 'Mr';

                        }else{
                            $gender_prifix = 'Ms';

                        }

                $department_counter = Department::where('group_id','=',$sdepartment_id)->first();
                $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                $counter_name_array = explode("-",  $counter_name->name);
                // $coutnername = str_replace(' ', '%20',$counter_name->name);
                $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                $floor =str_replace(' ', '%20', $counter_name_array[1]);
                $servicename =str_replace(' ', '%20', $counter_name_array[2]);



                  $servicestime = Department::where('id',$sdepartment_id)->first();
                  $service_time =$servicestime->service_time; 
                  $approx_time =  $service_time* ($total-1);


              $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.'%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


                      
 
 /*$message ='Dear%20Mr/Ms%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.'%20for%20the%20Service%20.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';
*/
                        /*$url = "http://sms.bulksmsind.in/sendSMS?username=acstechnologies&message=$message&sendername=ACSQUE&smstype=TRANS&numbers=$patient_phone&apikey=de51c133-a213-4951-b9d9-887e66c90948";*/
                       // $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";

                        $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';
                $total = $this->calls->getCustomersWaiting($department);

                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $data = curl_exec($ch);
                curl_close($ch);
                event(new \App\Events\TokenIssued());
                event(new \App\Events\TokenCalled());
               
              }
            }
          }
        }
      }
    }

  

   }
   
  }
  }
}
  
























/*



    foreach ($tokenTime as $token_time) {

      $token_update_time = $token_time->updated_at;
      $token_mrno =$token_time->bill_no;
      $token_mobile =$token_time->patient_phone;
      $ldate = date('Y-m-d H:i:s');
      $to_time = strtotime($ldate);
      $from_time = strtotime( $token_time->updated_at);
      $minites = round(abs($to_time - $from_time) / 60,2);

        $auto_call = DB::table('auto_call')->select('*')->where('mr_no',$token_mobile)->count();
       
        if($auto_call == 0){
        if($minites  > 3){
       // $temp_tokens = TokenTemp::where('bill_no','=',$token_mrno)->get(); 
        $temp_tokens = TokenTemp::where('patient_phone','=',$token_mobile)->get();

      //  $queues_number =Queue::where('department_id','=',3)->where('mr_no','=',$token_mrno)->first();
        $queues_number =Queue::where('department_id','=',14)->where('moblie_number','=',$token_mobile)->first();


        foreach ($temp_tokens as $temp_token){
        $qservice_id =$temp_token->service_id;
        $qbill_no  = $temp_token->bill_no;
        $department_id = $temp_token->department_id;
        $qmobile_number =$temp_token->patient_phone;
       // $queues = Queue::where('service_id','=',$qservice_id)->where('moblie_number','=',$qmobile_number)->first();
        $queues = Queue::where('department_id','=',$department_id)->where('moblie_number','=',$qmobile_number)->first();
      
        if($temp_token->priority != 0 ){
        if($queues == NULL){
             
              $service =  Services::where('service_id','=',$qservice_id)->first();
              $sdepartment_id = $service->counter_id;
              $department=  Department::where('id','=',$sdepartment_id)->first();
              $user =User::where('department_id','=',$department->id)->first();
              $queues_dup = Queue::where('department_id','=',$sdepartment_id)->where('moblie_number','=',$qmobile_number)->first();

        if($queues_dup == NULL){
            
            $queue = $department->queues()->create([
            'number' =>$queues_number->number,
            'called' => 0,
            'moblie_number' =>  $token_mobile,
            'user_id' =>$user->id,
            'service_id'=>$qservice_id,
            'mr_no'   => $qbill_no
             ]);

        $mobile_number =  TokenUsers::where('mr_no','=',$qbill_no)->first();
        $token_moblie_num=$qmobile_number;// $mobile_number->patient_phone;
        $departmentids = (explode('.',$qbill_no));
        //$dname =$departmentids[0];// 9739044807
         $dname =$department->name;
        $token_number = ($department->letter!='')?$departmentids[0].'-'.$queue->number:$queue->number;
        $total = $this->calls->getCustomersWaiting($department);
        $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;
        $url="http://1014.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$token_moblie_num&source=lmtpld&message=$message";
        $total = $this->calls->getCustomersWaiting($department);



         DB::table('auto_call')->insert(['mr_no' => $token_mobile]);



        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());
        return ;
        }
        }
        }

        }
        }
        }

        }
 
     }
}
   */