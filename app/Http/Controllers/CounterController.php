<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CounterRepository;
use App\Models\Counter;
use App\Models\Location;
use App\Models\DepartmentGroups;
use Auth;
class CounterController extends Controller
{
    protected $counters;

    public function __construct(CounterRepository $counters)
    {
        $this->counters = $counters;
    }

    public function index()
    {
        //$this->authorize('access', Counter::class);
           $user = Auth::user();
          // $$user->role = 'CM';
           $location_id = $user->location_id;
          if($user->role == 'A'){
            $counters = $this->counters->getAll();
          }
          if($user->role == 'CM'){
              $counters = Counter::where('location_id' , $location_id)->get();
          }
         //return $counters;
        return view('user.counters.index', [
            'counters' =>$counters,'locations' => Location::all(), 'departmentGrps' => DepartmentGroups::all()
        ]);
    }

    public function create()
    {
        //$this->authorize('access', Counter::class);

        return view('user.counters.create',['locations' => Location::all(), 'departmentGrps' => DepartmentGroups::all()]);
    }

    public function store(Request $request, Counter $counter)
    {
       // $this->authorize('access', Counter::class);
/*
        $this->validate($request, [
            'name' => 'required',
        ]);*/

       // $deptGrps = $request->input('group_id');
        //$deptGrps = implode(',', $deptGrps);

        $counter->name = $request->name;
        $counter->location_id =$request->location_id;
       // dd($counter);
       // $counter['group_id'] = $deptGrps;
        $counter->save();

        
        /*$data = $request->except('group_id');*/
        

        //Counter::create($data);

        flash()->success('Counter created');
        return redirect()->route('counters.index');
    }

    public function edit(Request $request, Counter $counter)
    {
        
        

        //$group_id =explode(',' ,$counter->group_id);



        $this->authorize('access', Counter::class);
        $location = Location::all();
        $counters = Counter::all();
        //$departmentGrps = DepartmentGroups::all();

        return view('user.counters.edit', [
            'counter' => $counter, 'locations' => $location
        
        ]);
    }

    public function update(Request $request, Counter $counter)
    {
       // $this->authorize('access', Counter::class);

        $this->validate($request, [
            'name' => 'required',
        ]);

        //$deptGrps = $request->input('group_id');
       // $deptGrps = implode(',', $deptGrps);

        $counter->name = $request->name;
        $counter->location_id = $request->location_id;
       // $counter['group_id'] = $deptGrps;
        $counter->save();

        flash()->success('Counter updated');
        return redirect()->route('counters.index');
    }

    public function destroy(Request $request, Counter $counter)
    {
        //$this->authorize('access', Counter::class);

        $counter->delete();

        flash()->success('Counter deleted');
        return redirect()->route('counters.index');
    }
}
