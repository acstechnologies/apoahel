<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use App\Repositories\CounterRepository;
use App\Models\Counter;
use App\Models\Online;
use App\Models\Location;
use Illuminate\Support\Facades\Input;

class LocationUserController extends Controller
{
    protected $users;

    public function __construct(UserRepository $users,DepartmentRepository $departments,CounterRepository $counters)
    {
        $this->users = $users;
        $this->departments = $departments;
        $this->counters = $counters;
    }

    public function index(Request $request)
    {



     $service_users = Online::all('user_id');

      foreach ($service_users as $service_user) {
            $service_user_id[]=$service_user->user_id;
      }
      
        $this->authorize('access', User::class);

        return view('user.locationusers.index', [
            
            'locations' =>Location::all(),
            
        ]);
    }



    public function edit(Request $request, User $user,$id){

        $user = User::where('id','=',$id)->first();

         $user->Location->name;
            
            $user->Department->name;

         $this->authorize('access', User::class);

       

        return view('user.locationusers.edit', ['departments' =>$this->departments->getAll(),
            'counters' =>$this->counters->getAll(),'locations' =>Location::all(),
            'cuser' => $user,
        ]);

    



    }
    

    public function show(Request $request, User $user,$id)
    {
        
         $service_users = Online::all('user_id');

      foreach ($service_users as $service_user) {
            $service_user_id[]=$service_user->user_id;
      }


        $this->authorize('access', User::class);

        $users = User::where('location_id','=',$id)->get();


        return view('user.locationusers.show', [
            'users' =>  $users,
            'departments' =>$this->departments->getAll(),
            'online_user' => $service_user_id,
            'locations' =>Location::all(),
            
        ]);
    }

    public function create(Request $request)
    {
        
        $this->authorize('access', User::class);
        
        return view('user.users.create',['departments' =>$this->departments->getAll(),
            'counters' =>$this->counters->getAll(),'locations' =>Location::all()
            ]);
    }

    public function store(Request $request)
    {
         $service_users = Online::all('user_id');

      foreach ($service_users as $service_user) {
            $service_user_id[]=$service_user->user_id;
      }


       
         $user =User::where('id','=', $request->id)->first();



       $user->name =  $request->name;
       $user->username = $request->username;
       $user->email =$request->email;
       $user->department_id=$request->department_id;
       $user->counter_id=$request->counter_id;
       $user->save();
       $department =Department::where('id','=',$request->department_id)->first();
       $department->counter_id=$request->counter_id;
       $department->save();

       $users =User::where('location_id','=',$user->location_id)->get();
    

        flash()->success('User Updates');
       return view('user.locationusers.show', [
            'users' =>  $users,
            'departments' =>$this->departments->getAll(),
            'online_user' => $service_user_id,
            'locations' =>Location::all(),
            
        ]);
    }

    public function getPassword(Request $request, User $user)
    {
        $this->authorize('access', User::class);

        if($user->id==$request->user()->id) abort(404);

        return view('user.users.password', [
            'cuser' => $user,
        ]);
    }

    public function postupdate(Request $request, User $user)


    {

        return 'hai';
        $this->authorize('access', User::class);

        if($user->id==$request->user()->id) abort(404);

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user->password = bcrypt($request->password);
        $user->save();

        flash()->success('Password changed');
        return redirect()->route('users.index');
    }

    public function destroy(Request $request, User $user)
    {
        $this->authorize('access', User::class);

        $user->delete();

        flash()->success('User deleted');
        return redirect()->route('users.index');
    }

    public function getDepartments(Request $request){
    $data =$request->option;
     return $subcategories  = Department::where('location_id','=',$data)->get();

    }
}
