<?php

namespace App\Listeners;
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Events\TokenCalled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Queue;
use App\Models\Call;
use App\Models\Location;
use Carbon\Carbon;

class UpdateDisplayController extends Controller
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(TokenCalled $event,Request $request,$id)
    {

           
        
          $location_id = Location::where('code',$id)->first();
        
        


        $calls = Call::with('department', 'counter')
                        ->where('called_date', Carbon::now()->format('Y-m-d'))
                        ->where('location_id',($location_id->id))
                        ->orderBy('calls.id', 'desc')
                        ->take(4)
                        ->get();

        $data = [];
        for ($i=0;$i<4;$i++) {

           
            
            $data[$i]['call_id'] = (isset($calls[$i]))?$calls[$i]->id:'NIL';
            $data[$i]['number'] = (isset($calls[$i]))?(($calls[$i]->queue->token_prefix!='')?$calls[$i]->queue->token_prefix.'-'.$calls[$i]->number:$calls[$i]->number):'NIL';
            $data[$i]['call_number'] =(isset($calls[$i]))?$calls[$i]->number:'NIL';
            $data[$i]['counter'] = (isset($calls[$i]))?$calls[$i]->counter->name:'NIL';
      
        }

 
      return  json_encode($data);

        //file_put_contents(base_path('assets/files/display'), json_encode($data));
    }
}
