<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use App\Repositories\CounterRepository;
use App\Models\Counter;
use App\Models\Online;
use App\Models\Location;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    protected $users;

    public function __construct(UserRepository $users,DepartmentRepository $departments,CounterRepository $counters)
    {
        $this->users = $users;
        $this->departments = $departments;
        $this->counters = $counters;
    }

    public function index(Request $request)
    {

         $service_users = Online::all('user_id');

      foreach ($service_users as $service_user) {
            $service_user_id[]=$service_user->user_id;
      }
      
        $this->authorize('access', User::class);

        return view('user.users.index', [
            'users' => $this->users->getAll(),
            'departments' =>$this->departments->getAll(),
            'online_user' => $service_user_id,
        ]);
    }

    public function create(Request $request)
    {
        $this->authorize('access', User::class);
        
        return view('user.users.create',['departments' =>$this->departments->getAll(),
            'counters' =>$this->counters->getAll(),'locations' =>Location::all()
            ]);
    }

    public function store(Request $request)
    {
              // dd($request->all());
        $this->authorize('access', User::class);

        $this->validate($request, [
            'name' => 'bail|required',
            'username' => 'bail|required|min:6|unique:users,username',
            'email' => 'bail|required|email|unique:users,email',
            'password' => 'bail|required|min:6|confirmed',
        ]);
       
        $data = $request->all();
       // $data['role'] = 'S';
        $data['password'] = bcrypt($request->password);
       // $date['department_id'] = $request->department;

        //return $data;

        $user = User::create($data);

        flash()->success('User created');
        return redirect()->route('users.index');
    }

    public function getPassword(Request $request, User $user)
    {
        $this->authorize('access', User::class);

        if($user->id==$request->user()->id) abort(404);

        return view('user.users.password', [
            'cuser' => $user,
        ]);
    }

    public function postPassword(Request $request, User $user)
    {
        $this->authorize('access', User::class);

        if($user->id==$request->user()->id) abort(404);

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user->password = bcrypt($request->password);
        $user->save();

        flash()->success('Password changed');
        return redirect()->route('users.index');
    }

    public function destroy(Request $request, User $user)
    {
        $this->authorize('access', User::class);

        $user->delete();

        flash()->success('User deleted');
        return redirect()->route('users.index');
    }

    public function getDepartments(Request $request){
    $data =$request->option;
     return $subcategories  = Department::where('location_id','=',$data)->get();

    }
}
