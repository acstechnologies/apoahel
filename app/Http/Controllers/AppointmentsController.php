<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;

use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;


class AppointmentsController  extends Controller
{
    protected $calls;
    public function __construct(CallRepository $calls)
    {
        $this->calls = $calls;
    }


    public function index(Request $request)
    {
       return 'hai';

        $queues= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',1)->where('department_id','=',1)->orderBy('queues.created_at', 'desc')
                    ->get(); 
        foreach($queues as $queue)
        {

            
            $patient_phone =$queue->moblie_number;

            $queues_count= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('moblie_number',$patient_phone)->count(); 

           
              if($queues_count == 0){
            
            $users_tokens_pp =TokenTemp::where('patient_phone',$patient_phone)
                          ->where('pp','=',1)->distinct()->count();
            $users_tokens_pp_called =TokenTemp::where('patient_phone',$patient_phone)
                          ->where('pp','=',1)->where('called','=',1)->distinct()->count();
            $users_token_pp_called_time =TokenTemp::where('patient_phone',$patient_phone)
                          ->where('pp','=',0)->whereIn('priority',[1])->where('called','=',1)->first();
            
            if($users_tokens_pp == 0)
            {
                $users_tokens = TokenTemp::where('patient_phone',$patient_phone)
                        ->where('called', '=', 0)->whereIn('priority',[1,2])
                        ->where('pp','=',NULL)->distinct()->get(['bill_no','department_id','updated_at']);
                $users_tokens_count = TokenTemp::where('patient_phone',$patient_phone)
                        ->where('called', '=', 0)->whereIn('priority',[1,2])
                        ->where('pp','=',NULL)->distinct()->count();
                if($users_tokens_count != 0)
                {
                    $count_array =array();
                    foreach ($users_tokens as $users_token)
                    {
                        $mr_no = $users_token->mr_no;
                        $departmentid = $users_token->department_id;
                        $user =User::where('department_id','=',$departmentid)->first();
                        $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                        $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                    
                    $count_array;
                    $min_counter_array=min($count_array);
                    $min_user_id=$min_counter_array['user_id'];
                    $foruserid= user::where('id','=',$min_user_id)->first();    
                    $department =Department::where('id','=',$foruserid->department_id)->first();          
                    $last_token = $this->calls->getLastToken($department);


          
                    $queue = $department->queues()->create([
                        'department_id' => $department->id,
                        'number' => $queue->number,
                        'called' => 0,
                        'user_id'  =>$foruserid->id,
                        'moblie_number' =>$patient_phone,
                        'service_id'=>0,
                        'bill_no' =>$mr_no,
                        'mr_no'   =>  0
                        ]);
                    
                   
                    $department = Department::where('id','=',($department->id))->first();
                    $elem=9959576026;//$mobile;
                    
                    $patient_phone = 9959576026;//$queue->moblie_number;   
                    $dname =$department->name;
                    $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;
                    $total = $this->calls->getCustomersWaiting($department);
                    $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn %20Total%20customer%20waiting%20'.$total;
                    $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$elem&source=ACSQUE&message=$message";

                    $ch = curl_init();
                    $timeout = 5;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    
                    event(new \App\Events\TokenIssued());
                    event(new \App\Events\TokenCalled());
                    // flash()->success('Token Called');
                    // return redirect()->route('calls');
                }
            }
        
            else
            {
                 
                $users_tokens = TokenTemp::where('patient_phone',$patient_phone)
                        ->where('called', '=', 0)->whereIn('priority',[1])
                        ->where('pp','=',NULL)->distinct()->get(['bill_no','department_id','updated_at']);
                $users_tokens_count = TokenTemp::where('patient_phone',$patient_phone)
                        ->where('called', '=', 0)->whereIn('priority',[1])
                        ->where('pp','=',NULL)->distinct()->count();
                
                if($users_tokens_count != 0)
                {
                    
                    $count_array =array();
                    foreach ($users_tokens as $users_token)
                    {
                        $mr_no = $users_token->mr_no;
                        $departmentid = $users_token->department_id;
                        $user =User::where('department_id','=',$departmentid)->first();
                        $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                        $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                    
                    $count_array;
                    $min_counter_array=min($count_array);
                    $min_user_id=$min_counter_array['user_id'];
                    $foruserid= user::where('id','=',$min_user_id)->first();    
                    $department =Department::where('id','=',$foruserid->department_id)->first();          
                    $last_token = $this->calls->getLastToken($department);
          
                    $queue = $department->queues()->create([
                        'department_id' => $department->id,
                        'number' => $queue->number,
                        'called' => 0,
                        'user_id'  =>$foruserid->id,
                        'moblie_number' =>$patient_phone,
                        'service_id'=>0,
                        'bill_no' =>$mr_no,
                        'mr_no'   =>  0
                        ]);
                    
                    $department = Department::where('id','=',($department->id))->first();
                    
                    //$elem=$mobile;//$patient_phone;
                    $elem=9959576026;//$patient_phone;
                    $dname =$department->name;
                    $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;
                    $total = $this->calls->getCustomersWaiting($department);
            
                    $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn %20Total%20customer%20waiting%20'.$total;
                    $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$elem&source=ACSQUE&message=$message";

                    $ch = curl_init();
                    $timeout = 5;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    
                    
                   /* event(new \App\Events\TokenIssued());
                    event(new \App\Events\TokenCalled());
                    flash()->success('Token Called');
                    return redirect()->route('calls');*/
                }
            
        }
}

    }
    

        
        
    }
}
      
