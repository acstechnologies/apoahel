<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;


class CallController extends Controller
{
    protected $calls;

    public function __construct(CallRepository $calls)
    {
        $this->calls = $calls;
    }


    public function newCall(Request $request)
    {
 //return $request->all();
        
/*
        $this->validate($request, [
            'user' => 'bail|required|exists:users,id',
            'counter' => 'bail|required|exists:counters,id',
            'department' => 'bail|required|exists:departments,id',
            ]);*/
       //return "hai";
        $id = $request->input('qid');
        $user = User::findOrFail($request->user);
        $counter = Counter::where('id','=',$request->counter)->first();
        $department = Department::findOrFail($request->department);
        $queue  =  $department->queues()
                    ->where('called', 0)
                    ->where('user_id',$user->id)
                    ->where('id',$id)
                    ->where('created_at', '>', Carbon::now()->format('Y-m-d 00:00:00'))
                    ->first();
        
        

   
        if ($queue != NULL)
        {          
            $queue_no = $queue->number;
        }

        if($queue == null)
        {
            flash()->warning('No Token for this department');
            return redirect()->route('calls');
        }
            
        $department = Department::findOrFail($request->department);

        if($department == null)
        {
            flash()->warning('No Token for this department');
            return redirect()->route('calls');
        }


        $call = $queue->call()->create([
                'department_id' => $department->id,
                'counter_id' => $counter->id,
                'user_id' => $user->id,
                'number' => $queue->number,
                'location_id' =>$queue->location_id,
                'called_date' => Carbon::now()->format('Y-m-d'),
                ]);

        $queue->called = 1;
        $queue->save();


     
        $patient_phone =$queue->moblie_number;
        $patient_mr = $queue->mr_no;
        $dname =$department->name;
        $departmentname = str_replace(' ', '%20', $dname);
        $token_number = $queue->token_prefix.'-'.$queue->number;
        $total = $this->calls->getCustomersWaiting($department);
        $department = Department::where('id',($department->id))->first();
        $loaction_id =$queue->location_id; 
        $counters = $department->counters;

        $pantry_departments = Department::where('counters', 2)->get();
        foreach ($pantry_departments as $pantry_department) {
            
            $pantry_department_ids[] = $pantry_department->id; 
        }
         $pantry_department_ids; 

  

        if($queue->defaults == 1)
        {
            $message= 'Token%20No.%20'.$token_number.'%20please%20proceed%20to%20the%20billing%20counter';
        }

        elseif(in_array(($department->id),$pantry_department_ids))
        {
            
            $pantry =TokenTemp::where('patient_phone','=',$patient_phone)->where('mr_no','=',$patient_mr)->first();
            $pantry_department=Department::where('id',($department->id))->first(); 
            $counter_name = Counter::where('id','=',($pantry_department->counter_id))->first();
            $token_prefix   =   $queue->token_prefix;
            $patient_name=$pantry->patient_name;
            $temptoken = TokenTemp::where('mr_no',$patient_mr)->first();
            $patient_name=$temptoken->patient_name;
            $patientname = str_replace(' ', '%20', $patient_name);
            $coutnername = str_replace(' ', '%20',$counter_name->name);
            $queue_token_prefix = $token_prefix;
            $token_number = $queue->number;
            $patient_phone =$patient_phone;
            $patient_mr =$pantry->mr_no;  
            $total = $this->calls->getCustomersWaiting($department);
            $servicestime = Department::where('id',$department->id)->first();
            $service_time =$servicestime->service_time; 
            $approx_time =  $service_time* ($total-1);
            $patient_gender=$pantry->patient_gender;
            if($patient_gender == 'M')
            {
                $gender_prifix = 'Mr';

            }
            else
            {
                $gender_prifix = 'Ms';
            }

            $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20complete%20your%20breakfast%20served%20in%20the%20'.$coutnername.'.%20Request%20you%20to%20return%20within%2020%20mins%20to%20complete%20other%20tests.%20Bon%20Appetit.';
        
        }else
        {

           
            $temptoken = TokenTemp::where('mr_no',$patient_mr)->first();

            if($temptoken == NULL){
            $temptoken = TokenTemp::where('patient_phone',$patient_phone)->first();
            }

            $patient_name=$temptoken->patient_name;
            $patientname = str_replace(' ', '%20', $patient_name);
            $department_counter = Department::where('id','=', $department->id)->first();
            $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
            $counter_name_array = explode("-",  $counter_name->name);
            $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
            $floor =str_replace(' ', '%20', $counter_name_array[1]);


            $patient_gender=$temptoken->patient_gender;
            if($patient_gender == 'M')
            {
                $gender_prifix = 'Mr';
            }
            else
            {
                $gender_prifix = 'Ms';
            }

                        
           $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20proceed%20to%20'.$coutnername.'%20'.$floor.'.'; 
        }
       
            $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';
     
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

     
         $departmentid =Department::where('id','=',($queue->department_id))->first();

         $tokenTempcount = TokenTemp::where('department_id',($queue->department_id))
        ->where('patient_phone',$queue->moblie_number)->where('called','=',0)->where('mr_no','=',$patient_mr)->count();
        
        $tokenTempcount_row = TokenTemp::where('department_id',($queue->department_id))
        ->where('patient_phone',$queue->moblie_number)->where('called','=',0)->where('mr_no','=',$patient_mr)->first();

        if($tokenTempcount == 1){  

         if($tokenTempcount_row->pp != 1){
            
         TokenTemp::where('department_id',($queue->department_id))
        ->where('patient_phone',$queue->moblie_number)->where('mr_no','=',$patient_mr)->update(['called' => 1]);  
        }else {
             

          TokenTemp::where('department_id',($queue->department_id))
        ->where('patient_phone',$queue->moblie_number)->where('pp','=',1)->where('mr_no','=',$patient_mr)
        ->update(['called' => 1]);






        }    
        }else
        {

 
            
             TokenTemp::where('department_id',($queue->department_id))
            ->where('patient_phone',$queue->moblie_number)->where('pp','=',NULL)->where('mr_no','=',$patient_mr)    ->update(['called' => 1]);
        }




















        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());







        $token_complited = TokenTemp::where('patient_phone','=',$patient_phone)->where('priority','!=',0)->where('called','=',0)->count();
      
        if($token_complited == 0)
        {
            $token_complited_nonc = TokenTemp::where('patient_phone','=',$patient_phone)->where('priority','!=',0)->count();
        if($token_complited_nonc  > 0)
        {
            $mobile=$queue->moblie_number;    
            $token_users_name =TokenTemp::where('patient_phone','=',$mobile)->first();
            $token_users_name_count =TokenTemp::where('patient_phone','=',$mobile)->count();

        if($token_users_name_count != 0)
        {
            $patient_name = str_replace(' ', '%20', $token_users_name->patient_name);
            $patient_gender=$token_users_name->patient_gender;

        if($patient_gender == 'M')
        {
            $gender_prifix = 'Mr';
        }else
        {
            $gender_prifix = 'Ms';
        }

        $message ='Dear%20'.$gender_prifix.'%20'.$patient_name.',%20Requested%20service(s)%20are%20completed.%20Please%20click%20on%20the%20link%20https://tinyurl.com/y44ederd%20to%20share%20your%20valuable%20feedback%20and%20rate%20us%20on%20Google%20at%20https://bit.ly/2uYgilK.%20Have%20a%20good%20day.';

        $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        }
        }
        }

        flash('Welcome to expertphp.in!');
        flash()->success('Token Called');
        return redirect()->route('calls');
    }
               

   
     














    
    public function index(Request $request)
    {

   
      $department= Department::whereIn('id', array(Auth::user()->department_id,1))->first();
      $user_id = Auth::user()->id;

      if($user_id == 1)
      {
        $queuess = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('called','=',0)
                    ->get();  
         
      }else
      {
        $queuess = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('user_id','=',$user_id)
                    ->where('called','=',0)
                    ->get();
                  
           }    
      $department = array();
    //  event(new \App\Events\TokenIssued());
      $user_id = Auth::user()->id;
      $role = Auth::user()->role;

      if($role == 'A')
      {
         $department =   Department::all();
      }else
      {
        $department= Department::whereIn('id', array(Auth::user()->department_id,1))->first();
      }

      if($role == 'A')
      {
        $counters =   Counter::all();
      }else
      {
        $department= Department::whereIn('id', array(Auth::user()->department_id))->first();
        $counters= Counter::where('id',$department->counter_id)->get();
      }

  


       return view('user.calls.index', [
            'users' => $this->calls->getUsers(),
            'counters' => $counters,
            'departments' => $department,
      
            'queuess' =>  $queuess,
          
        ]);
    }
    public function status_update(Request $request)
    {
        $callid = $request->callid;
        $call = Call::where('id',$callid)->get()->first();
        $update = Queue::where('id',$call['queue_id'])->update(['status'=>$request->stus,'status_message'=>$request->mes]); 
         if($update)
            return 'success';
        else
            return 'failed';
        //return($request->all());

    }

/*
    public function newCall(Request $request)
    {
        $this->validate($request, [
            'user' => 'bail|required|exists:users,id',
            'counter' => 'bail|required|exists:counters,id',
            'department' => 'bail|required|exists:departments,id',
        ]);


        $user = User::findOrFail($request->user);
        $counter = Counter::findOrFail($request->counter);
        $department = Department::findOrFail($request->department);
 
        $queue =  $department->queues()
                    ->where('called', 0)
                    ->where('user_id',$user->id)
                    ->where('created_at', '>', Carbon::now()->format('Y-m-d 00:00:00'))
                    ->first();

        

        if($queue==null)
         {
          flash()->warning('No Token for this department');
          return redirect()->route('calls');
         }

         $call = $queue->call()->create([
            'department_id' => $department->id,
            'counter_id' => $counter->id,
            'user_id' => $user->id,
            'number' => $queue->number,
            'called_date' => Carbon::now()->format('Y-m-d'),
             ]);
         $queue->called = 1;
         $queue->save();
         


        
         $package = Department::where('id','=',($department->id))->first();

    
         if($package->package_id == 1)
         {
            $department_id =$package->department_ids;
            $departmentids = (explode(',',$department_id));
            count($departmentids);
            
            $queues = Queue::where('department_id','=',($department->id))->where('number','=',($queue->number))->get();
            $department->id;
            $queue->number;

             count($queues).'__'.count($departmentids);

              if(count($queues) <=  count($departmentids))
              {

                foreach ($departmentids as $departmentids_s)
                  {
                    $users = User::where('department_id','=',$departmentids_s)->first();
                    $queuees = Queue::where('department_id','=',($department->id))
                               ->where('number','=',($queue->number))
                               ->where('user_id','=',$users->id)
                               ->get(); 
                    if(count($queuees) == 0 )
                    {

                      $users = User::where('department_id','=',$departmentids_s)->first();

                       $queue = $department->queues()->create([
                               'department_id' => $department->id,
                               'called'  => 0,
                               'number'  => $queue->number,
                               'user_id' => $users->id,
                               'moblie_number' =>$queue->moblie_number
                                ]);



                        $department =  Department::where('id',$department->id)->first();
                        $service_name=$department->name;
                        $service_name=$department->letter;
                        $moblie =$queue->moblie_number; 
                        $user =  User::where('id','=',$users->id)->first();
                        $departmentes = Department::where('id','=',$user->department_id)->first();
                        $next_depart = $departmentes->name;
                        $token_number = $service_name.'-'.$queue->number;
                        $dname =$department->name;
                        $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;
                        $total = $this->calls->getCustomersWaiting($department);

                        $message=  '%20Service%20'.$service_name.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20At%20Department%20'.$next_depart;
                        $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$moblie&source=lmtpld&message=$message";


                        $ch = curl_init();
                        $timeout = 5;
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                        $data = curl_exec($ch);
                        curl_close($ch);

                        $request->session()->flash('department', $department->id);
                        $request->session()->flash('counter', $counter->id);
                        event(new \App\Events\TokenIssued());
                        event(new \App\Events\TokenCalled());
                        flash()->success('Token Called');
                        return redirect()->route('calls');
                    }
                  }
              }
          }


        $request->session()->flash('department', $department->id);
        $request->session()->flash('counter', $counter->id);
        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());
        flash()->success('Token Called');
        return redirect()->route('calls');
    }
*/

/*
$message=  '%20Service%20'.$service_name.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20At%20Department%20'.$next_depart;
      
$url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$moblie&source=lmtpld&message=$message";
*/

  /* public function newCall(Request $request)
    {

        $this->validate($request, [
            'user' => 'bail|required|exists:users,id',
            'counter' => 'bail|required|exists:counters,id',
            'department' => 'bail|required|exists:departments,id',
        ]);


        $user = User::findOrFail($request->user);
        $counter = Counter::findOrFail($request->counter);
        $department = Department::findOrFail($request->department);
        $queue  =  $department->queues()
                    ->where('called', 0)
                    ->where('user_id',$user->id)
                    ->where('created_at', '>', Carbon::now()->format('Y-m-d 00:00:00'))
                    ->first();

   
        if ($queue != NULL){          
            $queue_no = $queue->number;
            }

        if($queue == null){
               flash()->warning('No Token for this department');
                return redirect()->route('calls');
              }

        $call = $queue->call()->create([
            'department_id' => $department->id,
            'counter_id' => $counter->id,
            'user_id' => $user->id,
            'number' => $queue->number,
            'called_date' => Carbon::now()->format('Y-m-d'),
             ]);
        $queue->called = 1;
        $queue->save();
     

        $ldate = date('Y-m-d H:i:s');
        $to_time = strtotime($ldate);
        $from_time = strtotime('2018-09-18 10:00:23');
        $minites = round(abs($to_time - $from_time) / 60,2);


      
  

        $userid =User::where('id','=',$queue->user_id)->first();
        $departmentid =Department::where('id','=',($userid->department_id))->first();
        TokenTemp::where('department_id',($departmentid->id))
        ->where('bill_no',$queue->mr_no)->where('pp','=',NULL)
        ->update(['called' => 1]);


      
        if($queue->department_id != 3){
        
        $temp_tokens = TokenTemp::where('bill_no','=',$queue->mr_no)->get(); 
        
        foreach ($temp_tokens as $temp_token) {
        $qservice_id =$temp_token->service_id;
        $qbill_no  = $temp_token->bill_no;
        $queues = Queue::where('service_id','=',$qservice_id)->where('mr_no','=',$qbill_no)->first();




        //if($queue->created_at  )
        if($temp_token->priority != 0 ){
        if($queues == NULL){
             
              $service =  Services::where('service_id','=',$qservice_id)->first();
              $sdepartment_id = $service->counter_id;
              $department=  Department::where('id','=',$sdepartment_id)->first();
              $user =User::where('department_id','=',$department->id)->first();
              $queues_dup = Queue::where('department_id','=',$sdepartment_id)->where('mr_no','=',$qbill_no)->first();

        if($queues_dup == NULL){
            
            $queue = $department->queues()->create([
            'number' => $queue_no,
            'called' => 0,
            'user_id' =>$user->id,
            'service_id'=>$qservice_id,
            'mr_no'   => $qbill_no
             ]);


        $mobile_number =  TokenUsers::where('mr_no','=',$qbill_no)->first();
        $token_moblie_num= $mobile_number->patient_phone;
        $departmentids = (explode('.',$qbill_no));
     //   $dname =$departmentids[0];// 9739044807
         $dname =$department->name;
        $token_number = ($department->letter!='')?$departmentids[0].'-'.$queue->number:$queue->number;
        $total = $this->calls->getCustomersWaiting($department);
        $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;
        $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$token_moblie_num&source=lmtpld&message=$message";
        $total = $this->calls->getCustomersWaiting($department);

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());
        flash()->success('Token Called');
        return redirect()->route('calls');
        }
        }


        }else{

          $temp_tokenses = TokenTemp::where('bill_no','=',$queue->mr_no)->where('called','=',0)->where('priority','!=',0)->count(); 
             
          
          if($temp_tokenses == 0 ){

            $service =  Services::where('service_id','=',$qservice_id)->first();
            $sdepartment_id = $service->counter_id;
            $department=  Department::where('id','=',$sdepartment_id)->first();
            $user =User::where('department_id','=',$department->id)->first();
            $queues_dup = Queue::where('department_id','=',$sdepartment_id)->where('mr_no','=',$qbill_no)->first();

          if($queues_dup == NULL){
          
            $queue = $department->queues()->create([
            'number' => $queue_no,
            'called' => 0,
            'user_id' =>$user->id,
            'service_id'=>$qservice_id,
            'mr_no'   => $qbill_no
             ]);

       $mobile_number =  TokenUsers::where('mr_no','=',$qbill_no)->first();
       $token_moblie_num= $mobile_number->patient_phone;
       $departmentids = (explode('.',$qbill_no));
       ///$dname =$departmentids[0];// 9739044807
        $dname =$department->name;
       $token_number = ($department->letter!='')?$departmentids[0].'-'.$queue->number:$queue->number;
       $total = $this->calls->getCustomersWaiting($department);
       $message= '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;
       $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$token_moblie_num&source=lmtpld&message=$message";
       $total = $this->calls->getCustomersWaiting($department);

       $ch = curl_init();
       $timeout = 5;
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
       $data = curl_exec($ch);
       curl_close($ch);
       

       event(new \App\Events\TokenIssued());
       event(new \App\Events\TokenCalled());
       flash()->success('Token Called');
       return redirect()->route('calls');
       }
        
  }

 }


}
}
    event(new \App\Events\TokenIssued());
    event(new \App\Events\TokenCalled());
    flash()->success('Token Called');
    return redirect()->route('calls');

}




*/




















         /*
       
         $package = Department::where('id','=',($department->id))->first();
    
         if($package->package_id == 1)
         {
            $department_id =$package->department_ids;
            $departmentids = (explode(',',$department_id));
            count($departmentids);
            
            $queues = Queue::where('department_id','=',($department->id))->where('number','=',($queue->number))->get();
            $department->id;
            $queue->number;

             count($queues).'__'.count($departmentids);

              if(count($queues) <=  count($departmentids))
              {

                foreach ($departmentids as $departmentids_s)
                  {
                    $users = User::where('department_id','=',$departmentids_s)->first();
                    $queuees = Queue::where('department_id','=',($department->id))
                               ->where('number','=',($queue->number))
                               ->where('user_id','=',$users->id)
                               ->get(); 
                    if(count($queuees) == 0 )
                    {

                      $users = User::where('department_id','=',$departmentids_s)->first();

                       $queue = $department->queues()->create([
                               'department_id' => $department->id,
                               'called'  => 0,
                               'number'  => $queue->number,
                               'user_id' => $users->id,
                               'moblie_number' =>$queue->moblie_number
                                ]);















                        $department =  Department::where('id',$department->id)->first();
                        $service_name=$department->name;
                        $service_name=$department->letter;
                        $moblie =$queue->moblie_number; 
                        $user =  User::where('id','=',$users->id)->first();
                        $departmentes = Department::where('id','=',$user->department_id)->first();
                        $next_depart = $departmentes->name;
                        $token_number = $service_name.'-'.$queue->number;
                        $dname =$department->name;
                        $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;
                        $total = $this->calls->getCustomersWaiting($department);

                        $message=  '%20Service%20'.$service_name.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20At%20Department%20'.$next_depart;
                        $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$moblie&source=lmtpld&message=$message";


                        $ch = curl_init();
                        $timeout = 5;
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                        $data = curl_exec($ch);
                        curl_close($ch);

                        $request->session()->flash('department', $department->id);
                        $request->session()->flash('counter', $counter->id);
                        event(new \App\Events\TokenIssued());
                        event(new \App\Events\TokenCalled());
                        flash()->success('Token Called');
                        return redirect()->route('calls');
                    }
                  }
              }
          }
*/
/*
        $request->session()->flash('department', $department->id);
        $request->session()->flash('counter', $counter->id);
        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());
        flash()->success('Token Called');
        return redirect()->route('calls');*/
    


































    public function postDept(Request $request, Department $department)
    {




        $last_token = $this->calls->getLastToken($department);
        if($department->package_id != 1){
        $users = User::where('department_id','=', ($department->id))->first();
        if($last_token) {
                $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$users->id]);
           
              } else {

            $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$users->id
                
            ]);
          }
       
        }else
        {

        
          $did = $department->department_ids;
          $departmentids = (explode(',',$did)); 

          foreach ($departmentids as  $departments_ids)
          {
            $user =User::where('department_id',$departments_ids)->first();
         
            $que = Queue::where('user_id','=',$user->id)->where('called','=',0)->get();
                 
            $counter_array[]= array ('count' => count($que),'userid' =>$user->id);
          }

          $min_counter_array=  min($counter_array);
          $min_user=  $min_counter_array['userid'];







          if($last_token)
          {
            $queue = $department->queues()->create([
            'number' => ((int)$last_token->number)+1,
            'called' => 0,
            'user_id' =>$min_user ]);
          }else
          {
            $queue = $department->queues()->create([
            'number' => $department->start,
            'called' => 0,
            'user_id'  =>$min_user ]);
          }
        }

        $total = $this->calls->getCustomersWaiting($department);
        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $department->name);
        $request->session()->flash('number', ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('calls');
       
    }

/*
     public function postDept1(Request $request, Department $department,$elem)
    {
      
          $token_users =  TokenUsers::where('bill_no','=',$elem)->get();


           foreach ($token_users as $token_user) {
            
                $service_id=    $token_user->service_id;
                $bill_no =    $token_user->bill_no;


                DB::table('token_temp')->insert(
                   ['service_id' => $service_id, 'bill_no' => $bill_no]
              );



           }



          // return $data_array;




      


        $last_token = $this->calls->getLastToken($department);
    
       if($department->package_id != 1){
       $users = User::where('department_id','=', ($department->id))->first();
       if($last_token) {
                $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$users->id,
                'moblie_number' =>$elem]);
           
              } else {

            $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$users->id,
                'moblie_number' =>$elem
                
            ]);
          }
       
        }else
        {

          $did = $department->department_ids;
          $departmentids = (explode(',',$did)); 

          foreach ($departmentids as  $departments_ids)
          {
            $user =User::where('department_id',$departments_ids)->first();
         
            $que = Queue::where('user_id','=',$user->id)->where('called','=',0)->get();
                 
            $counter_array[]= array ('count' => count($que),'userid' =>$user->id);
          }

          $min_counter_array=  min($counter_array);
          $min_user=  $min_counter_array['userid'];


          if($last_token)
          {
            $queue = $department->queues()->create([
            'number' => ((int)$last_token->number)+1,
            'called' => 0,
            'user_id' =>$min_user,
            'moblie_number' =>$elem ]);
          }else
          {
            $queue = $department->queues()->create([
            'number' => $department->start,
            'called' => 0,
            'user_id'  =>$min_user,
            'moblie_number' =>$elem ]);
          }
        }

  

 // $dname =$department->name;// 9739044807


      // echo 'http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination='.$elem.'&source=ACS&message='.$message.;

 // $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;

 
 
  






       $total = $this->calls->getCustomersWaiting($department);


    ////// $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;




 //$message =$dname.'%20Your%20Token%20%20Number%20'.$token_number;


 ///// $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$elem&source=lmtpld&message=$message";


  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  $data = curl_exec($ch);
  curl_close($ch);



        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $department->name);
        $request->session()->flash('number', ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('add_to_queue');
       
    }


*/

  public function postDept1(Request $request, Department $department,$elem)
  
    {
        
       $token_users =  TokenUsers::where('mr_no','=',$elem)->get();

       if(count($token_users) == 0){
       $last_token = $this->calls->getLastToken($department);
       $users = User::where('department_id','=', ($department->id))->first();
    
       if($last_token) {
                $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$users->id,
                'moblie_number'=>$elem,
                'service_id'=>0,
                 'mr_no'   =>  0 
                ]);
          }else
          {
            $queue = $department->queues()->create([
                     'number' => $department->start,
                     'called' => 0,
                     'user_id'  =>$users->id,
                     'moblie_number' =>$elem,
                     'service_id'=>0,
                     'mr_no'   =>  0
                    ]);
          }
       
        $dname =$department->name;// 9739044807
        $token_number = ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number;
        $total = $this->calls->getCustomersWaiting($department);
        $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;
        $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$elem&source=ACSQUE&message=$message";
       // $url = "http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=$p&senderid=APOLLO&dest_mobileno=$elem&message=$message";
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $department->name);
        $request->session()->flash('number', ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('add_to_queue');

    }else

    {

      foreach ($token_users as $token_user) {
                $service_id=    $token_user->service_id;
                $bill_no =    $token_user->mr_no;
                $services =Services::where('service_id','=',$service_id)->first();


        // $services->counter_id.'-'.$services->priority.''.$services->pp;

              $row =array(
                'service_id'    =>  $service_id,
                'bill_no'       =>  $bill_no,
                'department_id' =>  $services->counter_id,
                'priority'      =>  $services->priority,
                'pp'            =>  $services->pp
                );

              DB::table('token_temp')->insert($row);
           }

  /* return   $users_tokens = DB::table('token_temp')->select('*')->where('bill_no','=',$elem)->get();

        


          foreach ($users_tokens as $users_token) {
                  $usid  = $users_token->service_id;
                   $mr_no = $users_token->bill_no;
                  $dep_id =0;
                  $services=  DB::table('services')->select('id','name','service_id','department_id','counter_id')->where('service_id','=',$usid)->get();
                  foreach ($services as $service) { 
                  $dep_id =  $service->counter_id; 
                 
                  }

      $que = Queue::where('department_id','=',$dep_id)->get();

      $counter_array[]= array ('count' => count($que),'department_id' =>$dep_id,'service_id' =>$usid,);
      }

     $counter_array;
     $min_counter_array=  min($counter_array);
     $min_service_id = $min_counter_array['service_id'] ;

    $departments=  DB::table('services')->select('id','name','service_id','department_id','counter_id')->where('service_id','=',$min_service_id)->get();

    foreach ($departments as $department)  {
              $counter_id =  $department->counter_id; 

            }
    
   $department=  Department::where('id','=', $counter_id)->first();

   
   $user =User::where('department_id','=',$department->id)->first();
*/


   return $users_tokens = DB::table('token_temp')->select('*')->where('bill_no','=',$elem)->where('department_id','=',3)->where('priority','=',1)->where('pp','=',NULL)->get();


 // $user_queues =QueueUsers::where('mr_no','=',$elem)->where('priority','=',1)->where('pp','=',NULL)->get();

      foreach ($users_tokens as $users_token){
      
             $department_id = $users_token->department_id;}
             $department=  Department::where('id','=',$department_id)->first();
             $service_id = $users_token->service_id;
             $user =User::where('department_id','=',$department->id)->first();
             $last_token = $this->calls->getLastToken($department);


                
    if($last_token)
      {
        $queue = $department->queues()->create([
                 'number' => ((int)$last_token->number)+1,
                 'called' => 0,
                 'user_id' => $user->id,
                
                 'mr_no'   => $elem

            ]);
          }else
          {
            $queue = $department->queues()->create([
            'number' => $department->start,
            'called' => 0,
            'user_id' =>$user->id,
            'mr_no'   =>  $elem
         
            ]);
          }
        
    $token_name =explode(".",$elem);


    $mobile_number =  TokenUsers::where('mr_no','=',$elem)->first();

    $token_moblie_num= $mobile_number->patient_phone;

    $token_moblie_num= $token_moblie_num;

   $dname =$department->name;// 9739044807
   $token_number = ($department->letter!='')?$token_name[0].'-'.$queue->number:$queue->number;

   $total = $this->calls->getCustomersWaiting($department);

  //$message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;







$message='%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;




 //$message =$dname.'%20Your%20Token%20%20Number%20'.$token_number;


  $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$token_moblie_num&source=lmtpld&message=$message";









$total = $this->calls->getCustomersWaiting($department);




  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  $data = curl_exec($ch);
  curl_close($ch);

    


        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name',$department->name);
        $request->session()->flash('number', ($department->letter!='')?$token_name[0].'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('add_to_queue');
       
    }

}



     public function postPack(Request $request, Package $package)
      {

     $department_id=Department::where('package_id',$package->id)->first();
     $last_token = $this->calls->getLastPackToken($package);
      // Department $department
      if($last_token) {
            $queue = $package->queues()->create([
                'department_id'=> $department_id->id,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
            ]);
        } else {
            $queue = $package->queues()->create([
                'department_id'=> $department_id->id,
                'number' => $package->start,
                'called' => 0,
            ]);
        }

        $total = $this->calls->getCustomersWaitingp($package);
        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $package->name);
        $request->session()->flash('number', ($package->letter!='')?$package->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('calls');
    }


    public function recall(Request $request)

    {
     
      
    $call = Call::find($request->call_id);

       
     $queue = Queue::where('id','=',$call->queue_id)->first();

$department_id = $queue->department_id;
$token_prefix = $queue->token_prefix;
$number       = $queue->number;
$patient_phone = $queue->moblie_number;
$user_id  = $queue->user_id;

$token_number = $token_prefix.'-'.$number;

$department = Department::where('id','=',$department_id)->first();

$counter_name = Counter::where('id','=',$department->counter_id)->first(); 
$counter_name_array = explode("-",  $counter_name->name);
$coutnername =str_replace(' ', '%20', $counter_name_array[0]);
$floor =str_replace(' ', '%20', $counter_name_array[1]);

$department_name =str_replace(' ', '%20', $department->name);


 $temptoken = TokenTemp::where('patient_phone',$patient_phone)->first();
 $temptoken_count = TokenTemp::where('patient_phone',$patient_phone)->count();

if($temptoken_count != 0){
$patient_name=$temptoken->patient_name;
$patientname = str_replace(' ', '%20', $patient_name);


 $patient_gender=$temptoken->patient_gender;
                      if($patient_gender == 'M')
                        {
                            $gender_prifix = 'Mr';

                        }else{
                            $gender_prifix = 'Ms';

                        }
                        $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Recall%20from%20'.$department_name.'%20Please%20proceed%20to%20'.$coutnername.'%20'.$floor.'.';

}else{

  $message= 'Recall%20Token%20No.%20'.$token_number.'%20please%20proceed%20to%20the%20billing%20counter';
}









 $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";


         $ch = curl_init();
         $timeout = 5;
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
         $data = curl_exec($ch);
         curl_close($ch);





        $new_call = $call->replicate();
        $new_call->save();
        $call->delete();
        event(new \App\Events\TokenCalled());
        flash()->success('Token ReCalled');
        return $new_call->toJson();





        
    }

    

    public function getDeparts()
    {
      $user_id = Auth::user()->id;

      if($user_id == 1)
      {

        /*return  $queuess1 =  Queue::with('department')
                          ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                          ->where('called','=',0)
                          ->get();  
*/

                       return    $queuess1= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   ->where('called','=',0)
                    //->orderBy('queues.priority','asc')
                    ->orderBy('queues.created_at', 'asc')
                    ->get();  







      }else
      {
        /*return  $queuess1 =  Queue::with('department')
                          ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                          ->where('user_id','=',$user_id)
                          ->where('called','=',0)
                          ->get();  */

                  return         $queuess1 = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('called','=',0)
                    ->where('user_id','=', $user_id)
                   // ->orderBy('queues.priority','DESC')
                    ->orderBy('queues.created_at', 'asc')
                    ->get();  
            
            

      }

    }
}






/*      MOBILE----SMS
        $dname =$department->name;
        $token_number = ($department->letter!='')?$departmentids[0].'-'.$queue->number:$queue->number;
        $total = $this->calls->getCustomersWaiting($department);
        $message=  '%20Service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;
        $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-lmtpld&password=1478145&type=0&dlr=1&destination=$token_moblie_num&source=lmtpld&message=$message";
        $total = $this->calls->getCustomersWaiting($department);

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
*/