<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Response;
use App\Repositories\AddToQueueRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use Session;
use App\Models\Online;

class ScheduleQueueController  extends Controller
{
  protected $calls;
  public function __construct(AddToQueueRepository $add_to_queues)
  {
   
    $this->add_to_queues = $add_to_queues;
  }

  public function index(Request $request)
  {

    $service_users = Online::where('user_id','!=',NULL)->get();
    $service_user_id =array();

    foreach ($service_users as $service_user)
    {
      $service_user_id[]=$service_user['user_id'];
    }
    $users_tokens_pp =NULL;
    $users_token_pp_called_time =NULL;
    
   
     $tokentemps =  TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->whereIn('priority',[1,2])->where('called','=',0)->distinct()->get(['mr_no','patient_phone']); 
      foreach ($tokentemps  as $tokentemp)
      {
            $patient_phone = $tokentemp->patient_phone;
             $mr_no = $tokentemp->mr_no;
            $count_array = array();
            $min_user_id =0;
    
          
            $queue_count =  Queue::where('mr_no',$mr_no)->where('called',0)->count();
           if($queue_count == 0)
            {
             
              $tokenusers_priority_count = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('mr_no',$mr_no)->where('pp','=',1)->orderBy('priority', 'asc')->count();
               if($tokenusers_priority_count == 0)
              {
              
               $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->whereIn('priority',[1,2])->where('mr_no',$mr_no)->distinct()->get(['department_id','location_id','gender','mr_no','patient_name','patient_phone','priority']);
              
               foreach ($tokenusers as $tokenuser)
                {
                  $department_ids[] = $tokenuser->department_id;
                  $location_id =$tokenuser->location_id;
                  $patient_gender= $tokenuser->gender;
                   $priority = $tokenuser->priority;  
                  $patient_name=$tokenuser->patient_name;
                  $patient_moblie=substr($tokenuser->patient_phone,3);
                  $location_id = $tokenuser->location_id;
                  
                  
             // }
              
               $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
              
              if(count($count_array) != 0)
              {
                  $min_counter_array=min($count_array);
                  $min_user_id=$min_counter_array['user_id'];
              
              
              
              $user = User::where('id',$min_user_id)->first();
              $department = Department::where('id',($user->department_id))->first();
              $last_token = $this->add_to_queues->getLastToken($department);
              $token_prefix ='AHL';
              //if()
            
              if($last_token)
              {
               // return $priority;
                $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'priority' => $priority,
                'called' => 0,
                'department_id' => $department->id,
                'user_id' =>$user->id,
                'moblie_number'=>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
               }else
               {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'priority' => $priority,
                'user_id'  =>$user->id,
                'moblie_number' =>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
              }
              
              
              }
             
             
         
              } }else
             {
                
              $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->whereIn('priority',[1])->where('mr_no',$mr_no)->distinct()->get(['department_id','location_id','gender','mr_no','gender','patient_name','patient_phone','priority']);
              $department_ids =array();
              foreach ($tokenusers as $tokenuser)
              {
                  $department_ids[] = $tokenuser->department_id;
                  $location_id =$tokenuser->location_id;
                  $patient_gender= $tokenuser->gender;
                  $priority = $tokenuser->priority;  
                  $patient_name=$tokenuser->patient_name;
                  $patient_moblie=substr($tokenuser->patient_phone,3);
                  $location_id = $tokenuser->location_id;
                  
                  
              }
              
               $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  $count_array =array();
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
              
              if(count($count_array) != 0)
              {
                  $min_counter_array=min($count_array);
                  $min_user_id=$min_counter_array['user_id'];
              
              
              
              $user = User::where('id',$min_user_id)->first();
              $department = Department::where('id',($user->department_id))->first();
              $last_token = $this->add_to_queues->getLastToken($department);
              $token_prefix ='AHL';
            
              if($last_token)
              {
                $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'priority' =>$priority,
                'department_id' => $department->id,
                'user_id' =>$user->id,
                'moblie_number'=>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
           }else
               {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'priority'=>$priority,
                'user_id'  =>$user->id,
                'moblie_number' =>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
              }
              
              
              }
                                                 
          }
            
      
          
          
      }

   // return   $queue_count =  Queue::where('status',1)->count();

/*        $queue_count =  Queue::where('status',2)->count();
           if($queue_count != 0)
          {
               $last_tokens =Queue::where('status',2)->get();
               $ldate = date('Y-m-d H:i:s');
               $to_time = strtotime($ldate);
              foreach($last_tokens as $last_token){
               $sla = Department::where('id',$last_token['department_id'])->first();
               $token_update_time =$last_token['updated_at']; 
               $from_time = strtotime($token_update_time);
               $minites = round(abs($to_time - $from_time) / 60,2);
               $seconds = ($minites * 60);
               $elapsed_seconds = $sla->service_time;

              if($seconds > $elapsed_seconds)    
              {
               $ultraque =TokenTemp::where('priority',2)->where('mr_no',$last_token['mr_no'])->distinct()->get();
               $token_prefix ='AHL';

               foreach ($ultraque as $ultra) {
                 //  return $last_token['mr_no'];
                 $ultraquee[] = array(
                'department_id'=> $ultra['department_id'],
                'token_prefix' => $token_prefix,
                'number' => $sla->start,
                'called' => 0,
                'user_id'  =>$last_token['user_id'],
                'moblie_number' =>substr($ultra['patient_phone'], 3),
                'patient_name' =>$ultra['patient_name'],
                'gender' =>$ultra['patient_gender'],
                'mr_no'   =>  $last_token['mr_no']

                 );

               }

                Queue::insert($ultraquee); 
                return "haii";

              }   
          }
        }*/

       //'priority',[1,2]
    }
/*     $tokentemps =  TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->whereIn('priority',[1,2])->where('called','=',1)->distinct()->get(['mr_no','patient_phone']); 
     if($tokentemps)
     {
      foreach ($tokentemps as $key => $tokentemp) {
        $mr_no = $tokentemp->mr_no;
            $queue =  Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->whereIn('priority',[1,2])->where('mr_no',$mr_no)->where('status','=',1)->distinct()->get(['mr_no']); 

        $service_user_id[]=$service_user['user_id'];
      }
          return $queue; 


     }*/
    $priority1 =  Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('priority','=',1)->where('status','=',1)->distinct()->get(['mr_no']); 
    if(isset($priority1))
      {
         $priority2 =  Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('priority','=',2)->where('status','=',1)->distinct()->get(['mr_no','department_id','updated_at']);
        foreach ($priority2 as $key => $priority) {
            $mr_no = $priority->mr_no;
               $ldate = date('Y-m-d H:i:s');
                $to_time = strtotime($ldate); 
               $sla = Department::where('id', $priority->department_id)->first();
               $token_update_time =$priority->updated_at; 
               $from_time = strtotime($token_update_time);
               $minites = round(abs($to_time - $from_time) / 60,2);
               $seconds = ($minites * 60);
              $elapsed_seconds = $sla->service_time;
           $queue_count =  Queue::where('mr_no',$mr_no)->where('priority','=',3)->count();
           if($queue_count == 0)
            {
          $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('priority','=',3)->where('mr_no',$mr_no)->distinct()->get(['department_id','location_id','mr_no','patient_name','patient_phone']);

                 foreach ($tokenusers as $tokenuser)
                {
                  $department_ids[] = $tokenuser->department_id;
                  $location_id =$tokenuser->location_id;
                  $patient_gender= $tokenuser->gender;
                  $patient_name=$tokenuser->patient_name;
                  $patient_moblie=substr($tokenuser->patient_phone,3);
                  $location_id = $tokenuser->location_id;
                                                   
                 }

                   $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  $count_array =array();
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
              
              if(count($count_array) != 0)
              {
                  $min_counter_array=min($count_array);
                  $min_user_id=$min_counter_array['user_id'];
              
              
              
              $user = User::where('id',$min_user_id)->first();
              $department = Department::where('id',($user->department_id))->first();
              $last_token = $this->add_to_queues->getLastToken($department);
              $token_prefix ='AHL';
             
            if($seconds > $elapsed_seconds)    
              {
              if($last_token)
              {
                //return '1';
                $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'department_id' => $department->id,
                'priority' => 3,
                'user_id' =>$user->id,
                'moblie_number'=>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
           }else
               {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user->id,
                'priority' =>3,
                'moblie_number' =>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
              }
              
              }
            }
          
         }
        } 


      }


  }


}

    






/*



 if($queue_count == 0)
        {
           $tokenusers_priority_count = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('mr_no',$mr_no)->where('pp','=',1)->orderBy('priority', 'asc')->count();
           
        if($tokenusers_priority_count != 0)
        {
            $count_array=NULL;
            $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->whereIn('priority',[1])->where('mr_no',$mr_no)->where('pp','=',NULL)->orderBy('priority', 'asc')->get(); 

              foreach($tokenusers as $users_token)
                                  {
                                      $department_ids[] = $users_token->department_id;
                                      $location_id =$users_token->location_id;
                                    
                                  }

                 

               $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  // return $service_user_id;
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
                   
                    if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                       $min_user_id=$min_counter_array['user_id'];
                     }

                  $user = User::where('id',$min_user_id)->first();
                         $department = Department::where('id',($user->department_id))->first();
                //return  $user;       

                $last_token = $this->add_to_queues->getLastToken($department);
                 $token_prefix ='CON';
            
                if($last_token)
                {
                  $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'department_id' => $department->id,
                'user_id' =>$user->id,
                'moblie_number'=>$patient_phone,
                'patient_name' =>$tokentemp->patient_name,
                'gender' =>$tokentemp->patient_gender,
                'mr_no'   =>  $mr_no
                ]);
          }else
               {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user->id,
                'moblie_number' =>$patient_phone,
                'patient_name' =>$tokentemp->patient_name,
                'gender' =>$tokentemp->patient_gender,
                'mr_no'   =>  $mr_no
                ]);
              }
           }else {
            

              $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->whereIn('priority',[1,2])->where('mr_no',$mr_no)->orderBy('priority', 'asc')->get(); 

              foreach($tokenusers as $users_token)
                                  {
                                      $department_ids[] = $users_token->department_id;
                                      $location_id =$users_token->location_id;
                                    
                                  }

                 $department_ids;

               $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  // return $service_user_id;
            $count_array  =array();
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
                   
                    if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                       $min_user_id=$min_counter_array['user_id'];
                     }

                  $user = User::where('id',$min_user_id)->first();
                         $department = Department::where('id',($user->department_id))->first();
                //return  $user;       

                $last_token = $this->add_to_queues->getLastToken($department);
                 $token_prefix ='CON';
            
                if($last_token)
                {
                  $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'department_id' => $department->id,
                'user_id' =>$user->id,
                'moblie_number' =>substr($patient_phone,3),
                'patient_name' =>$tokentemp->patient_name,
                'gender' =>$tokentemp->patient_gender,
                'mr_no'   =>  $mr_no
                ]);
          }else
               {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user->id,
                 'moblie_number' =>substr($patient_phone,3),
                'patient_name' =>$tokentemp->patient_name,
                'gender' =>$tokentemp->patient_gender,
                'mr_no'   =>  $mr_no
                ]);
              }

                }








       }






























































          

          if($minites > $elapsed_minites)
          {
           

            
              foreach ($tokenusers as $tokenuser)
              {
              $queue_count =  Queue::where('moblie_number',$patient_phone)->where('mr_no',$mr_no)->where('called',0)->count();
              if($queue_count == 0)
                {
                  $service_id =$tokenuser->service_id;           
                  $department_id =$tokenuser->department_id;
                  $patient_phone =$tokenuser->patient_phone;
                  $location_id =$tokenuser->location_id;
                  $patient_name =$tokenuser->patient_name;
                  $count_array =array();
                 
                  foreach($tokenusers as $users_token)
                                  {
                                      $department_ids[] = $users_token->department_id;
                                      $location_id =$users_token->location_id;
                                    
                                  }



                  $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                // return $users;
                  foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }

                   $count_array;

                  if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                       $min_user_id=$min_counter_array['user_id'];

                      if(in_array($min_user_id, $service_user_id))
                      {

                          $user = User::where('id',$min_user_id)->first();
                          $department = Department::where('id',($user->department_id))->first();
                          $queue = $department->queues()->create([
                                                'token_prefix' => $last_tokens->token_prefix,  
                                                'number' =>$last_tokens->number,
                                                'called' => 0,
                                                'moblie_number' => $patient_phone,
                                                'user_id' =>$min_user_id,
                                                'service_id'=> $service_id,
                                                'location_id' =>$location_id,
                                                'mr_no'   => $mr_no,
                                                'patient_name' =>$patient_name
                                                ]);

                         
                        
                          $patient_phone = $patient_phone; 
                          $dname =$department->name;
                          $queue_token_prefix =$last_tokens->token_prefix;
                          $token_number = $last_tokens->number;
                         
                          $total = $this->calls->getCustomersWaiting($department);
                          $departmentname = str_replace(' ', '%20', $dname);
                          $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                          $patient_name=$temptoken->patient_name;
                          $patientname = str_replace(' ', '%20', $patient_name);
                          $department_counter = Department::where('id','=',$department->id)->first();
                          $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                          
                          $counter_name_array = explode("-",  $counter_name->name);
                          $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                          $floor =str_replace(' ', '%20', $counter_name_array[1]);
                          $servicename =str_replace(' ', '%20', $department_counter->name);
                          $servicestime = Department::where('id',$department->id)->first();
                          $service_time =$servicestime->service_time; 
                          $approx_time =  $service_time* ($total-1);
                          $patient_gender=$temptoken->patient_gender;

                          if($patient_gender == 'M')
                          {
                            $gender_prifix = 'Mr';
                          }
                          else
                          {
                            $gender_prifix = 'Ms';
                          }

                          $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.',%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


  

                          $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';


                            $ch = curl_init();
                            $timeout = 5;
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                            $data = curl_exec($ch);
                            curl_close($ch);
                            event(new \App\Events\TokenIssued());
                            event(new \App\Events\TokenCalled());
                            }
                          }



                }
              }
            }
            else
            {
              $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('status',0)->where('called','=',0)->whereIn('priority',[1])->where('mr_no',$mr_no)->where('pp','=',NULL)->orderBy('priority', 'asc')->get();


               
              foreach ($tokenusers as $tokenuser)
              {
              $queue_count =  Queue::where('moblie_number',$patient_phone)->where('mr_no',$mr_no)->where('called',0)->count();
              if($queue_count == 0)
                {
                  $service_id =$tokenuser->service_id;           
                  $department_id =$tokenuser->department_id;
                  $patient_phone =$tokenuser->patient_phone;
                  $location_id =$tokenuser->location_id;
                  $patient_name =$tokenuser->patient_name;
                  $count_array =array();
                  $users =User::where('department_id',$department_id)->get();

                  foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }

                  $count_array;
                  if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                      $min_user_id=$min_counter_array['user_id'];

                      if(in_array($min_user_id, $service_user_id))
                      {

                          $user = User::where('id',$min_user_id)->first();
                          $department = Department::where('id',($user->department_id))->first();
                          $queue = $department->queues()->create([
                                                'token_prefix' => $last_tokens->token_prefix,  
                                                'number' =>$last_tokens->number,
                                                'called' => 0,
                                                'moblie_number' => $patient_phone,
                                                'user_id' =>$min_user_id,
                                                'service_id'=> $service_id,
                                                'location_id' =>$location_id,
                                                'mr_no'   => $mr_no,
                                                'patient_name' =>$patient_name
                                                ]);

                         
                          $patient_phone = $patient_phone; 
                          $dname =$department->name;
                          $queue_token_prefix =$last_tokens->token_prefix;
                          $token_number = $last_tokens->number;
                         
                          $total = $this->calls->getCustomersWaiting($department);
                          $departmentname = str_replace(' ', '%20', $dname);
                          $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                          $patient_name=$temptoken->patient_name;
                          $patientname = str_replace(' ', '%20', $patient_name);
                          $department_counter = Department::where('id','=',$department->id)->first();
                          $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                          
                          $counter_name_array = explode("-",  $counter_name->name);
                          $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                          $floor =str_replace(' ', '%20', $counter_name_array[1]);
                          $servicename =str_replace(' ', '%20', $department_counter->name);
                          $servicestime = Department::where('id',$department->id)->first();
                          $service_time =$servicestime->service_time; 
                          $approx_time =  $service_time* ($total-1);
                          $patient_gender=$temptoken->patient_gender;

                          if($patient_gender == 'M')
                          {
                            $gender_prifix = 'Mr';
                          }
                          else
                          {
                            $gender_prifix = 'Ms';
                          }

                          $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.',%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


  

                          $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';


                            $ch = curl_init();
                            $timeout = 5;
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                            $data = curl_exec($ch);
                            curl_close($ch);
                            event(new \App\Events\TokenIssued());
                            event(new \App\Events\TokenCalled());
                            }
                          }
                

























                }
              }










            }








          }
       
      }
    }
  }
}   

              */