<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Response;
use App\Repositories\AddToQueueRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use Session;
use App\Models\Online;

class SecondQueueController  extends Controller
{
  protected $calls;
  public function __construct(AddToQueueRepository $add_to_queues)
  {
   
    $this->add_to_queues = $add_to_queues;
  }

  public function index(Request $request)
  {

    $service_users = Online::where('user_id','!=',NULL)->get();
    $service_user_id =array();

    foreach ($service_users as $service_user)
    {
      $service_user_id[]=$service_user['user_id'];
    }
    $users_tokens_pp =NULL;
    $users_token_pp_called_time =NULL;   
 
    $tokenusers =  Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('priority','=',3)->where('status','=',1)->distinct()->get(['mr_no','department_id','updated_at']);
        foreach ($tokenusers as $key => $tokenuser) {
             $mr_no = $tokenuser->mr_no;
                        //return $tokenuser;//return "fsdf";

               
          $queue_count =  Queue::where('mr_no',$mr_no)->whereIn('priority',[4,5,6,7])->get()->toArray();
           $priority_arr =   array_column($queue_count, 'priority');
          // if($queue_count == 0)
           // {
           $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->whereIn('priority',[4,5,6,7])->where('mr_no',$mr_no)->distinct()->get(['department_id','location_id','mr_no','patient_name','patient_phone','priority']);
                 foreach ($tokenusers as $tokenuser)
                {
                  $department_ids[] = $tokenuser->department_id;
                  $location_id =$tokenuser->location_id;
                  $patient_gender= $tokenuser->gender;
                  $priority= $tokenuser->priority;
                  $patient_name=$tokenuser->patient_name;
                  $patient_moblie=substr($tokenuser->patient_phone,3);
                  $location_id = $tokenuser->location_id;
                                                   
                 }
                
                if(empty($tokenusers)){
                   $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  $count_array =array();
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
            
              if(count($count_array) != 0)
              {
                  $min_counter_array=min($count_array);
                  $min_user_id=$min_counter_array['user_id'];
              
              
              
              $user = User::where('id',$min_user_id)->first();
              $department = Department::where('id',($user->department_id))->first();
              $last_token = $this->add_to_queues->getLastToken($department);
              $token_prefix ='AHL';
           $queue_count =  Queue::where('mr_no',$mr_no)->where('called','=',1)->latest()->first();


          /* return $queue_count =  Queue::where('mr_no',$mr_no)->where('called','=',1)->whereIn('priority',[3,4,5,6,7])->distinct()->get(['mr_no','department_id','updated_at']);*/
               $ldate = date('Y-m-d H:i:s');
             //  foreach ($queue_count as $que) {
              $to_time = strtotime($ldate); 
               $sla = Department::where('id', $queue_count->department_id)->first();
                $token_update_time =$queue_count->updated_at; 
               $from_time = strtotime($token_update_time);
               $minites = round(abs($to_time - $from_time) / 60,2);
               $seconds = ($minites * 60);
              $elapsed_seconds = $sla->service_time;
           //  }
               
            if($seconds > $elapsed_seconds)    
              {
                if (!in_array($priority, $priority_arr)) {
                 
                $queue = $department->queues()->create([
                'token_prefix' => $queue_count->token_prefix,
                'number' => $queue_count->number,
                'called' => 0,
                'user_id'  =>$user->id,
                'priority' =>$priority,
                'moblie_number' =>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
            }
                         
              }

            }
          
      //   }
     //0 //  } 
       }
     }

      }
}

