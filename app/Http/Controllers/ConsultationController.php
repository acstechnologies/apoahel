<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Repositories\CallRepository;
use App\Models\User;      
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use App\Models\Online;

class ConsultationController extends Controller
{
  protected $calls;
  public function __construct(CallRepository $calls)
  {
    $this->calls = $calls;
  }

  public function index(Request $request)
  {

    $service_users = Online::where('user_id','!=',NULL)->get();
    foreach ($service_users as $service_user)
    {
      $service_user_id[]=$service_user['user_id'];
    }
 $queues= Queue::with('package')->where('location_id',7)
                  ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',1)->where('defaults','=',1)->orderBy('queues.created_at', 'asc')
                   ->get(); 

                /*   $queues= Queue::with('package')->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',1)->where('defaults','=',1)->orderBy('queues.created_at', 'asc')
                   ->get(); 
      */
     /*$mobile ='9739211318';
      $queues= Queue::with('package')->where('moblie_number','=',$mobile)
                  ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',1)->where('defaults','=',1)->orderBy('queues.created_at', 'asc')
                   ->get(); */
      
      
      
      
    foreach($queues as $queue)
    {
      $patient_phone =$queue->moblie_number;
       $patient_phone_lists =TokenTemp::where('patient_phone',$patient_phone)->where('status',0)->where('remarks',2)->distinct()->get(['mr_no','patient_name','patient_phone','location_id']);
    
      foreach ($patient_phone_lists as $patient_phone_list)
      {
         $patient_phone =$patient_phone_list->patient_phone;
        $mr_no = $patient_phone_list->mr_no;

        $queues_count= Queue::with('package')->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('moblie_number',$patient_phone)->count(); 


        if($queues_count == 0)
        {

          $users_tokens = TokenTemp::where('patient_phone',$patient_phone)
                        ->where('called', '=', 0)->where('status',0)->whereIn('priority',[0])->where('remarks',2)
                        ->where('pp','=',NULL)->distinct()->get(['bill_no','location_id','patient_name','patient_phone','department_id','mr_no','updated_at','service_id']);
          $users_tokens_c = TokenTemp::where('patient_phone',$patient_phone)->where('status',0)->where('remarks',2)->where('called', '=', 0)->where('priority','=',0)->where('pp','=',NULL)->distinct()->count();

          $count_array =array();

          if($users_tokens_c != 0)
          {
            foreach ($users_tokens as $users_token)
            {
                
              $mr_no = $users_token->mr_no;
              $mobile =$users_token->patient_phone;
              $qservice_id = $users_token->service_id;
              $location_id = $users_token->location_id; 
               $service =  Services::where('service_id','=',$qservice_id)->first();
           
               $sdepartment_id =  $service->counter_id;
              $department = Department::where('location_id','=',$location_id)->where('group_id','=',$sdepartment_id)->first();
              
              
              if($department == NULL){

  $text =$qservice_id.'--'.$mobile;

        $message ='unable%20to%20find'.$text;

               // $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";
       /* $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno=9959576026&msgtype=UNI&message='.$message.'&response=Y';

                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $data = curl_exec($ch);
                curl_close($ch);
                event(new \App\Events\TokenIssued());
                event(new \App\Events\TokenCalled());


*/



              }












              if($department != NULL){


                 $service;



              



              $departmentid =$department->id;
              $user =User::where('department_id','=',$departmentid)->first();
               $patient_name =$users_token->patient_name;


            
              if(in_array($user->id, $service_user_id)){
              $user_count =User::where('department_id','=',$departmentid)->count();
              $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
              $count_array[]=array ('count' => $counts,'user_id'=>$user->id);

            }
              
              $count_array;

            

           


              $last_tokens =Queue::where('mr_no','=', $mr_no)->orderBy('updated_at', 'desc')->first();


              if($last_tokens == NULL)
              {
                 $last_tokens =Queue::where('moblie_number','=',$patient_phone)->orderBy('updated_at', 'desc')->first();


              }
              




              $sla = Department::where('id',($last_tokens->department_id))->first();
              $token_update_time =$last_tokens->updated_at ; 
              $token_mrno = $last_tokens->bill_no;
              $token_mobile =$last_tokens->patient_phone;
              $ldate = date('Y-m-d H:i:s');
              $to_time = strtotime($ldate);
              $from_time = strtotime($token_update_time);
               $minites = round(abs($to_time - $from_time) / 60,2);
              $mr_no =  $mr_no;
              $elapsed_minites =$sla->service_time;
            

               if(count($count_array) != 0){
                  
            if($minites > $elapsed_minites){

              $users_service = $users_token->service_id;
              $service_name = Services::where('service_id','=',$users_service)->first();
              $service_description = str_replace(' ', '%20',$service_name->name);
              
              
              $min_counter_array=min($count_array);

              $min_user_id=$min_counter_array['user_id'];
              $foruserid= user::where('id','=',$min_user_id)->first();    
              $department =Department::where('id','=',$foruserid->department_id)->where('location_id','=',$location_id)->first();
              $last_token = $this->calls->getLastToken($department);
              
               $queues_count_check= Queue::where('called','=', 0)->where('moblie_number','=',$patient_phone)->where('mr_no','=',$mr_no)->count();
              if($queues_count_check == 0)
              {

                if(in_array($min_user_id, $service_user_id))
                {
                     
                  
                  

               $queue = $department->queues()->create([
                    'department_id' => $department->id,
                    'token_prefix'  => $queue->token_prefix,
                    'number' => $queue->number,
                    'called' => 0,
                    'user_id'  =>$foruserid->id,
                    'moblie_number' =>$patient_phone,
                    'service_id'=>$qservice_id,
                    'mr_no' =>$mr_no,
                     'location_id' =>$location_id,
                    'bill_no'   =>  0,
                   'patient_name' =>$patient_name
                    ]);

               
                $patient_phone =$patient_phone;  
                $dname =$department->name;
                $token_number = $queue->number;
                $departmentname = str_replace(' ', '%20', $dname);
                $total = $this->calls->getCustomersWaiting($department);
                $temptoken = TokenTemp::where('patient_phone',$patient_phone)->first();
                $patient_name=$temptoken->patient_name;
                $patientname = str_replace(' ', '%20', $patient_name);
                $department_counter = Department::where('id','=',$departmentid)->first();
                $counter_name = Counter::where('id','=',$department_counter->counter_id)->first();
                $room_number = (explode('-',$counter_name->name));
                $coutnername= $departmentname = str_replace(' ', '%20',$room_number[0]);
                $patient_gender=$temptoken->patient_gender;
                if($patient_gender == 'M')
                {
                  $gender_prifix = 'Mr';
                }
                else
                {
                  $gender_prifix = 'Ms';
                }

                $queue_token_prefix =  $queue->token_prefix;
                $servicestime = Department::where('id',$department->id)->first();
                $service_time =$servicestime->service_time; 
                $approx_time =  $service_time* $total;
            
       
                $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.'%20for%20the%20Service%20of%20'.$service_description.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';

               // $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";
                $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';

                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $data = curl_exec($ch);
                curl_close($ch);
                event(new \App\Events\TokenIssued());
                event(new \App\Events\TokenCalled());
              }
              }
            }
          }
       }
     }
      }
    }
  }
}
}
      }    

         
 