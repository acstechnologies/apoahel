<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use Session;
use App\Models\Online;

class PPTimeCallController extends Controller
{
  protected $calls;
  public function __construct(CallRepository $calls)
  {
    $this->calls = $calls;
  }

  public function index(Request $request)
  {
      $service_users = Online::where('user_id','!=',NULL)->get();
      
      foreach ($service_users as $service_user)
      {
          $service_user_id[]=$service_user['user_id'];
      }
      
      $users_tokens_pp =NULL;
      $users_token_pp_called_time =NULL;
      $min_user_id =0;
      $department_for_pantry =   Department::where('counters','=',1)->get();
      
      foreach ( $department_for_pantry as  $department_for)
      {
          $department_ids[] = $department_for->id;
      }
      
     

      $queues= Queue::with('department')
      ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',1)->where('defaults','=',1)->get();
      
      foreach ($queues as $queue)
      {
          $patient_phone =$queue->moblie_number;
          $patient_phone_lists =TokenTemp::where('patient_phone',$patient_phone)->where('status',0)
          ->distinct()->get(['mr_no','patient_phone','location_id']);
           foreach ($patient_phone_lists as $patient_phone_list)
           {
               $mr_no = $patient_phone_list->mr_no;
               $patient_phone = $patient_phone_list->patient_phone;
               $location_id=$patient_phone_list->location_id;
               $tokenTime =  TokenTemp::where('mr_no','=',$mr_no)->where('status',0)->where('patient_phone','=',$queue->moblie_number)->where('called','=',0)->distinct()->get(['bill_no','patient_phone','patient_name','location_id','department_id','mr_no','location_id','updated_at']);
               $tokenTime_pp =TokenTemp::where('mr_no','=',$mr_no)->where('status',0)->where('patient_phone','=',$queue->moblie_number)->where('pp',1)->count();
               
               if($tokenTime_pp  != 0 )
               {
                   foreach ($tokenTime as $token_time)
                   {
                       $location_id = $token_time->location_id;
                       $elapsed_minites =0;
                       $last_tokens =Queue::where('mr_no','=', $mr_no)->orderBy('updated_at', 'desc')->first();
                       if($last_tokens != NULL)
                       {
                           $sla = Department::where('id',($last_tokens->department_id))->first();
                           
                           if($last_tokens->service_id =='pantry')
                           {
                               $token_update_time =$last_tokens->updated_at;
                               $token_mrno =$token_time->bill_no;
                               $token_mobile =$token_time->patient_phone;
                               $ldate = date('Y-m-d H:i:s');
                               $to_time = strtotime($ldate);
                               $from_time = strtotime( $token_time->updated_at);
                               $minites = round(abs($to_time - $from_time) / 60,2);
                               $mr_no = $token_time->mr_no;
                               $elapsed_minites =$sla->service_time;
                           }
                           else
                           {
                               $token_update_time =$last_tokens->updated_at;
                               $token_mrno =$token_time->bill_no;
                               $token_mobile =$token_time->patient_phone;
                               $ldate = date('Y-m-d H:i:s');
                               $to_time = strtotime($ldate);
                               $from_time = strtotime($token_update_time);
                               $minites = round(abs($to_time - $from_time) / 60,2);
                               $mr_no = $token_time->mr_no;
                               $elapsed_minites =$sla->service_time;
                           }
                           
                           if($minites > $elapsed_minites)
                           {
                               $temp_tokens = TokenTemp::where('patient_phone','=',$token_mobile)->where('status',0)
                               ->where('priority','!=',0)->where('mr_no','=',$mr_no)->where('called','=',0)->where('pp',null)->get();
                               $queues_number =Queue::where('moblie_number','=',$token_mobile)->where('mr_no','=',$mr_no)
                               ->first();
                               
                                foreach ($temp_tokens as $temp_token)
                                {
                                    $qservice_id =$temp_token->service_id;
                                    $qbill_no  = $temp_token->bill_no;
                                    $department_id = $temp_token->department_id;
                                    $qmobile_number =$temp_token->patient_phone;
                                    $location_id =$temp_token->location_id;
                                    $patient_name = $temp_token->patient_name;
                                    $queues_count = Queue::where('department_id','=',$department_id)->where('mr_no','=',$mr_no)->where('moblie_number','=',$qmobile_number)->where('called','=',0)->count();
                                    if($temp_token->priority != 0 )
                                    {
                                         if($queues_count == 0)
                                         {
                                             $service =  Services::where('service_id','=',$qservice_id)->first();
                                             $sdepartment_id = $service->counter_id;
                                             $department = Department::where('location_id','=',$location_id)
                                             ->where('group_id','=',$sdepartment_id)->first();
                                             $queues_dup = Queue::where('department_id','=',($department->id))
                                             ->where('moblie_number','=',$qmobile_number)->where('mr_no','=',$mr_no)
                                             ->first();
                                            $queues_dup_count = Queue::where('department_id','=',($department->id))->where('moblie_number','=',$qmobile_number)->where('called','=',0)->where('mr_no','=',$mr_no)->count();
                                             if($queues_dup_count == 0)
                                             {
                                                  $queues_count_check= Queue::where('called','=', 0)->where('moblie_number','=', $token_mobile)->where('mr_no','=',$mr_no)->count();
                                                  if($queues_count_check == 0)
                                                  {
                                                      $count_array =array();
                                                      // return $department->id.'-'.$location_id;
                                                      $users =User::where('department_id',$department->id)->where('location_id','=',$location_id)->get();
                                                     
                                                      $min_user_id =0;

                                                      foreach ($users as $user) 
                                                      {
                                                        if(in_array($user->id, $service_user_id))
                                                        {
                                                            $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                                                            $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                                                        }
                                                      }
                                                      
                                                       if(count($count_array) != 0)
                                                       {
                                                           $min_counter_array=min($count_array);
                                                           $min_user_id=$min_counter_array['user_id'];
                                                       }
                                                      if(in_array($min_user_id, $service_user_id))
                                                      {
                                                          $user = User::where('id',$min_user_id)->first();
                        $department = Department::where('id',($user->department_id))->first();
                       
                       
                        $queue = $department->queues()->create([
                                                'token_prefix' => $queues_number->token_prefix,  
                                                'number' =>$queues_number->number,
                                                'called' => 0,
                                                'moblie_number' =>  $token_mobile,
                                                'user_id' =>$min_user_id,
                                                'service_id'=>$qservice_id,
                                                'mr_no'   => $mr_no,
                                                'location_id' =>$location_id,
                                                'patient_name' =>$patient_name
                                                ]);





                          $mobile_number =  TokenTemp::where('bill_no','=',$qbill_no)->first();
                          $patient_phone =$queue->moblie_number; 
                          $dname =$department->name;
                          $queue_token_prefix = $queue->token_prefix;
                          $token_number = $queue->number;
                         
                          $total = $this->calls->getCustomersWaiting($department);
                          $departmentname = str_replace(' ', '%20', $dname);
                          $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                          $patient_name=$temptoken->patient_name;
                          $patientname = str_replace(' ', '%20', $patient_name);
                           $department_counter = Department::where('id','=',$department->id)->first();
                          $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                          
                          $counter_name_array = explode("-",  $counter_name->name);
                          $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                          $floor =str_replace(' ', '%20', $counter_name_array[1]);
                          $servicename =str_replace(' ', '%20', $department_counter->name);
                          $servicestime = Department::where('id',$department->id)->first();
                          $service_time =$servicestime->service_time; 
                          $approx_time =  $service_time* ($total-1);
                          $patient_gender=$temptoken->patient_gender;

                          if($patient_gender == 'M')
                          {
                            $gender_prifix = 'Mr';
                          }
                          else
                          {
                            $gender_prifix = 'Ms';
                          }

                          $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.',%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


  //$url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";
                          $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';


                            $ch = curl_init();
                            $timeout = 5;
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                            $data = curl_exec($ch);
                            curl_close($ch);
                            event(new \App\Events\TokenIssued());
                            event(new \App\Events\TokenCalled());
                                                      }
                                                  }
                                             }
                                         }
                                    }
                                }
                           }
                       }
                   }
               }
           }
      }
  }
}
