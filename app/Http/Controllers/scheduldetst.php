<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use Session;
use App\Models\Online;

class ScheduleQueueController  extends Controller
{
  protected $calls;
  public function __construct(CallRepository $calls)
  {
    $this->calls = $calls;
  }

  public function index(Request $request)
  {

    $service_users = Online::where('user_id','!=',NULL)->get();
    $service_user_id =array();

    foreach ($service_users as $service_user)
    {
      $service_user_id[]=$service_user['user_id'];
    }

    $service_user_id;


    $users_tokens_pp =NULL;
    $users_token_pp_called_time =NULL;
    $min_user_id =0;
   
     return  $tokentemps =  TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->whereIn('priority',[1,2])->where('called','=',0)->distinct()->get(['mr_no','patient_phone']); 
      foreach ($tokentemps  as $tokentemp)
     {
      $patient_phone = $tokentemp->patient_phone;
      $mr_no = $tokentemp->mr_no;

      $queue =  Queue::where('moblie_number',$patient_phone)->where('defaults',1)->first();

      if($queue != nuLL)
      {
        $token_number = $queue->number;
        $token_prefix = $queue->token_prefix;
        $mr_no = $tokentemp->mr_no;

          $queue_count =  Queue::where('moblie_number',$patient_phone)->where('mr_no',$mr_no)->where('called',0)->count();

        if($queue_count != 0)
        {
          $last_tokens =Queue::where('moblie_number','=',$patient_phone)->orderBy('updated_at', 'desc')->first();
          $sla = Department::where('id',($last_tokens->department_id))->first();
          $ldate = date('Y-m-d H:i:s');
          $to_time = strtotime($ldate);
          $token_update_time =$last_tokens->updated_at; 
          $from_time = strtotime($token_update_time);
          $minites = round(abs($to_time - $from_time) / 60,2);
          $elapsed_minites =$sla->service_time;


          if($minites > $elapsed_minites)
          {
           $tokenusers_priority_count = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('status',0)->where('called','=',0)->whereIn('priority',[1,2])->where('mr_no',$mr_no)->where('pp','=',1)->orderBy('priority', 'asc')->count();

    

            if($tokenusers_priority_count == 0)
            {

           return  $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('status',0)->where('called','=',0)->whereIn('priority',[1,2])->where('mr_no',$mr_no)->where('pp','=',NULL)->orderBy('priority', 'asc')->get(); 
                   
              foreach ($tokenusers as $tokenuser)
              {
              $queue_count =  Queue::where('moblie_number',$patient_phone)->where('mr_no',$mr_no)->where('called',0)->count();
              if($queue_count == 0)
                {
                  $service_id =$tokenuser->service_id;           
                  $department_id =$tokenuser->department_id;
                  $patient_phone =$tokenuser->patient_phone;
                  $location_id =$tokenuser->location_id;
                  $patient_name =$tokenuser->patient_name;
                  $count_array =array();
                 
                  foreach($tokenusers as $users_token)
                                  {
                                      $department_ids[] = $users_token->department_id;
                                      $location_id =$users_token->location_id;
                                    
                                  }



                  $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                 return $users;
                  foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }

                   $count_array;

                  if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                       $min_user_id=$min_counter_array['user_id'];

                      if(in_array($min_user_id, $service_user_id))
                      {

                          $user = User::where('id',$min_user_id)->first();
                          $department = Department::where('id',($user->department_id))->first();
                          $queue = $department->queues()->create([
                                                'token_prefix' => $last_tokens->token_prefix,  
                                                'number' =>$last_tokens->number,
                                                'called' => 0,
                                                'moblie_number' => $patient_phone,
                                                'user_id' =>$min_user_id,
                                                'service_id'=> $service_id,
                                                'location_id' =>$location_id,
                                                'mr_no'   => $mr_no,
                                                'patient_name' =>$patient_name
                                                ]);

                         
                        
                          $patient_phone = $patient_phone; 
                          $dname =$department->name;
                          $queue_token_prefix =$last_tokens->token_prefix;
                          $token_number = $last_tokens->number;
                         
                          $total = $this->calls->getCustomersWaiting($department);
                          $departmentname = str_replace(' ', '%20', $dname);
                          $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                          $patient_name=$temptoken->patient_name;
                          $patientname = str_replace(' ', '%20', $patient_name);
                          $department_counter = Department::where('id','=',$department->id)->first();
                          $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                          
                          $counter_name_array = explode("-",  $counter_name->name);
                          $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                          $floor =str_replace(' ', '%20', $counter_name_array[1]);
                          $servicename =str_replace(' ', '%20', $department_counter->name);
                          $servicestime = Department::where('id',$department->id)->first();
                          $service_time =$servicestime->service_time; 
                          $approx_time =  $service_time* ($total-1);
                          $patient_gender=$temptoken->patient_gender;

                          if($patient_gender == 'M')
                          {
                            $gender_prifix = 'Mr';
                          }
                          else
                          {
                            $gender_prifix = 'Ms';
                          }

                          $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.',%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


  

                          $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';


                            $ch = curl_init();
                            $timeout = 5;
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                            $data = curl_exec($ch);
                            curl_close($ch);
                            event(new \App\Events\TokenIssued());
                            event(new \App\Events\TokenCalled());
                            }
                          }



                }
              }
            }
            else
            {
              $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('status',0)->where('called','=',0)->whereIn('priority',[1])->where('mr_no',$mr_no)->where('pp','=',NULL)->orderBy('priority', 'asc')->get();


               
              foreach ($tokenusers as $tokenuser)
              {
              $queue_count =  Queue::where('moblie_number',$patient_phone)->where('mr_no',$mr_no)->where('called',0)->count();
              if($queue_count == 0)
                {
                  $service_id =$tokenuser->service_id;           
                  $department_id =$tokenuser->department_id;
                  $patient_phone =$tokenuser->patient_phone;
                  $location_id =$tokenuser->location_id;
                  $patient_name =$tokenuser->patient_name;
                  $count_array =array();
                  $users =User::where('department_id',$department_id)->get();

                  foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }

                  $count_array;
                  if(count($count_array) != 0)
                    {
                      $min_counter_array=min($count_array);
                      $min_user_id=$min_counter_array['user_id'];

                      if(in_array($min_user_id, $service_user_id))
                      {

                          $user = User::where('id',$min_user_id)->first();
                          $department = Department::where('id',($user->department_id))->first();
                          $queue = $department->queues()->create([
                                                'token_prefix' => $last_tokens->token_prefix,  
                                                'number' =>$last_tokens->number,
                                                'called' => 0,
                                                'moblie_number' => $patient_phone,
                                                'user_id' =>$min_user_id,
                                                'service_id'=> $service_id,
                                                'location_id' =>$location_id,
                                                'mr_no'   => $mr_no,
                                                'patient_name' =>$patient_name
                                                ]);

                         
                          $patient_phone = $patient_phone; 
                          $dname =$department->name;
                          $queue_token_prefix =$last_tokens->token_prefix;
                          $token_number = $last_tokens->number;
                         
                          $total = $this->calls->getCustomersWaiting($department);
                          $departmentname = str_replace(' ', '%20', $dname);
                          $temptoken = TokenTemp::where('patient_phone',$patient_phone)->where('mr_no','=',$mr_no)->first();
                          $patient_name=$temptoken->patient_name;
                          $patientname = str_replace(' ', '%20', $patient_name);
                          $department_counter = Department::where('id','=',$department->id)->first();
                          $counter_name = Counter::where('id','=',$department_counter->counter_id)->first(); 
                          
                          $counter_name_array = explode("-",  $counter_name->name);
                          $coutnername =str_replace(' ', '%20', $counter_name_array[0]);
                          $floor =str_replace(' ', '%20', $counter_name_array[1]);
                          $servicename =str_replace(' ', '%20', $department_counter->name);
                          $servicestime = Department::where('id',$department->id)->first();
                          $service_time =$servicestime->service_time; 
                          $approx_time =  $service_time* ($total-1);
                          $patient_gender=$temptoken->patient_gender;

                          if($patient_gender == 'M')
                          {
                            $gender_prifix = 'Mr';
                          }
                          else
                          {
                            $gender_prifix = 'Ms';
                          }

                          $message ='Dear%20'.$gender_prifix.'%20'.$patientname.',%20Please%20wait%20at%20'.$coutnername.',%20'.$floor.'%20for%20the%20service%20of%20'.$servicename.'.Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Pls%20be%20seated%20until%20your%20token%20number%20is%20called.';


  

                          $url = 'http://www.smsjust.com/sms/user/urlsms.php?username=apollohealth&pass=dM76$Bc-&senderid=APOLLO&dest_mobileno='.$patient_phone.'&msgtype=UNI&message='.$message.'&response=Y';


                            $ch = curl_init();
                            $timeout = 5;
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                            $data = curl_exec($ch);
                            curl_close($ch);
                            event(new \App\Events\TokenIssued());
                            event(new \App\Events\TokenCalled());
                            }
                          }
                

























                }
              }










            }








          }
        }
      }
    }
  }
}   

              