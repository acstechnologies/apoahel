<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DashbordRepository;
use App\Models\Department;
use App\Models\User;
use App\Models\Location;
use Carbon\Carbon;
use App\Models\Queue_reports;
use DB;
use Mail;


class QueueReportsController extends Controller
{
    protected $setting;
    public function __construct(DashbordRepository $setting)
    {
        $this->setting = $setting;
    }

    public function index()
    {
       
        $locations = Location::wherein('id',[3,17,8,7,9,18,21,22,23,24])->get();
        // $today_date= Carbon::now()->format('d-m-Y H:m:s');

        $today_date = Carbon::yesterday()->format('Y-m-d'); 
      
     

      
        foreach($locations as $location ){

              $id =$location->id;
               $tokens = $this->setting->locationissedTokenReport($id,$today_date);
               $today_queue =$this->setting->locationGetTodayQueueReports($id,$today_date);
               $served=$this->setting->loctionGetTodayServedReports($id,$today_date);
               $waiting =$this->setting->loctionGetwaitingReports($id,$today_date);
               $missed = $this->setting->locationGetTodayMissedReports($id,$today_date);
               $overtime =$this->setting->locationGetTodayOverTimeReports($id,$today_date);

            
            $reports[] =array('today_date' => $today_date,
                              'location_name' => $location->name,
                              'tokens' =>  $tokens,
                              'today_queue' =>$today_queue,
                              'served' => $served,
                              'waiting' =>$waiting,
                              'missed' => $missed,
                              'overtime' =>$overtime,


                              );


                            
                            }
                            //}

                            $today['day']=Carbon::yesterday()->format('d-m-Y');
                          //  }

        return view('mail', [
            'reports' => $reports,'today'=>$today
           
        ]);
       

 

       
       

        
    }

    public function store(Request $request,Queue_reports $qmsr)
    {
          $date= Carbon::now()->format('Y-m-d').' 00:00:00';
        
           $date= Carbon::now()->format('d-m-Y');
        
           //  $curent_date = date('01-10-2019');
        
              $today_date = date('2019-12-31');
        
      //   Carbon::now()->format('Y-m-d')
       //  $date = Carbon::createFromFormat('d-m-Y','15-11-2019');
        
       

      //  $newformat = date('Y-m-d');
            
            $locations = Location::wherein('id',[3,17,8,7,9,18,21,22,23,24])->get();
       
      
       

        foreach($locations as $location ){

             $id =$location->id;
               $tokens = $this->setting->locationissedTokenReport($id,$today_date);
               $today_queue =$this->setting->locationGetTodayQueueReports($id,$today_date);
               $served=$this->setting->loctionGetTodayServedReports($id,$today_date);
               $waiting =$this->setting->loctionGetwaitingReports($id,$today_date);
               $missed = $this->setting->locationGetTodayMissedReports($id,$today_date);
               $overtime =$this->setting->locationGetTodayOverTimeReports($id,$today_date);


             
            
            
            DB::table('queue_reports')->insert(['date'   =>$today_date,
                              'name' => $location->name,
                              'tokens' =>  $tokens,
                              'queue' =>$today_queue,
                              'served' => $served,
                              'awaiting' =>$waiting,
                              'missed' => $missed,
                              'over_time' =>$overtime]);



        }
        
        
       // return $reports;
    }
    
    public function email(Queue_reports $qmsr,Request $request){
        
      $today_date= Carbon::now()->format('d-m-Y'); 
      $qmsr = Queue_reports::all();
        
        
        $list[] =array('Date','Clinic','Tokens','Queue','Served','Awaiting','Missed','Overtime');
        
        foreach($qmsr as $qms)
        {
           $date = $date=date_create($qms->date);
           $current_date = date_format($date,"d-m-Y");
          
            $list[] =array(
                           $current_date,
                           $qms->name,
                           $qms->tokens,
                           $qms->queue,
                           $qms->served,
                           $qms->awaiting,
                           $qms->missed,
                           $qms->over_time
            );
            
            
        }
    
         
        
      $path =  storage_path().'/queue_reports.csv';

$file = fopen($path,"w");

foreach ($list as $line) {


   $line;
  fputcsv($file, $line);
}

fclose($file);
   
    
     /* $data = array('name'=>"QMS");
   
      \Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('KVK2LEAD@gmail.com ', 'IQMS Reports')->subject
            ('Laravel Basic Testing Mail');
         $message->from('noreply@apolloclinics.info','noreply');
      });  
      echo "Email Sent with attachment. Check your inbox.";
        
         */
    
   /*     
    $data = array('name'=>"Dear Concern,");
    \Mail::send('mail', $data, function($message) {
        
         $message->to('pamidi.ramakanth@gmail.com', 'QMS Reports')->subject
            ('AHLL-IQMS-MIS');
        
         $message->attach(storage_path().'\queue_reports.csv');
        // $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('noreply@apolloclinics.info','IQMS');
      });
      echo "Email Sent with attachment. Check your inbox.";
       */
        
        
        
        
        
        
        
        
        
        
        
    
    }

    
    public function send($view, array $data = [], $callback = null)
    {
        
    }
    
}
