<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use App\Repositories\CounterRepository;
use App\Models\Counter;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Location;


class LocationController extends Controller
{
    protected $locations;

    public function __construct(Location $locations)
    {
        $this->locations = $locations;
    }

    public function index()
    {

      //  $this->authorize('access', Location::class);

       $locations =  Location::where('id','>',0)->get();

        return view('user.locations.index', [
            'locations' =>$locations,
        ]);
    }

    public function create()
    {
       // dd($request->all());//$this->authorize('access', Location::class);

        return view('user.locations.create',['locations' => Location::all()]);
    }

    public function store(Request $request, Location $location)
    {
        //$this->authorize('access', Location::class);

        $this->validate($request, [
            'name' => 'required',
            'code' =>'required'
        ]);

      //  Department::create($request->all());

        $location->name = $request->name;
        $location->code = $request->code;
        $location->save();



        flash()->success('Location created');
        return redirect()->route('locations.index');
    }

    public function edit(Request $request, Location $location)
    {
      $locations = Location::all();
        return view('user.locations.edit', [
            'locations' => $locations,
            'location'  => $location
        ]);
    }

    public function update(Request $request, Location $location)
    {
        //$this->authorize('access', Location::class);

        $this->validate($request, [
            'name' => 'required',
            'code' =>'required',
        ]);

        $location->name = $request->name;
        /*$department->location = $request->location_id;
        $department->counter = $request->counter;*/
        $location->code = $request->code;
        $location->save();

        flash()->success('Location updated');
        return redirect()->route('locations.index');
    }

    public function destroy(Request $request, Location $location)
    {
        //$this->authorize('access', Location::class);

        $location->delete();

        flash()->success('Location deleted');
        return redirect()->route('locations.index');
    }
}
