<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AddToQueueRepository;
use App\Models\Setting;
use App\Models\Department;
use App\Models\User;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use App\Models\Location;
use App\Models\Online;





class AddToNewQueueController extends Controller
{
    protected $add_to_queues;

    public function __construct(AddToQueueRepository $add_to_queues)
    {
        $this->add_to_queues = $add_to_queues;
    }

    public function index(Request $request)
    {
            $id = Auth::user();
            $request->session()->put('user_id', $id );
            $request->session()->put('location_id',($id->location_id));
            $value = $request->session()->get('user_id');
            $session = $request->session()->get('location_id');
            $settings = Setting::first();
            \App::setLocale($settings->language->code);
            return view('addtoqueue.index_new', [
            'settings' => $settings,
         
           
        ]);
    }
    
    public function billcounter(Request $request, $id) 
    {
        
            $id = Auth::user();

        
            $request->session()->put('user_id', $id );
           // $request->session()->put('location_id',($id->location_id));
           // $value = $request->session()->get('user_id');
           // $session = $request->session()->get('location_id');
            $settings = Setting::first();
            \App::setLocale($settings->language->code);
            return view('addtoqueue.index_new', [
            'settings' => $settings,
            
           
        ]);
        
    }
    
    
    
    
    

/*    public function postDept(Request $request)
    {

       
        $department = Department::findOrFail($request->department);

        $last_token = $this->add_to_queues->getLastToken($department);

        if($last_token) {
            $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
            ]);
        } else {
            $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
            ]);
        }

        $total = $this->add_to_queues->getCustomersWaiting($department);

      //  event(new \App\Events\TokenIssued());

        $request->session()->flash('department_name', $department->name);
        $request->session()->flash('number', ($queue->token_prefix!='')?$queue->token_prefix.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);

        flash()->success('Token Added');
        return redirect()->route('add_to_queue');
      
    }*/
        public function postDept(Request $request)
    {
        $uhid = $request->uhid;
        $hid =$request->hid;
        if($hid == 6){
          //return $request->all();

            $queue =array(
            'mr_no' => $request['uhid'],
            'called'=> 0
             );
          $message = "http://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?appid=xxxx&userId=xxxx&pass=xxxx&contenttype=1&from=xxxx&to=7032031854&text=this is a demo message with url https://abcd.com/accountdetails?account=123456.&alert=1&selfid=true&intflag=false&&s=1&f=1";
        Queue::insert($queue);
        flash()->success('Token Added');
        return redirect()->route('add_to_queue'); 
        }


        if($hid == 1){
        $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_PORT => "9097",
      CURLOPT_URL => "http://115.112.108.122:9097/api/Service/Package?UHID=".$uhid."&RegionID=2",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "{\r\n\t\"authKey\": \"F5A18170-87A4-4BCC-BD2D-522649FA90D0\"\r\n}\r\n",
      CURLOPT_HTTPHEADER => array(
        "authkey: F5A18170-87A4-4BCC-BD2D-522649FA90D0",
        "cache-control: no-cache",
        "postman-token: c22171d6-8885-c8d6-71a1-845ecc9a2856"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;
       $manage = json_decode($response, true);
    
     /*  $Uhid = $manage['Patient']['Uhid'];
       $patient_name =$manage['Patient']['patient_name'];
       $patient_phone =9959576026;//$manage['Patient']['patient_phone'];
       $patient_email =$manage['Patient']['patient_email'];
       $patient_gender =$manage['Patient']['patient_gender'];
       $appointment_datetime =$manage['Patient']['appointment_datetime'];
       $doctor_id =$manage['Patient']['doctor_id'];
       $doctor_speciality =$manage['Patient']['doctor_speciality'];
       $hospital_id =$manage['Patient']['hospital_id'];
       $appointment_type =$manage['Patient']['appointment_type'];
       $appointment_status =$manage['Patient']['appointment_status'];*/
        
/*       $doctor =  Doctors::where('doctor_id',$doctor_id)->first();   
       $user = User::where('doctor_id',$doctor->id)->first();
       $user_id = $user->id;
       $department =Department::where('id',$user->department_id)->first(); 
      
       $last_token = $this->add_to_queues->getLastToken($department);
            
         $token_prefix ='CON';
            */
      $Uhid = $manage['Patient']['UHID'];
       $patient_name =$manage['Patient']['FirstName'];
       $patient_phone =$manage['Patient']['MobileNumber'];
       $AHCPackage =$manage['Patient']['AHCPackage']['PackageID'];
       $Gender =$manage['Patient']['Gender'];
       $PackageDate =$manage['Patient']['AHCPackage']['PackageDate'];
    
       $ahcpackage =array();
     
    
       $packages = Package::where('package_id',$AHCPackage)->get();
        
        foreach($packages as $package)
        {
             $activity_description= $package->service_name;
             $service_id = $package->service_id;
            
            $ahcpackage[] =array(
            'package_id' => $AHCPackage,
            'patient_phone' =>$patient_phone,
            'activity_description' => $activity_description,
            'service_id' => $service_id,
            'mr_no' =>$Uhid,
            'patient_name' => $patient_name,
            'department_id' =>$package->department_id,
                'priority' =>$package->priority,
            'gender' => $Gender,
            'called'=> 0
                );
          }
    
        TokenTemp::insert($ahcpackage); 
     /*   if($last_token)
        {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$user_id,
                'moblie_number'=>$patient_phone,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'service_id'=>0,
                'mr_no'   =>  0 
                ]);
          }else
          {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user_id,
                'moblie_number' =>$patient_phone,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'service_id'=>0,
                'mr_no'   =>  0
                ]);
           }*/


/*            $patient_phone=$patient_phone;
            $dname =$department->name;
            $token_number = $queue->number;
            $queue_token_prefix =  $queue->token_prefix;
            $total = $this->add_to_queues->getCustomersWaiting($department);
           $servicestime = Department::where('id',$department->id)->first();
           $service_time =$servicestime->service_time; 
           $approx_time =  $service_time* ($total-1);
            
           $message = 'Namaste,%20Welcome%20to%20Apollo%20Hospital.%20Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Please%20be%20seated%20until%20your%20token%20number%20is%20called.';*/

// $message = 'Namaste';

/*
 $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";*/


 



         /*   $url="http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";*/

          /*    $url = "http://sms.bulksmsind.in/sendSMS?username=acstechnologies&message=$message&sendername=ACSQUE&smstype=TRANS&numbers=$patient_phone&apikey=de51c133-a213-4951-b9d9-887e66c90948";
*/
/*            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            
            $data = curl_exec($ch);
            curl_close($ch);
            

            event(new \App\Events\TokenIssued());
            $request->session()->flash('department_name', $department->name);
            $request->session()->flash('number', ($queue->token_prefix!='')?$queue->token_prefix.'-'.$queue->number:$queue->number);
            $request->session()->flash('total', $total);
            flash()->success('Token Added');
            return redirect()->route('add_to_queue');*/
            
            
            
            
            
        }
    }
            
    if($hid == 2){
        
        
        $uhid = $request->uhid;


    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_PORT => "9097",
      CURLOPT_URL => "http://115.112.108.122:9097/api/Service/Package?UHID=".$uhid."&RegionID=2",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "{\r\n\t\"authKey\": \"F5A18170-87A4-4BCC-BD2D-522649FA90D0\"\r\n}\r\n",
      CURLOPT_HTTPHEADER => array(
        "authkey: F5A18170-87A4-4BCC-BD2D-522649FA90D0",
        "cache-control: no-cache",
        "postman-token: c22171d6-8885-c8d6-71a1-845ecc9a2856"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;
       $manage = json_decode($response, true);
  
    
       $Uhid = $manage['Patient']['UHID'];
       $patient_name =$manage['Patient']['FirstName'];
       $patient_phone =$manage['Patient']['MobileNumber'];
       $AHCPackage =$manage['Patient']['AHCPackage']['PackageID'];
       $Gender =$manage['Patient']['Gender'];
       $PackageDate =$manage['Patient']['AHCPackage']['PackageDate'];
    
       $ahcpackage =array();
     
    
       $packages = Package::where('package_id',$AHCPackage)->get();
        
        foreach($packages as $package)
        {
             $activity_description= $package->service_name;
             $service_id = $package->service_id;
            
            $ahcpackage[] =array(
            'package_id' => $AHCPackage,
            'patient_phone' =>$patient_phone,
            'activity_description' => $activity_description,
            'service_id' => $service_id,
            'mr_no' =>$Uhid,
            'patient_name' => $patient_name,
            'department_id' =>$package->department_id,
                'priority' =>$package->priority,
            'gender' => $Gender,
            'called'=> 0
                );
          }
    
        TokenTemp::insert($ahcpackage); 

    /*
        $token_temp = TokenTemp::where('mr_no','=',$Uhid)->where('priority',1)->first();
        $department_id = $token_temp->department_id;
        $department = Department::where('id',$department_id)->first();
        $user =User::where('department_id',$department_id)->first();
        
    
    
         $last_token = $this->add_to_queues->getLastToken($department);
            
         $token_prefix ='HC';
            
            
        if($last_token)
        {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$user->id,
                'moblie_number' =>$token_temp->patient_phone,
                'patient_name' =>$token_temp->patient_name,
                'gender' =>$token_temp->patient_gender,
                'service_id'=>0,
                'gender' =>$token_temp->gender,
                'mr_no'   =>$Uhid 
                ]);
          }else
          {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user->id,
                'moblie_number' =>$token_temp->patient_phone,
                'patient_name' =>$token_temp->patient_name,
                'gender' =>$token_temp->patient_gender,
                'service_id'=>0,
                'mr_no'   =>$Uhid,
                'gender' =>$token_temp->gender,
                ]);
           }

            $patient_phone=$patient_phone;
            //$elem=$elem;
            $dname =$department->name;
            $token_number = $queue->number;
            $queue_token_prefix =  $queue->token_prefix;
            $total = $this->add_to_queues->getCustomersWaiting($department);
           /* $message=  '%20service%20'.$dname.'%20Your%20Token%20Number%20'.$token_number.'%20Please%20wait%20for%20your%20turn%20Total%20customer%20waiting%20'.$total;*/
/*           $servicestime = Department::where('id',$department->id)->first();
           $service_time =$servicestime->service_time; 
           $approx_time =  $service_time* ($total-1);*/
            
        /*   $message = 'Namaste,%20Welcome%20to%20Apollo%20Hospital.%20Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Please%20be%20seated%20until%20your%20token%20number%20is%20called.';

    


 $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";


 */


/*
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            
            $data = curl_exec($ch);
            curl_close($ch);
            

            event(new \App\Events\TokenIssued());
            $request->session()->flash('department_name', $department->name);
            $request->session()->flash('number', ($queue->token_prefix!='')?$queue->token_prefix.'-'.$queue->number:$queue->number);
            $request->session()->flash('total', $total);
            flash()->success('Token Added');
            return redirect()->route('add_to_queue');*/
            

    
    

}
          
      
          
      }
        
   
    }
    public function postDept1(Request $request,Location $location, Department $department,$counter,$uhid,$qtype,$lid)
  
    {
          $service_users = Online::where('user_id','!=',NULL)->get();
            foreach ($service_users as $service_user)
            {
                $service_user_id[]=$service_user['user_id'];
            }
          $uhid ='AC01.0002227078';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_PORT => "9097",
          CURLOPT_URL => "http://115.112.108.122:9097/api/Service/Package?UHID=".$uhid."&RegionID=2",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{\r\n\t\"authKey\": \"F5A18170-87A4-4BCC-BD2D-522649FA90D0\"\r\n}\r\n",
          CURLOPT_HTTPHEADER => array(
            "authkey: F5A18170-87A4-4BCC-BD2D-522649FA90D0",
            "cache-control: no-cache",
            "postman-token: 15c315cb-67b9-3c9c-2369-8dbacd047e6d"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
         $responsedata = json_decode($response, true);
         $responsedata['Patient']['AHCPackage'];
         $Uhid = $responsedata['Patient']['UHID'];
         $patient_name =$responsedata['Patient']['FirstName'];
         $patient_phone =$responsedata['Patient']['patient_phone'];
         $patient_gender =$responsedata['Patient']['Gender'];
         $appointment_datetime =$responsedata['Patient']['CreatedDate'];
         $PackageID =$responsedata['Patient']['AHCPackage']['PackageID'];
  
         //$doctor =  Doctors::where('doctor_id',$doctor_id)->first();   
        //$user = User::where('doctor_id',$doctor->id)->first();
       // $user_id = $user->id;
        //$department =Department::where('id',$user->department_id)->first(); 
      
        $last_token = $this->add_to_queues->getLastToken($department);

          if($qtype == 1){

                $token_prefix = 'INV';

            }elseif ($qtype == 2) {
                $token_prefix = 'OTH';
               
            }elseif ($qtype == 3) {
                $token_prefix = 'CON';
             
            }elseif ($qtype == 4) {
                 $token_prefix = 'RHC';               
            }elseif($qtype == 5) {
                 $token_prefix = 'CHC';
               
            }
            
            $location_id = $lid;
            $id = $lid ;        

            $location = Location::where('code','=',$location_id)->first();
            $location_id =$location->id;
            
            $id1 =$counter;
        
        
            $departments = Department::where('name',$counter)->where('location_id',$location_id)->first();
            //return $departmentsid;
               $users = User::where('department_id','=',($departments->id))->get();

              $count_array =array();
              $counts=0;
              $min_user_id =0;

            foreach ($users as $user)
            {
                if (in_array($user->id,$service_user_id))
                {
                    $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                    $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                }
            }

            if($count_array == NULL)
            {

                $message ='FOE not Logged in';
                 $request->session()->flash('messages', $message);

                flash()->success('FOE not Logged in');
             return redirect()->route('add_to_queue_counter', ['id' => $id,'id1' => $id1]);

            }           
            $min_counter_array=min($count_array);
            $min_user_id=$min_counter_array['user_id'];

            $last_token = $this->add_to_queues->getLastToken($departments);
            $users = User::where('department_id','=', ($departments->id))->first();
        // return $user;
            
        if($last_token)
        {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$min_user_id,
                'moblie_number'=>$patient_phone,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'location_id' => $location_id,
                'service_id'=>0,
                'mr_no'   =>$uhid 
                ]);
          }else
          {
            $queue = $department->queues()->create([
                'token_prefix' => $token_prefix,
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$min_user_id,
                'moblie_number' =>$patient_phone,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'location_id' => $location_id,
                'service_id'=>0,
                'mr_no'   =>$uhid
                ]);
           }

            $patient_phone=$patient_phone;
            //$elem=$elem;
            $dname =$department->name;
            $token_number = $queue->number;
            $queue_token_prefix =  $queue->token_prefix;
            $total = $this->add_to_queues->getCustomersWaiting($department);
             $servicestime = Department::where('id',$departments->id)->first();
             $service_time =$servicestime->service_time; 
             $approx_time =  $service_time* ($total-1);
            
           $message = 'Namaste,%20Welcome%20to%20Apollo%20Hospital.%20Your%20token%20No.%20'.$queue_token_prefix.'-'.$token_number.'%20is%20'.$total.'%20in%20the%20queue%20with%20approx%20wait%20time%20of%20'. $approx_time.'%20mins.%20Please%20be%20seated%20until%20your%20token%20number%20is%20called.';


            $url = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-acstech&password=acs1234&type=0&dlr=1&destination=$patient_phone&source=ACSQUE&message=$message";
          $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            
            $data = curl_exec($ch);
            curl_close($ch);
            

            event(new \App\Events\TokenIssued());
            $request->session()->flash('department_name', $department->name);
            $request->session()->flash('number', ($queue->token_prefix!='')?$queue->token_prefix.'-'.$queue->number:$queue->number);
            $request->session()->flash('total', $total);
            flash()->success('Token Added');
            return redirect()->route('add_to_queue');

        }
        

   }
}


