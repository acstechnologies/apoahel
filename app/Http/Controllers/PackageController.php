<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use App\Repositories\PackageRepository;
use App\Models\Package;
use App\Repositories\CounterRepository;
use App\Models\Counter;

class PackageController extends Controller
{
   protected $departments;

    public function __construct(DepartmentRepository $departments,CounterRepository $counters)
    {
        $this->departments = $departments;
        $this->counters = $counters;
    }

    public function index()
    {
        $this->authorize('access', Department::class);

         $departments =  Department::where('package_id','=',1)->get();
        $this->departments->getAll();
        return view('user.packages.index', [
            'departments' =>$departments,
           'counters' =>$this->counters->getAll()
        ]);
    }

    public function create()
    {
        $this->authorize('access', Department::class);

        return view('user.packages.create',[ 'counters' =>$this->counters->getAll()]);
    }

    public function store(Request $request, Department $department)
    
    {

        $department_array =array();

        $this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'counter' =>'required',
            'start' =>'required|numeric'
        ]);
 
        $counters = implode(",",$request->counter);  

        foreach($request->counter  as $count){
            
               $departmentes= Department::where('counter_id','=',$count)->first();
               $department_array[]= $departmentes->id;
            }

        $department_string = implode(',',$department_array);

        $department->name = $request->name;
        $department->letter = $request->letter;
        $department->start = $request->start;
        $department->package_id = 1;
        $department->department_ids=$department_string;
        $department->counter_id = $counters;
        $department->save();

        flash()->success('Department created');
        return redirect()->route('departments.index');
    }



    public function edit(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        return view('user.departments.edit', [
            'department' => $department,
        ]);
    }

    public function update(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'start' =>'required|numeric',
        ]);

        $department->name = $request->name;
        $department->letter = $request->letter;
        $department->start = $request->start;
        $department->save();


        flash()->success('Department updated');
        return redirect()->route('departments.index');
    }

    public function destroy(Request $request, Department $department)
    {
        $this->authorize('access', Department::class);

        $department->delete();

        flash()->success('Department deleted');
        return redirect()->route('departments.index');
    }
}
