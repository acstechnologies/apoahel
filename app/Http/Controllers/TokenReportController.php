<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use Auth;
use Response;

class TokenReportController extends Controller
{
    protected $calls;

    public function __construct(CallRepository $calls)
    {
        $this->calls = $calls;
    }

    public function index(Request $request)


    {
     $department= Department::whereIn('id', array(Auth::user()->department_id,1))->first();
           
    $user_id = Auth::user()->id;



          if($user_id == 1){
          $queuess = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   
                    ->where('called','=',0)
                    ->get();  
                }else{

         $queuess = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('user_id','=',$user_id)
                    ->where('called','=',0)
                    ->get();  
               }

           // $this->calls->getQue();


              //  return $queuess;
              //  $department= Department::whereIn('id', array(Auth::user()->department_id,1))->first();
              //return $counters= Counter::where('id',$department->counter_id)->get();


        $department = array();
        event(new \App\Events\TokenIssued());

        $user_id = Auth::user()->id;
        $role = Auth::user()->role;

        if($role == 'A'){

         $department =   Department::all();
        }else{

          $department= Department::whereIn('id', array(Auth::user()->department_id,1))->first();
                     }

         if($role == 'A'){

         $counters =   Counter::all();
        }else{

          $department= Department::whereIn('id', array(Auth::user()->department_id))->first();
          $counters= Counter::where('id',$department->counter_id)->get();
             }
                
        return view('user.reports.token_reports.index', [
            'users' => $this->calls->getUsers(),
            'counters' => $counters,
            'departments' => $department,// $this->calls->getDepartments(),
            'departments_menu' => Department::where('package_id','=',0)->get(),
            'packages' =>Department::where('package_id','=',1)->get(),
            'queuess' =>  $queuess,
          
        ]);
    }

    public function newCall(Request $request)
    {
        $this->validate($request, [
            'user' => 'bail|required|exists:users,id',
            'counter' => 'bail|required|exists:counters,id',
            'department' => 'bail|required|exists:departments,id',
        ]);


        $user = User::findOrFail($request->user);
        $counter = Counter::findOrFail($request->counter);
        $department = Department::findOrFail($request->department);

      //    $queue = $this->calls->getNextToken($department);
       $queue =  $department->queues()
                    ->where('called', 0)
                    ->where('user_id',$user->id)
                    ->where('created_at', '>', Carbon::now()->format('Y-m-d 00:00:00'))
                    ->first();

//return $user.$counter.$department.$queue;

        if($queue==null) {
            flash()->warning('No Token for this department');
            return redirect()->route('calls');
        }

        

        $call = $queue->call()->create([
           
            'department_id' => $department->id,
            'counter_id' => $counter->id,
            'user_id' => $user->id,
            'number' => $queue->number,
            'called_date' => Carbon::now()->format('Y-m-d'),
        ]);
         $queue->called = 1;
         $queue->save();

       $package = Department::where('id','=',($department->id))->first();

    

           if($package->package_id == 1){

            $department_id =Department::where('id','=',$department->id)->first();
            $departmentid= $department_id->department_ids;
            $departmentids = (explode(' ',$departmentid));
            count($departmentids);
            $queues = Queue::where('department_id','=',($department->id))->where('number','=',($queue->number))->get();

            $department->id;
            $queue->number;

            if(count($queues) <=  count($departmentids)){

            //$user_ids = User::where('department_id',$departmentids[0])->first();

            foreach ($departmentids as $departmentids_s) {

             // $user = User::('department_id')
            $users = User::where('department_id','=',$departmentids_s)->first();

            $queuees = Queue::where('department_id','=',($department->id))
                             ->where('number','=',($queue->number))
                             ->where('user_id','=',$users->id)
                              ->get(); 
            if(count($queuees) == 0 ){

               $users = User::where('department_id','=',$departmentids_s)->first();


                 $queue = $department->queues()->create([
                 'department_id' => $department->id,
                 'called'  => 0,
                 'number'  => $queue->number,
                 'user_id' => $users->id
          ]);


            $request->session()->flash('department', $department->id);
            $request->session()->flash('counter', $counter->id);

            event(new \App\Events\TokenIssued());
            event(new \App\Events\TokenCalled());

            flash()->success('Token Called');
            return redirect()->route('calls');
            }
          }

            }




/*
        $queue = $department->queues()->create([
            'department_id' => $department->id,
            'called'  => 0,
            'number'  => $queue->number,
            'user_id' =>$user->id
          ]);

*/

   }
        $request->session()->flash('department', $department->id);
        $request->session()->flash('counter', $counter->id);

        event(new \App\Events\TokenIssued());
        event(new \App\Events\TokenCalled());

        flash()->success('Token Called');
        return redirect()->route('calls');
    }

    public function postDept(Request $request, Department $department)
    {
      
        $last_token = $this->calls->getLastToken($department);
        $userid=0; 
       
        $user_ids = User::where('department_id',$department->id)->first();

         if(count($user_ids) >= 1){
          $userid= $user_ids->id; }else{

        $min_user=0;
        $counter_array = array();
        $department_id =Department::where('id','=',$department->id)->first();
        $departmentid= $department_id->department_ids;
        $departmentids = (explode(' ',$departmentid));
            
            
        foreach ($departmentids as  $departments_ids) {
              $user =User::where('department_id',$departments_ids)->first();
            
                  $que = Queue::where('user_id','=',$user->id)->where('called','=',0)->get();
                 
                  $counter_array[]= array ('count' => count($que),'counter' =>$user->id);
                 
                }
                $counter_array;
                $min_counter_array=  min($counter_array);
                $min_user=  $min_counter_array['counter'];
        }

        if($department->package_id  != 1){
        if($last_token) {
        $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$userid]);
        } else {

        $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$userid
                
            ]);
          }
        }else{
    

           /* $counter_array = array();
            $department_id =Department::where('id','=',$department->id)->first();
            $departmentid= $department_id->department_ids;
            $counter_ids = $department_id->counter_id;
            $departmentids = (explode(',',$departmentid));
            $counter_ids=(explode(',',$counter_ids));
            $user_ids = User::where('department_id',$departmentids[0])->first();

            if($last_token) {
                 $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$user_ids->id]);
        } else {

                 $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$user_ids->id
                
            ]);
        }*/
           /* $counter_array = array();
            $department_id =Department::where('id','=',$department->id)->first();
            $departmentid= $department_id->department_ids;
            $departmentids = (explode(',',$departmentid));
            
            
            foreach ($departmentids as  $departments_ids) {
                    $user =User::where('department_id',$departments_ids)->first();
                    $que = Queue::where('user_id','=',$user->user_id)->where('called','=',0)->get();
                    $counter_array[]= array ('count'   => count($que),
                                          'counter' =>$user->id);
           }
               $counter_array;
               $min_counter_array=  min($counter_array);
               $min_user=  $min_counter_array['counter'];*/
           
            if($last_token) {
                $queue = $department->queues()->create([
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
                'user_id' =>$min_user]);
           
              } else {

            $queue = $department->queues()->create([
                'number' => $department->start,
                'called' => 0,
                'user_id'  =>$min_user
                
            ]);
        }



    }


        $total = $this->calls->getCustomersWaiting($department);
        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $department->name);
        $request->session()->flash('number', ($department->letter!='')?$department->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);

       
        flash()->success('Token Added');
        return redirect()->route('calls');
    }


     public function postPack(Request $request, Package $package)
      {

     $department_id=Department::where('package_id',$package->id)->first();
     $last_token = $this->calls->getLastPackToken($package);
      // Department $department
      if($last_token) {
            $queue = $package->queues()->create([
                'department_id'=> $department_id->id,
                'number' => ((int)$last_token->number)+1,
                'called' => 0,
            ]);
        } else {
            $queue = $package->queues()->create([
                'department_id'=> $department_id->id,
                'number' => $package->start,
                'called' => 0,
            ]);
        }

        $total = $this->calls->getCustomersWaitingp($package);
        event(new \App\Events\TokenIssued());
        $request->session()->flash('department_name', $package->name);
        $request->session()->flash('number', ($package->letter!='')?$package->letter.'-'.$queue->number:$queue->number);
        $request->session()->flash('total', $total);
        flash()->success('Token Added');
        return redirect()->route('calls');
    }


    public function recall(Request $request)
    {
        $call = Call::find($request->call_id);
        $new_call = $call->replicate();
        $new_call->save();
        $call->delete();
        event(new \App\Events\TokenCalled());
        flash()->success('Token Called');
        return $new_call->toJson();
    }


    public function getDeparts()
  {
      $user_id = Auth::user()->id;



          if($user_id == 1){
     return      $queuess1 = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   
                    ->where('called','=',0)
                    ->get();  
                }else{

    return    $queuess1 = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('user_id','=',$user_id)
                    ->where('called','=',0)
                    ->get();  
               }

 

  }







}
