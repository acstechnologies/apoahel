<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Response;
use App\Repositories\AddToQueueRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use Session;
use App\Models\Online;

class ThirdQueueController  extends Controller
{
  protected $calls;
  public function __construct(AddToQueueRepository $add_to_queues)
  {
   
    $this->add_to_queues = $add_to_queues;
  }

  public function index(Request $request)
  {

    $service_users = Online::where('user_id','!=',NULL)->get();
    $service_user_id =array();

    foreach ($service_users as $service_user)
    {
      $service_user_id[]=$service_user['user_id'];
    }
    $users_tokens_pp =NULL;
    $users_token_pp_called_time =NULL;
    
 
     $tokentemps =  Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->whereIn('priority',[4,5,6,7])->where('called','=',1)->distinct()->get();

      foreach ($tokentemps  as $tokentemp)
      {
            $patient_phone = $tokentemp->patient_phone;
             $mr_no = $tokentemp->mr_no;
            $count_array = array();
            $min_user_id =0;

         $queue_count =  TokenTemp::where('mr_no',$mr_no)->whereIn('priority',[4,5,6,7])->where('called','=',0)->get()->count();
           //return $priority_arr =   array_column($queue_count, 'priority');


        if($queue_count == 0)
        {
            if($priority_sort == $priority_checksort)
            {
         $queue_count = Queue::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('priority','=',8)->where('mr_no',$mr_no)->distinct()->get()->count();

            if($queue_count == 0)
            {
    
               $tokenusers = TokenTemp::whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])->where('called','=',0)->where('priority','=',8)->where('mr_no',$mr_no)->distinct()->get(['department_id','location_id','gender','mr_no','patient_name','patient_phone','priority']);
              
               foreach ($tokenusers as $tokenuser)
                {
                  $department_ids[] = $tokenuser->department_id;
                  $location_id =$tokenuser->location_id;
                  $patient_gender= $tokenuser->gender;
                   $priority = $tokenuser->priority;  
                  $patient_name=$tokenuser->patient_name;
                  $patient_moblie=substr($tokenuser->patient_phone,3);
                  $location_id = $tokenuser->location_id;

              }
               $users =User::whereIn('department_id',$department_ids)->where('location_id','=',$location_id)->get();
                  
                   foreach ($users as $user)
                  {
                    if(in_array($user->id, $service_user_id))
                    {
                      $counts = Queue::where('user_id','=',$user->id)->where('called','=',0)->count();
                      $count_array[]=array ('count' => $counts,'user_id'=>$user->id);
                    }
                  }
              
              if(count($count_array) != 0)
              {
                  $min_counter_array=min($count_array);
                  $min_user_id=$min_counter_array['user_id'];
              
              
              
              $user = User::where('id',$min_user_id)->first();
              $department = Department::where('id',($user->department_id))->first();
              $last_token = $this->add_to_queues->getLastToken($department);
              $token_prefix ='AHL';
            $queue_count =  Queue::where('mr_no',$mr_no)->where('called','=',1)->latest()->first();

               $ldate = date('Y-m-d H:i:s');
               $to_time = strtotime($ldate); 
               $sla = Department::where('id', $queue_count->department_id)->first();
                $token_update_time =$queue_count->updated_at; 
               $from_time = strtotime($token_update_time);
               $minites = round(abs($to_time - $from_time) / 60,2);
               $seconds = ($minites * 60);
              $elapsed_seconds = $sla->service_time;
              if($seconds > $elapsed_seconds)    
              {
                //return $priority;
                $queue = $department->queues()->create([
                'token_prefix' => $queue_count->token_prefix,
                'number' => $queue_count->number,
                'called' => 0,
                'user_id'  =>$user->id,
                'priority' =>$priority,
                'moblie_number' =>$patient_moblie,
                'patient_name' =>$patient_name,
                'gender' =>$patient_gender,
                'mr_no'   =>  $mr_no
                ]);
                         
              }
              
              
              }
             
             
         
          //foreach    } 
         
      }
    }
  }

    }
        //function end

  }


}

