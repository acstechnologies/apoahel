<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CounterRepository;
use App\Repositories\MissedOvertimeReportRepository;
use App\Models\Counter;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Events\TokenIssued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Queue;
use App\Repositories\DepartmentRepository;
use App\Models\Department;
use Carbon\Carbon;
use Auth;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\TokenTemp;


class HelpController extends Controller
{
    protected $counters;
    public function __construct(
      CounterRepository $counters,
      MissedOvertimeReportRepository $missed_overtime_reports
    )
    {
        $this->counters = $counters;
        $this->missed_overtime_reports = $missed_overtime_reports;
    }

    public function index(TokenIssued $event)
    {
      //return "hai";
         Auth::user();
        
        
        
         $user_id = Auth::user()->id;
        $user_role =  Auth::user()->role;
        $user_location_id =Auth::user()->location_id;
        

          
        
        
        
            if($user_id == 1){
               $queues= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   ->where('status',NULL)
                   ->orderBy('queues.id', 'ACS')->get();  
                }
        elseif($user_role == 'CM'){
             $queues= Queue::with('package')->where('location_id',$user_location_id)
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('status',NULL)
                   ->orderBy('queues.id', 'ACS')->get();  
        }
           else {

             $queues = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                     ->where('status',NULL)
                    ->where('user_id','=', $user_id)->orderBy('queues.id', 'ACS')->get();  
               }


        $queue_array = [];
            foreach ($queues as $key => $queue)
            {

             // return ;

             // $userName_count =  TokenTemp::where('mr_no','=',($queue->mr_no))->count();

           
             if($queue->mr_no == "0"){
                $p_name=$queue->moblie_number;
             }else
             {
                 $p_name =$queue->patient_name;
                 
                 
             }
          
 

            if($queue->called) {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['service_user'] =$queue->User->name;// $queue->department->name;
                $queue_array[$key]['number'] =$queue->token_prefix.'-'.$queue->number;
                $queue_array[$key]['called'] = 'Yes';
                $queue_array[$key]['counter1'] = $queue->Department->name;
                $queue_array[$key]['name'] = $p_name;

                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';
                 $queue_array[$key]['hold'] =' <a class="waves-effect waves-light btn" style="border-radius:50px; height:30px; padding:0, 15px;line-height:30px;" onclick="status('.$queue->call->id.')">Status</a> </div>';


                
            } else {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['service_user'] = $queue->User->name;//$queue->department->name;
                $queue_array[$key]['number'] = $queue->token_prefix.'-'.$queue->number;
                $queue_array[$key]['called'] = 'No';
                $queue_array[$key]['counter1'] =  $queue->Department->name;
                $queue_array[$key]['name'] =$p_name;

                $queue_array[$key]['counter'] = 'NIL';
                $queue_array[$key]['recall'] = '<button class="btn-floating disabled" disabled><i class="mdi-navigation-refresh"></i></button>';
                $queue_array[$key]['hold'] = '<button class="waves-effect waves-light btn" style="border-radius:50px; height:30px; padding:0, 15px;line-height:30px;" disabled="disabled">Status</a> ';
            }
        



        }



     return    $data = array('data' => $queue_array);


    }
 
   public function missed(){
        $user =  Auth::user();
       // dd($user);
         $user_id = Auth::user()->id;
          $user_role =  Auth::user()->role;
        $user_location_id =Auth::user()->location_id;

          if($user_id == 1){
               $queues= Queue::whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   ->whereIn('status' , array(3,5))
                   ->orderBy('queues.id', 'ACS')->get(); 
                }
        elseif($user_role == 'CM'){
             $queues= Queue::where('location_id',$user_location_id)
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                   ->whereIn('status' , array(3,5))
                   ->orderBy('queues.id', 'ACS')->get(); 
        }
           else {

             $queues = Queue::whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->whereIn('status' , array(3,5))
                    ->where('user_id','=', $user_id)->orderBy('queues.id', 'ACS')->get(); 
        
           }




      $date =  Carbon::now()->format('d-m-Y');
      $department = Department::where('id',$user->department_id)->first();
      $counter = "all";
      $calls = $this->missed_overtime_reports->getMissedDetails($date, $user, $counter,$department);
      if($user_id == 1){
          $partial= Queue::whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
          ->whereIn('status' , array(2))
          ->orderBy('queues.id', 'ACS')->get(); 
          }
          elseif($user_role == 'CM'){
          $partial= Queue::where('location_id',$user_location_id)
          ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
          ->whereIn('status' , array(2))
          ->orderBy('queues.id', 'ACS')->get(); 
          }
          else {

          $partial = Queue::whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
          ->whereIn('status' , array(2))
          ->where('user_id','=', $user_id)->orderBy('queues.id', 'ACS')->get(); 

           }

          $queue_array = [];
            foreach ($queues as $key => $queue)
             // return ($mergedata);
            {
          
             if($queue->mr_no == "0"){
                $p_name=$queue->moblie_number;
             }else
             {
                 $p_name =$queue->patient_name;
                 
                 
             }

          if($queue->status == 2)
            $stus = 'Partial Complete';
          if($queue->status == 3)
            $stus = 'Stand By';
          if($queue->status == 4)
            $stus = 'Rescheduled';
          if($queue->status == 5)
            $stus = 'No Show';

      
            if($queue->called) {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['service_user'] =$queue->User->name;// $queue->department->name;
                $queue_array[$key]['number'] =$queue->token_prefix.'-'.$queue->number;
                $queue_array[$key]['called'] = 'Yes';
                $queue_array[$key]['counter1'] = $queue->Department->name;
                $queue_array[$key]['status'] = $stus;

                $queue_array[$key]['name'] = $p_name;

                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';
            } else {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['service_user'] = $queue->User->name;//$queue->department->name;
                $queue_array[$key]['number'] = $queue->token_prefix.'-'.$queue->number;
                $queue_array[$key]['called'] = 'No';
                $queue_array[$key]['counter1'] =  $queue->Department->name;
                $queue_array[$key]['status'] = $stus;
                $queue_array[$key]['name'] =$p_name;

                $queue_array[$key]['counter'] = 'NIL';
                $queue_array[$key]['recall'] = '<button class="btn-floating disabled" disabled><i class="mdi-navigation-refresh"></i></button>';
            }
      //    }
      //  }
        



        }
    
         $calls_array = [];
          foreach ($calls as $key => $call)
            {
           if($call->mr_no == "0"){
                $p_name=$call->moblie_number;
             }else
             {
                 $p_name =$call->patient_name;
                 
                 
             }
             //return $call;
              if($call) {

                $calls_array[$key]['id'] = ((int)$key)+1;
                $calls_array[$key]['service_user'] =$call->User->name;// $queue->department->name;
                $calls_array[$key]['number'] =$call->token_prefix.'-'.$call->number;
                $calls_array[$key]['called'] = 'Yes';
                $calls_array[$key]['counter1'] = $call->Department->name;
                $calls_array[$key]['status'] = "Missed";
                $calls_array[$key]['name'] = $p_name;

                $calls_array[$key]['counter'] = $call->counter->name;
                $calls_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$call->id.')"><i class="mdi-navigation-refresh"></i></button>';
            } else {
                $calls_array[$key]['id'] = ((int)$key)+1;
                $calls_array[$key]['service_user'] = $call->User->name;//$queue->department->name;
                $calls_array[$key]['number'] = $call->token_prefix.'-'.$call->number;
                $calls_array[$key]['called'] = 'No';
                $calls_array[$key]['counter1'] =  $call->Department->name;
                 $calls_array[$key]['status'] = "Missed";
               $calls_array[$key]['name'] =$p_name;

                $calls_array[$key]['counter'] = 'NIL';
                $calls_array[$key]['recall'] = '<button class="btn-floating disabled" disabled><i class="mdi-navigation-refresh"></i></button>';
            }
            }

          $partial_array = [];
          foreach ($partial as $key => $par)
          {
          
            if($par){
            $ldate = date('Y-m-d H:i:s');
            $to_time = strtotime($ldate);
            $sla = Department::where('id',$par->Department->id)->first();
            $token_update_time =$par['updated_at']; 
            $from_time = strtotime($token_update_time);
            $minites = round(abs($to_time - $from_time) / 60,2);
            $seconds = ($minites * 60);
            $elapsed_seconds = $sla->service_time;
            $stus = 'Partial Complete';
             if($seconds > $elapsed_seconds)    
              {
                $partial_array[$key]['id'] = ((int)$key)+1;
                $partial_array[$key]['service_user'] =$par->User->name;// $queue->department->name;
                $partial_array[$key]['number'] =$par->token_prefix.'-'.$par->number;
                $partial_array[$key]['called'] = 'Yes';
                $partial_array[$key]['counter1'] = $par->Department->name;
                $partial_array[$key]['status'] = $stus;

                $partial_array[$key]['name'] = $par['patient_name'];

               $partial_array[$key]['counter'] = $par->call->counter->name;
                $partial_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$par->call->id.')"><i class="mdi-navigation-refresh"></i></button>';
              }
            }
          }

          //return  ;  
       $mergedata =   array_merge($queue_array,$calls_array,$partial_array);

        return    $data = array('data' => $mergedata);            
   }

  

    public function help(){




      


   $host        = "host = onlyintegratehis.apollohl.in";
   $port        = "port = 5432";
   $dbname      = "dbname = hms";
   $credentials = "user = token password=T@ken$%!";

   $db = pg_connect( "$host $port $dbname $credentials"  );
   if(!$db) {
      echo "Error : Unable to open database\n";
   } else {
      echo "Opened database successfully<br>";
   }



$sql =<<<EOF
      select * from ahllclinics.ahll_custom_token_management where patient_phone ='+919962003199';
EOF;

   $ret = pg_query($db, $sql);
   if(!$ret) {
      echo pg_last_error($db);
      exit;
   } 


        return pg_fetch_assoc($ret);





   while($row = pg_fetch_row($ret)) {
      echo "ID = ". $row[0] . "\n";
      echo "SID = ". $row[1] ."\n";
      echo "MRNO = ". $row[3] ."\n";
      echo "NAME =  ".$row[4] ."<br>";
   }
   echo "Operation done successfully\n";
   pg_close($db);









/*



         $departmentes = Department::where('package_id','=',1)->get();

$ids =array();
foreach ($departmentes as $key ) {

    $ids[]=$key['id'];
 
}


        $user_id = Auth::user()->id;
          if($user_id == 1){
           $queues= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->whereIn('department_id',$ids)
                     ->distinct()
                    ->orderBy('queues.created_at', 'desc')
                    ->get(['number','department_id']);  
                }else{

             $queues = Queue::with('package')
                    ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('user_id','=', $user_id)
                    ->orderBy('queues.created_at', 'desc')
                     ->distinct(['number','department_id'])
                    ->get();  
               }


        $queue_array1 = [];
        foreach ($queues as $key => $queue) {

             //$user =  User::where('id','=',$queue->user_id)->first();

             $departmentes = Department::where('id','=','department_id')->first();
           
            if($queue->called) {
                $queue_array1[$key]['id'] = ((int)$key)+1;
                $queue_array1[$key]['department'] = $queue->department->name;
                $queue_array1[$key]['department_id'] =$queue->department_id;
                $queue_array1[$key]['number'] = ($queue->department->letter!='')?$queue->department->letter.'-'.$queue->number:$queue->number;
                $queue_array1[$key]['numbers'] = $queue->number;
                $queue_array1[$key]['called'] = 'Yes';
                $queue_array1[$key]['counter1'] = NULL;
                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array1[$key]['call_id'] = $queue->call->id;
                $queue_array1[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';
            } else {
                $queue_array1[$key]['id'] = ((int)$key)+1;
                $queue_array1[$key]['department'] = $queue->department->name;
                 $queue_array1[$key]['department_id'] =$queue->department_id;
                $queue_array1[$key]['number'] = ($queue->department->letter!='')?$queue->department->letter.'-'.$queue->number:$queue->number;
                $queue_array1[$key]['numbers'] = $queue->number;
                $queue_array1[$key]['called'] = 'No';
                $queue_array[$key]['counter1'] = Null;
                 $queue_array1[$key]['call_id'] =0;
                $queue_array1[$key]['counter'] = 'NIL';

                $queue_array1[$key]['recall'] = '<button class="btn-floating disabled" disabled><i class="mdi-navigation-refresh"></i></button>';
            }
        }



  return    $data = array('data' => $queue_array1);

*/

   


   
  }




 public function myformAjax1($id,$tno)
        {


      $queues= Queue::with('package')
                   ->whereBetween('queues.created_at',[Carbon::now()->format('Y-m-d 00:00:00'), Carbon::now()->format('Y-m-d 23:59:59')])
                    ->where('department_id',$id)
                    ->where('number',$tno)
                    ->orderBy('queues.created_at', 'desc')
                    ->get(); 



        $queue_array = array();
        foreach ($queues as $key => $queue) {

           $user =  User::where('id','=',$queue->user_id)->first();

             $departmentes = Department::where('id','=',$user->department_id)->first();

     if($queue->called) {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['department'] = $queue->department->name;
                $queue_array[$key]['number'] = ($queue->department->letter!='')?$queue->department->letter.'-'.$queue->number:$queue->number;
                $queue_array[$key]['called'] = 'Yes';
                $queue_array[$key]['counter1'] = $departmentes->name;
                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array[$key]['call_id'] = $queue->call->id;
                $queue_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';
            } else {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['department'] = $queue->department->name;
                $queue_array[$key]['called'] = 'No';
                $queue_array[$key]['counter1'] = $departmentes->name;
                $queue_array[$key]['counter'] = 'NIL';
                $queue_array[$key]['call_id'] = $queue->call->id;
                $queue_array[$key]['recall'] = '<button class="btn-floating disabled" disabled><i class="mdi-navigation-refresh"></i></button>';
            }
          }
        

      return     $queue_array;

        count($queue_array);

         // $json = json_encode($$queue_array);

    }

 


    
}
