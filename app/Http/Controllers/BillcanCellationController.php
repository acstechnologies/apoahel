<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;
use App\Models\Call;
use App\Models\Doctor;
use App\Models\Location;
use App\Models\DepartmentGroups;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use Carbon\Carbon;


use DB;

class BillcanCellationController extends Controller
{
    protected $departments;

    public function __construct(Location $locations,DepartmentGroups $departmentGrps)
    {
        $this->departmentGrps = $departmentGrps;
        $this->locations = $locations;
    }

    public function index()
    {

        //$this->authorize('access', Department::class);

        $bills = TokenTemp::distinct()->get(['mr_no','bill_no','patient_name','patient_gender','patient_phone','status']);

         $bills = TokenTemp::where('remarks',1)
       ->where('called',0)
       ->whereBetween('created_at',[Carbon::now()->format('Y-m-d 00:00:00'),Carbon::now()->format('Y-m-d 23:59:59')])
       ->distinct()->get(['mr_no','bill_no','patient_name','patient_gender','patient_phone','status']);


       $doctors =  Doctor::all();
       $locations = Location::all();
       $departmentGrps = DepartmentGroups::all();
        return view('user.billcancellation.index', [
            'doctors' => $doctors,
           'locations' => $locations,
           'departmentGrps' => $departmentGrps,
           'bills'          => $bills,

        ]);
    }

    public function create()
    {
        //$this->authorize('access', Department::class);
        $locations = Location::all();
        $departmentGrps = DepartmentGroups::all();

        return view('user.doctors.create',[ 'departmentGrps' =>$departmentGrps,'locations' => $locations]);
    }

    public function store(Request $request, TokenTemp $temp)
    {
        return  $request->id;
        //$this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'service_id' =>'required',
            'location_id' =>'required|numeric',
            'group_id' => 'required|numeric'
        ]);

      //  Department::create($request->all());

        $doctor->name = $request->name;
        $doctor->service_id = $request->service_id;
        $doctor->location_id = $request->location_id;
        $doctor->group_id = $request->group_id;
        $doctor->save();



        flash()->success('Doctor created');
        return redirect()->route('doctors.index');
    }

    public function edit(Request $request,TokenTemp $temp,$id)
    {
         TokenTemp::where('bill_no','=',$id)->update(['status'=>0]); 
  
        
         $location = Location::all();
         $departmentGrps = DepartmentGroups::all();
         return redirect('billcancellation');
        
    }

    public function update(Request $request, Doctor $doctor)
    {
        //$this->authorize('access', Department::class);

        $this->validate($request, [
            'name' => 'required',
            'service_id' =>'required',
            'location_id' => 'required|numeric',
            'group_id' => 'required|numeric',
        ]);

        $doctor->location_id = $request->location_id;
        $doctor->service_id = $request->service_id;
        $doctor->name = $request->name;        
        $doctor->group_id = $request->group_id;
        $doctor->save();

        flash()->success('Doctor updated');
        return redirect()->route('doctors.index');
    }
    public function destroy(Request $request, Doctor $doctor,$id)
    {
       

       TokenTemp::where('bill_no','=',$id)->update(['status'=>1]); 
  
       

        flash()->success('Bill Cancelled');
        return redirect('billcancellation');
        
    }

}
