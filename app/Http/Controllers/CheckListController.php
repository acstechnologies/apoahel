<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\CallRepository;

use App\Models\User;
use App\Models\Department;
use App\Models\Counter;
use App\Models\Call;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Queue;
use App\Models\Services;
use Auth;
use Response;
use App\Repositories\TokenUserRepository;
use App\Repositories\ServicesRepository;
use App\Models\TokenUsers;
use Illuminate\Support\Facades\DB;
use App\Models\TokenTemp;
use App\Repositories\TokenTempRepository;
use App\Repositories\DisplayRepository;





class CheckListController extends Controller
{
    protected $calls;

    public function __construct(CallRepository $calls,DisplayRepository $displays)
    {
        $this->calls = $calls;
        $this->displays = $displays;
    }

 
    
    public function index(Request $request,$mobile)

    {
        $settings = $this->displays->getSettings();

        \App::setLocale($settings->language->code);
      
     $counters =   Counter::all();
     $department =   Department::all();
     $queuess = Queue::all();
     $counter =NULL;
     $called =NULL;


      //  $token_users = TokenTemp::where('mr_no','=',$mobile)->get();


       $token_users =   TokenTemp::where('mr_no',$mobile)->distinct()->get(['mr_no','location_id','patient_name','called','patient_phone','department_id','mr_no','updated_at']);


       $token_phone= $token_users[0]['patient_phone'];
       
       
       $queue_position =Queue::where('mr_no','=',$mobile)->orderBy('updated_at', 'desc')->first();
       $queue_position_count =Queue::where('mr_no','=',$mobile)->orderBy('updated_at', 'desc')->count();


        if($queue_position_count == 0){


          $queue_position =Queue::where('moblie_number','=',$token_phone)->orderBy('updated_at', 'desc')->first();
          $queue_position_count =Queue::where('moblie_number','=',$token_phone)->orderBy('updated_at', 'desc')->count();


        }


       


        if($queue_position_count != 0){
       $user =User::where('id','=',($queue_position->user_id))->first();
       $department =Department::where('id','=',($user->department_id))->first(); 
       $counter = Counter::where('id','=',($department->counter_id))->first();
       $called =$queue_position->called;

                $total = $this->calls->getCustomersWaiting($department);
                $servicestime = Department::where('id',$department->id)->first();
                $service_time =$servicestime->service_time; 
                $approx_time =  $service_time* $total;


       $position_department =$department->name;
       $position_token =$queue_position->token_prefix.'-'.$queue_position->number;
       


      if($queue_position->called == 1){
            $status ='<button class="btn-floating  green " ><i class="mdi-navigation-check"></i></button>';
            $approx_time =0;
            
        }else{
            
            $status ='<button class="btn-floating orange  " ><i class="mdi-navigation-clear"></i></button>';
            $approx_time =$approx_time;
        }


      }else{

       $position_department ='NA';
       $position_token ='NA';
       $status ='NA';


      }




        
        
      /*  if($queue_position->called == 1){
            $status ='<button class="btn-floating  green " ><i class="mdi-navigation-check"></i></button>';
            
        }else{
            
            $status ='<button class="btn-floating orange  " ><i class="mdi-navigation-clear"></i></button>';
        }
        */
        
              $current_position =array('department' => $position_department,
                                    'tone_number' => $position_token,
                                     'called'     => $status,
                                     'user_name'  => $token_users[0]['patient_name'],
                                     'mr_no'      => $token_users[0]['mr_no'],
                                     'approx_time'=> $approx_time,
                                     
                                 );
        //echo $token_users[12]['department_id'];
        
         
       // return  $departments =Department::where('id','=',($token_users[12]['department_id']))->first(); 
        
      $token_users_count = TokenTemp::count();

      foreach ($token_users as $key => $token_user) {

              // $secvice_name = Services::where('service_id','=',($token_user->service_id))->first();
         $departments =Department::where('id','=',($token_user->department_id))->first(); 


              if($token_user->called == 1){
                $tick ='<button class="btn-floating green" ><i class="mdi-navigation-check"></i></button>';

              }else{

                $tick='<button class="btn-floating " <i class="mdi-content-clear"></i></button>';


              }



           
            if($token_users_count >= 0) {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['department'] = $departments->name;
                //$queue_array[$key]['name'] = $approx_time;
                $queue_array[$key]['called'] = $tick;
                /*$queue_array[$key]['counter1'] = $departmentes->name;
                $queue_array[$key]['name'] = $p_name;

                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';*/
            } 
        }


                   


       return view('user.check.index', [
            'users' => $this->calls->getUsers(),
            'counters' => $counters,
            'counter' => $counter,
            'called'  =>$called,
            'departments' => $department,
            'departments_menu' => Department::where('package_id','=',0)->get(),
            'packages' =>Department::where('package_id','=',1)->get(),
            'queuess' =>  $queuess,
            'mychecklist' => $queue_array,
           'current_position' =>$current_position,
            'settings' => $settings,
          
        ]);


     


    }


    public function checkList(){


       $token_users = TokenTemp::all();
      $token_users_count = TokenTemp::count();

      foreach ($token_users as $key => $token_user) {


           
            if($token_users_count >= 0) {
                $queue_array[$key]['id'] = ((int)$key)+1;
                $queue_array[$key]['department'] = $token_user->service_id;
                /*$queue_array[$key]['number'] = 'AHL-'.$queue->number;
                $queue_array[$key]['called'] = 'Yes';
                $queue_array[$key]['counter1'] = $departmentes->name;
                $queue_array[$key]['name'] = $p_name;

                $queue_array[$key]['counter'] = $queue->call->counter->name;
                $queue_array[$key]['recall'] = '<button class="btn-floating waves-effect waves-light tooltipped" onclick="recall('.$queue->call->id.')"><i class="mdi-navigation-refresh"></i></button>';*/
            } 
        }



      return    $data = array('data' => $queue_array);




    }
}