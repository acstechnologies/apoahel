<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenUsers extends Model
{
    protected $fillable = ['activity_description', 'service_id', 'specialization', 'mr_no', 'patient_name', 'patient_gender','bill_no','visit_id','patient_phone','department'];

    public function queue()
	{
		return $this->belongsTo('App\Models\Queue');
	}

    public function department()
	{
		return $this->belongsTo('App\Models\Department');
	}

    public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

    public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
}
