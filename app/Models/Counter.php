<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    protected $fillable = ['name','location_id', 'counter_id','group_id'];

    public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}

	public function location()
    {
    	return $this->belongsTo('App\Models\Location');
    }

    public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

	public function departmentGrps()
	{
		return $this->belongsTo('App\Models\DepartmentGroups');
	}
    public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
}
