<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenTemp extends Model
{
    protected $fillable = ['activity_description','service_id','bill_no','department_id','priority','patient_phone','pp','location_id','called','status','remarks'];

    protected $primaryKey = 'id';
	protected $table = 'token_temp';


    

    public function queue()
	{
		return $this->belongsTo('App\Models\Queue');
	}

    public function department()
	{
		return $this->belongsTo('App\Models\Department');
	}

    public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

    public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
}
