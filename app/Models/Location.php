<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['name','code','location_id'];

    public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}
    
    public function billincounter()
	{
		return $this->hasMany('App\Models\BillingCounter');
	}
    
    
    
    

	/*public function location()
	{
		return $this->belongsTo('App\Models\Location');
	}

	public function department()
	{
		return $this->belongsTo('App\Models\Department');
	}

	public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

	public function queue()
	{
		return $this->hasMany('App\Models\Queue');
	}
*/












}
