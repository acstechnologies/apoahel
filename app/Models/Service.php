<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['id','name','service_id','depatment_id','counter_id','priority','pp','inv_time'];

    public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}

	public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

	public function doctor()
	{
		return $this->belongsTo('App\Models\Doctor');
	}
}
