<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    protected $fillable = ['department_id','token_prefix','number', 'called','user_id','location_id','moblie_number','priority','service_id','mr_no','patient_name','defaults'];

    public function call()
	{
		return $this->hasOne('App\Models\Call');
	}

    public function department()
	{
		return $this->belongsTo('App\Models\Department');
	}

	public function package()
	{
		return $this->hasMany('App\Models\Call');
	}

	 public function user()
	{
		return $this->belongsTo('App\Models\User');
	}



}
