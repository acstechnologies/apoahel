<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'letter', 'start','package_id','department_ids','location_id','counter_id','user_id','group_id'];
    public function location()
    {
    	return $this->belongsTo('App\Models\Location');
    }

    public function queues()
	{
		return $this->hasMany('App\Models\Queue');
	}

    public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}

	public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}
}
