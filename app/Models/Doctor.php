<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['name', 'doctor_id','specialization_id','specialization','hospital_id'];

     public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}

    public function specialization()
    {
    	return $this->belongsTo('App\Models\Specialization');
    }

    public function hospital()
	{
		return $this->belongsTo('App\Models\Hospital');
	}

}
