<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Queue_reports extends Model
{
    protected $fillable = ['date','name','tokens','queue','served','awaiting','missed','over_time'];

    public function calls()
	{
		return $this->hasMany('App\Models\Call');
	}

	public function location()
    {
    	return $this->belongsTo('App\Models\Location');
    }

    public function counter()
	{
		return $this->belongsTo('App\Models\Counter');
	}

	public function departmentGrps()
	{
		return $this->belongsTo('App\Models\DepartmentGroups');
	}
}
