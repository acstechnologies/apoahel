<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = ['id','name','doctor_id'];

   

	
	public function doctor()
	{
		return $this->belongsTo('App\Models\Doctor');
	}
}