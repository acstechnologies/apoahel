<?php return array (
  'laracasts/flash' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Flash\\FlashServiceProvider',
    ),
    'aliases' => 
    array (
      'Flash' => 'Laracasts\\Flash\\Flash',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'rachidlaasri/laravel-installer' => 
  array (
    'providers' => 
    array (
      0 => 'RachidLaasri\\LaravelInstaller\\Providers\\LaravelInstallerServiceProvider',
    ),
  ),
);